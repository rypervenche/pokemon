use once_cell::sync::Lazy;
use pokemon::{Pokemon, Type, Types};
use std::collections::{BTreeMap, HashSet};

pub static GENERATIONS: Lazy<[HashSet<i16>; 8]> = Lazy::new(|| {
    let mut set1 = HashSet::new();
    let mut set2 = HashSet::new();
    let mut set3 = HashSet::new();
    let mut set4 = HashSet::new();
    let mut set5 = HashSet::new();
    let mut set6 = HashSet::new();
    let mut set7 = HashSet::new();
    let mut set8 = HashSet::new();
    set1.extend(1..=151);
    set2.extend(152..=251);
    set3.extend(252..=386);
    set4.extend(387..=493);
    set5.extend(494..=649);
    set6.extend(650..=721);
    set7.extend(722..=809);
    set8.extend(810..=898);
    [set1, set2, set3, set4, set5, set6, set7, set8]
});

pub static POKEMON_LIST: Lazy<BTreeMap<i16, Pokemon>> = Lazy::new(|| {
    let mut m = BTreeMap::new();

    m.insert(
        1,
        Pokemon {
            number: 1,
            english: "Bulbasaur",
            mandarin: "妙蛙種子",
            french: "Bulbizarre",
            german: "Bisasam",
            korean: "이상해씨",
            japanese: "フシギダネ",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        2,
        Pokemon {
            number: 2,
            english: "Ivysaur",
            mandarin: "妙蛙草",
            french: "Herbizarre",
            german: "Bisaknosp",
            korean: "이상해풀",
            japanese: "フシギソウ",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        3,
        Pokemon {
            number: 3,
            english: "Venusaur",
            mandarin: "妙蛙花",
            french: "Florizarre",
            german: "Bisaflor",
            korean: "이상해꽃",
            japanese: "フシギバナ",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        4,
        Pokemon {
            number: 4,
            english: "Charmander",
            mandarin: "小火龍",
            french: "Salamèche",
            german: "Glumanda",
            korean: "파이리",
            japanese: "ヒトカゲ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        5,
        Pokemon {
            number: 5,
            english: "Charmeleon",
            mandarin: "火恐龍",
            french: "Reptincel",
            german: "Glutexo",
            korean: "리자드",
            japanese: "リザード",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        6,
        Pokemon {
            number: 6,
            english: "Charizard",
            mandarin: "噴火龍",
            french: "Dracaufeu",
            german: "Glurak",
            korean: "리자몽",
            japanese: "リザードン",
            types: Types::Dual(Type::Fire, Type::Flying),
        },
    );
    m.insert(
        7,
        Pokemon {
            number: 7,
            english: "Squirtle",
            mandarin: "傑尼龜",
            french: "Carapuce",
            german: "Schiggy",
            korean: "꼬부기",
            japanese: "ゼニガメ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        8,
        Pokemon {
            number: 8,
            english: "Wartortle",
            mandarin: "卡咪龜",
            french: "Carabaffe",
            german: "Schillok",
            korean: "어니부기",
            japanese: "カメール",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        9,
        Pokemon {
            number: 9,
            english: "Blastoise",
            mandarin: "水箭龜",
            french: "Tortank",
            german: "Turtok",
            korean: "거북왕",
            japanese: "カメックス",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        10,
        Pokemon {
            number: 10,
            english: "Caterpie",
            mandarin: "綠毛蟲",
            french: "Chenipan",
            german: "Raupy",
            korean: "캐터피",
            japanese: "キャタピー",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        11,
        Pokemon {
            number: 11,
            english: "Metapod",
            mandarin: "鐵甲蛹",
            french: "Chrysacier",
            german: "Safcon",
            korean: "단데기",
            japanese: "トランセル",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        12,
        Pokemon {
            number: 12,
            english: "Butterfree",
            mandarin: "巴大蝶",
            french: "Papilusion",
            german: "Smettbo",
            korean: "버터플",
            japanese: "バタフリー",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        13,
        Pokemon {
            number: 13,
            english: "Weedle",
            mandarin: "獨角蟲",
            french: "Aspicot",
            german: "Hornliu",
            korean: "뿔충이",
            japanese: "ビードル",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        14,
        Pokemon {
            number: 14,
            english: "Kakuna",
            mandarin: "鐵殼蛹",
            french: "Coconfort",
            german: "Kokuna",
            korean: "딱충이",
            japanese: "コクーン",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        15,
        Pokemon {
            number: 15,
            english: "Beedrill",
            mandarin: "大針蜂",
            french: "Dardargnan",
            german: "Bibor",
            korean: "독침붕",
            japanese: "スピアー",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        16,
        Pokemon {
            number: 16,
            english: "Pidgey",
            mandarin: "波波",
            french: "Roucool",
            german: "Taubsi",
            korean: "구구",
            japanese: "ポッポ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        17,
        Pokemon {
            number: 17,
            english: "Pidgeotto",
            mandarin: "比比鳥",
            french: "Roucoups",
            german: "Tauboga",
            korean: "피죤",
            japanese: "ピジョン",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        18,
        Pokemon {
            number: 18,
            english: "Pidgeot",
            mandarin: "大比鳥",
            french: "Roucarnage",
            german: "Tauboss",
            korean: "피죤투",
            japanese: "ピジョット",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        19,
        Pokemon {
            number: 19,
            english: "Rattata",
            mandarin: "小拉達",
            french: "Rattata",
            german: "Rattfratz",
            korean: "꼬렛",
            japanese: "コラッタ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        20,
        Pokemon {
            number: 20,
            english: "Raticate",
            mandarin: "拉達",
            french: "Rattatac",
            german: "Rattikarl",
            korean: "레트라",
            japanese: "ラッタ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        21,
        Pokemon {
            number: 21,
            english: "Spearow",
            mandarin: "烈雀",
            french: "Piafabec",
            german: "Habitak",
            korean: "깨비참",
            japanese: "オニスズメ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        22,
        Pokemon {
            number: 22,
            english: "Fearow",
            mandarin: "大嘴雀",
            french: "Rapasdepic",
            german: "Ibitak",
            korean: "깨비드릴조",
            japanese: "オニドリル",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        23,
        Pokemon {
            number: 23,
            english: "Ekans",
            mandarin: "阿柏蛇",
            french: "Abo",
            german: "Rettan",
            korean: "아보",
            japanese: "アーボ",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        24,
        Pokemon {
            number: 24,
            english: "Arbok",
            mandarin: "阿柏怪",
            french: "Arbok",
            german: "Arbok",
            korean: "아보크",
            japanese: "アーボック",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        25,
        Pokemon {
            number: 25,
            english: "Pikachu",
            mandarin: "皮卡丘",
            french: "Pikachu",
            german: "Pikachu",
            korean: "피카츄",
            japanese: "ピカチュウ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        26,
        Pokemon {
            number: 26,
            english: "Raichu",
            mandarin: "雷丘",
            french: "Raichu",
            german: "Raichu",
            korean: "라이츄",
            japanese: "ライチュウ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        27,
        Pokemon {
            number: 27,
            english: "Sandshrew",
            mandarin: "穿山鼠",
            french: "Sabelette",
            german: "Sandan",
            korean: "모래두지",
            japanese: "サンド",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        28,
        Pokemon {
            number: 28,
            english: "Sandslash",
            mandarin: "穿山王",
            french: "Sablaireau",
            german: "Sandamer",
            korean: "고지",
            japanese: "サンドパン",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        29,
        Pokemon {
            number: 29,
            english: "Nidoran♀",
            mandarin: "尼多蘭",
            french: "Nidoran♀",
            german: "Nidoran♀",
            korean: "니드런♀",
            japanese: "ニドラン♀",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        30,
        Pokemon {
            number: 30,
            english: "Nidorina",
            mandarin: "尼多娜",
            french: "Nidorina",
            german: "Nidorina",
            korean: "니드리나",
            japanese: "ニドリーナ",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        31,
        Pokemon {
            number: 31,
            english: "Nidoqueen",
            mandarin: "尼多后",
            french: "Nidoqueen",
            german: "Nidoqueen",
            korean: "니드퀸",
            japanese: "ニドクイン",
            types: Types::Dual(Type::Poison, Type::Ground),
        },
    );
    m.insert(
        32,
        Pokemon {
            number: 32,
            english: "Nidoran♂",
            mandarin: "尼多朗",
            french: "Nidoran♂",
            german: "Nidoran♂",
            korean: "니드런♂",
            japanese: "ニドラン♂",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        33,
        Pokemon {
            number: 33,
            english: "Nidorino",
            mandarin: "尼多力諾",
            french: "Nidorino",
            german: "Nidorino",
            korean: "니드리노",
            japanese: "ニドリーノ",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        34,
        Pokemon {
            number: 34,
            english: "Nidoking",
            mandarin: "尼多王",
            french: "Nidoking",
            german: "Nidoking",
            korean: "니드킹",
            japanese: "ニドキング",
            types: Types::Dual(Type::Poison, Type::Ground),
        },
    );
    m.insert(
        35,
        Pokemon {
            number: 35,
            english: "Clefairy",
            mandarin: "皮皮",
            french: "Mélofée",
            german: "Piepi",
            korean: "삐삐",
            japanese: "ピッピ",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        36,
        Pokemon {
            number: 36,
            english: "Clefable",
            mandarin: "皮可西",
            french: "Mélodelfe",
            german: "Pixi",
            korean: "픽시",
            japanese: "ピクシー",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        37,
        Pokemon {
            number: 37,
            english: "Vulpix",
            mandarin: "六尾",
            french: "Goupix",
            german: "Vulpix",
            korean: "식스테일",
            japanese: "ロコン",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        38,
        Pokemon {
            number: 38,
            english: "Ninetales",
            mandarin: "九尾",
            french: "Feunard",
            german: "Vulnona",
            korean: "나인테일",
            japanese: "キュウコン",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        39,
        Pokemon {
            number: 39,
            english: "Jigglypuff",
            mandarin: "胖丁",
            french: "Rondoudou",
            german: "Pummeluff",
            korean: "푸린",
            japanese: "プリン",
            types: Types::Dual(Type::Normal, Type::Fairy),
        },
    );
    m.insert(
        40,
        Pokemon {
            number: 40,
            english: "Wigglytuff",
            mandarin: "胖可丁",
            french: "Grodoudou",
            german: "Knuddeluff",
            korean: "푸크린",
            japanese: "プクリン",
            types: Types::Dual(Type::Normal, Type::Fairy),
        },
    );
    m.insert(
        41,
        Pokemon {
            number: 41,
            english: "Zubat",
            mandarin: "超音蝠",
            french: "Nosferapti",
            german: "Zubat",
            korean: "주뱃",
            japanese: "ズバット",
            types: Types::Dual(Type::Poison, Type::Flying),
        },
    );
    m.insert(
        42,
        Pokemon {
            number: 42,
            english: "Golbat",
            mandarin: "大嘴蝠",
            french: "Nosferalto",
            german: "Golbat",
            korean: "골뱃",
            japanese: "ゴルバット",
            types: Types::Dual(Type::Poison, Type::Flying),
        },
    );
    m.insert(
        43,
        Pokemon {
            number: 43,
            english: "Oddish",
            mandarin: "走路草",
            french: "Mystherbe",
            german: "Myrapla",
            korean: "뚜벅쵸",
            japanese: "ナゾノクサ",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        44,
        Pokemon {
            number: 44,
            english: "Gloom",
            mandarin: "臭臭花",
            french: "Ortide",
            german: "Duflor",
            korean: "냄새꼬",
            japanese: "クサイハナ",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        45,
        Pokemon {
            number: 45,
            english: "Vileplume",
            mandarin: "霸王花",
            french: "Rafflesia",
            german: "Giflor",
            korean: "라플레시아",
            japanese: "ラフレシア",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        46,
        Pokemon {
            number: 46,
            english: "Paras",
            mandarin: "派拉斯",
            french: "Paras",
            german: "Paras",
            korean: "파라스",
            japanese: "パラス",
            types: Types::Dual(Type::Bug, Type::Grass),
        },
    );
    m.insert(
        47,
        Pokemon {
            number: 47,
            english: "Parasect",
            mandarin: "派拉斯特",
            french: "Parasect",
            german: "Parasek",
            korean: "파라섹트",
            japanese: "パラセクト",
            types: Types::Dual(Type::Bug, Type::Grass),
        },
    );
    m.insert(
        48,
        Pokemon {
            number: 48,
            english: "Venonat",
            mandarin: "毛球",
            french: "Mimitoss",
            german: "Bluzuk",
            korean: "콘팡",
            japanese: "コンパン",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        49,
        Pokemon {
            number: 49,
            english: "Venomoth",
            mandarin: "摩魯蛾",
            french: "Aéromite",
            german: "Omot",
            korean: "도나리",
            japanese: "モルフォン",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        50,
        Pokemon {
            number: 50,
            english: "Diglett",
            mandarin: "地鼠",
            french: "Taupiqueur",
            german: "Digda",
            korean: "디그다",
            japanese: "ディグダ",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        51,
        Pokemon {
            number: 51,
            english: "Dugtrio",
            mandarin: "三地鼠",
            french: "Triopikeur",
            german: "Digdri",
            korean: "닥트리오",
            japanese: "ダグトリオ",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        52,
        Pokemon {
            number: 52,
            english: "Meowth",
            mandarin: "喵喵",
            french: "Miaouss",
            german: "Mauzi",
            korean: "나옹",
            japanese: "ニャース",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        53,
        Pokemon {
            number: 53,
            english: "Persian",
            mandarin: "貓老大",
            french: "Persian",
            german: "Snobilikat",
            korean: "페르시온",
            japanese: "ペルシアン",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        54,
        Pokemon {
            number: 54,
            english: "Psyduck",
            mandarin: "可達鴨",
            french: "Psykokwak",
            german: "Enton",
            korean: "고라파덕",
            japanese: "コダック",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        55,
        Pokemon {
            number: 55,
            english: "Golduck",
            mandarin: "哥達鴨",
            french: "Akwakwak",
            german: "Entoron",
            korean: "골덕",
            japanese: "ゴルダック",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        56,
        Pokemon {
            number: 56,
            english: "Mankey",
            mandarin: "猴怪",
            french: "Férosinge",
            german: "Menki",
            korean: "망키",
            japanese: "マンキー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        57,
        Pokemon {
            number: 57,
            english: "Primeape",
            mandarin: "火爆猴",
            french: "Colossinge",
            german: "Rasaff",
            korean: "성원숭",
            japanese: "オコリザル",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        58,
        Pokemon {
            number: 58,
            english: "Growlithe",
            mandarin: "卡蒂狗",
            french: "Caninos",
            german: "Fukano",
            korean: "가디",
            japanese: "ガーディ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        59,
        Pokemon {
            number: 59,
            english: "Arcanine",
            mandarin: "風速狗",
            french: "Arcanin",
            german: "Arkani",
            korean: "윈디",
            japanese: "ウインディ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        60,
        Pokemon {
            number: 60,
            english: "Poliwag",
            mandarin: "蚊香蝌蚪",
            french: "Ptitard",
            german: "Quapsel",
            korean: "발챙이",
            japanese: "ニョロモ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        61,
        Pokemon {
            number: 61,
            english: "Poliwhirl",
            mandarin: "蚊香君",
            french: "Têtarte",
            german: "Quaputzi",
            korean: "슈륙챙이",
            japanese: "ニョロゾ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        62,
        Pokemon {
            number: 62,
            english: "Poliwrath",
            mandarin: "蚊香泳士",
            french: "Tartard",
            german: "Quappo",
            korean: "강챙이",
            japanese: "ニョロボン",
            types: Types::Dual(Type::Water, Type::Fighting),
        },
    );
    m.insert(
        63,
        Pokemon {
            number: 63,
            english: "Abra",
            mandarin: "凱西",
            french: "Abra",
            german: "Abra",
            korean: "캐이시",
            japanese: "ケーシィ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        64,
        Pokemon {
            number: 64,
            english: "Kadabra",
            mandarin: "勇基拉",
            french: "Kadabra",
            german: "Kadabra",
            korean: "윤겔라",
            japanese: "ユンゲラー",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        65,
        Pokemon {
            number: 65,
            english: "Alakazam",
            mandarin: "胡地",
            french: "Alakazam",
            german: "Simsala",
            korean: "후딘",
            japanese: "フーディン",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        66,
        Pokemon {
            number: 66,
            english: "Machop",
            mandarin: "腕力",
            french: "Machoc",
            german: "Machollo",
            korean: "알통몬",
            japanese: "ワンリキー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        67,
        Pokemon {
            number: 67,
            english: "Machoke",
            mandarin: "豪力",
            french: "Machopeur",
            german: "Maschock",
            korean: "근육몬",
            japanese: "ゴーリキー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        68,
        Pokemon {
            number: 68,
            english: "Machamp",
            mandarin: "怪力",
            french: "Mackogneur",
            german: "Machomei",
            korean: "괴력몬",
            japanese: "カイリキー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        69,
        Pokemon {
            number: 69,
            english: "Bellsprout",
            mandarin: "喇叭芽",
            french: "Chétiflor",
            german: "Knofensa",
            korean: "모다피",
            japanese: "マダツボミ",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        70,
        Pokemon {
            number: 70,
            english: "Weepinbell",
            mandarin: "口呆花",
            french: "Boustiflor",
            german: "Ultrigaria",
            korean: "우츠동",
            japanese: "ウツドン",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        71,
        Pokemon {
            number: 71,
            english: "Victreebel",
            mandarin: "大食花",
            french: "Empiflor",
            german: "Sarzenia",
            korean: "우츠보트",
            japanese: "ウツボット",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        72,
        Pokemon {
            number: 72,
            english: "Tentacool",
            mandarin: "瑪瑙水母",
            french: "Tentacool",
            german: "Tentacha",
            korean: "왕눈해",
            japanese: "メノクラゲ",
            types: Types::Dual(Type::Water, Type::Poison),
        },
    );
    m.insert(
        73,
        Pokemon {
            number: 73,
            english: "Tentacruel",
            mandarin: "毒刺水母",
            french: "Tentacruel",
            german: "Tentoxa",
            korean: "독파리",
            japanese: "ドククラゲ",
            types: Types::Dual(Type::Water, Type::Poison),
        },
    );
    m.insert(
        74,
        Pokemon {
            number: 74,
            english: "Geodude",
            mandarin: "小拳石",
            french: "Racaillou",
            german: "Kleinstein",
            korean: "꼬마돌",
            japanese: "イシツブテ",
            types: Types::Dual(Type::Rock, Type::Ground),
        },
    );
    m.insert(
        75,
        Pokemon {
            number: 75,
            english: "Graveler",
            mandarin: "隆隆石",
            french: "Gravalanch",
            german: "Georok",
            korean: "데구리",
            japanese: "ゴローン",
            types: Types::Dual(Type::Rock, Type::Ground),
        },
    );
    m.insert(
        76,
        Pokemon {
            number: 76,
            english: "Golem",
            mandarin: "隆隆岩",
            french: "Grolem",
            german: "Geowaz",
            korean: "딱구리",
            japanese: "ゴローニャ",
            types: Types::Dual(Type::Rock, Type::Ground),
        },
    );
    m.insert(
        77,
        Pokemon {
            number: 77,
            english: "Ponyta",
            mandarin: "小火馬",
            french: "Ponyta",
            german: "Ponita",
            korean: "포니타",
            japanese: "ポニータ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        78,
        Pokemon {
            number: 78,
            english: "Rapidash",
            mandarin: "烈焰馬",
            french: "Galopa",
            german: "Gallopa",
            korean: "날쌩마",
            japanese: "ギャロップ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        79,
        Pokemon {
            number: 79,
            english: "Slowpoke",
            mandarin: "呆呆獸",
            french: "Ramoloss",
            german: "Flegmon",
            korean: "야돈",
            japanese: "ヤドン",
            types: Types::Dual(Type::Water, Type::Psychic),
        },
    );
    m.insert(
        80,
        Pokemon {
            number: 80,
            english: "Slowbro",
            mandarin: "呆殼獸",
            french: "Flagadoss",
            german: "Lahmus",
            korean: "야도란",
            japanese: "ヤドラン",
            types: Types::Dual(Type::Water, Type::Psychic),
        },
    );
    m.insert(
        81,
        Pokemon {
            number: 81,
            english: "Magnemite",
            mandarin: "小磁怪",
            french: "Magnéti",
            german: "Magnetilo",
            korean: "코일",
            japanese: "コイル",
            types: Types::Dual(Type::Electric, Type::Steel),
        },
    );
    m.insert(
        82,
        Pokemon {
            number: 82,
            english: "Magneton",
            mandarin: "三合一磁怪",
            french: "Magnéton",
            german: "Magneton",
            korean: "레어코일",
            japanese: "レアコイル",
            types: Types::Dual(Type::Electric, Type::Steel),
        },
    );
    m.insert(
        83,
        Pokemon {
            number: 83,
            english: "Farfetch'd",
            mandarin: "大蔥鴨",
            french: "Canarticho",
            german: "Porenta",
            korean: "파오리",
            japanese: "カモネギ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        84,
        Pokemon {
            number: 84,
            english: "Doduo",
            mandarin: "嘟嘟",
            french: "Doduo",
            german: "Dodu",
            korean: "두두",
            japanese: "ドードー",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        85,
        Pokemon {
            number: 85,
            english: "Dodrio",
            mandarin: "嘟嘟利",
            french: "Dodrio",
            german: "Dodri",
            korean: "두트리오",
            japanese: "ドードリオ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        86,
        Pokemon {
            number: 86,
            english: "Seel",
            mandarin: "小海獅",
            french: "Otaria",
            german: "Jurob",
            korean: "쥬쥬",
            japanese: "パウワウ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        87,
        Pokemon {
            number: 87,
            english: "Dewgong",
            mandarin: "白海獅",
            french: "Lamantine",
            german: "Jugong",
            korean: "쥬레곤",
            japanese: "ジュゴン",
            types: Types::Dual(Type::Water, Type::Ice),
        },
    );
    m.insert(
        88,
        Pokemon {
            number: 88,
            english: "Grimer",
            mandarin: "臭泥",
            french: "Tadmorv",
            german: "Sleima",
            korean: "질퍽이",
            japanese: "ベトベター",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        89,
        Pokemon {
            number: 89,
            english: "Muk",
            mandarin: "臭臭泥",
            french: "Grotadmorv",
            german: "Sleimok",
            korean: "질뻐기",
            japanese: "ベトベトン",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        90,
        Pokemon {
            number: 90,
            english: "Shellder",
            mandarin: "大舌貝",
            french: "Kokiyas",
            german: "Muschas",
            korean: "셀러",
            japanese: "シェルダー",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        91,
        Pokemon {
            number: 91,
            english: "Cloyster",
            mandarin: "刺甲貝",
            french: "Crustabri",
            german: "Austos",
            korean: "파르셀",
            japanese: "パルシェン",
            types: Types::Dual(Type::Water, Type::Ice),
        },
    );
    m.insert(
        92,
        Pokemon {
            number: 92,
            english: "Gastly",
            mandarin: "鬼斯",
            french: "Fantominus",
            german: "Nebulak",
            korean: "고오스",
            japanese: "ゴース",
            types: Types::Dual(Type::Ghost, Type::Poison),
        },
    );
    m.insert(
        93,
        Pokemon {
            number: 93,
            english: "Haunter",
            mandarin: "鬼斯通",
            french: "Spectrum",
            german: "Alpollo",
            korean: "고우스트",
            japanese: "ゴースト",
            types: Types::Dual(Type::Ghost, Type::Poison),
        },
    );
    m.insert(
        94,
        Pokemon {
            number: 94,
            english: "Gengar",
            mandarin: "耿鬼",
            french: "Ectoplasma",
            german: "Gengar",
            korean: "팬텀",
            japanese: "ゲンガー",
            types: Types::Dual(Type::Ghost, Type::Poison),
        },
    );
    m.insert(
        95,
        Pokemon {
            number: 95,
            english: "Onix",
            mandarin: "大岩蛇",
            french: "Onix",
            german: "Onix",
            korean: "롱스톤",
            japanese: "イワーク",
            types: Types::Dual(Type::Rock, Type::Ground),
        },
    );
    m.insert(
        96,
        Pokemon {
            number: 96,
            english: "Drowzee",
            mandarin: "催眠貘",
            french: "Soporifik",
            german: "Traumato",
            korean: "슬리프",
            japanese: "スリープ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        97,
        Pokemon {
            number: 97,
            english: "Hypno",
            mandarin: "引夢貘人",
            french: "Hypnomade",
            german: "Hypno",
            korean: "슬리퍼",
            japanese: "スリーパー",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        98,
        Pokemon {
            number: 98,
            english: "Krabby",
            mandarin: "大鉗蟹",
            french: "Krabby",
            german: "Krabby",
            korean: "크랩",
            japanese: "クラブ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        99,
        Pokemon {
            number: 99,
            english: "Kingler",
            mandarin: "巨鉗蟹",
            french: "Krabboss",
            german: "Kingler",
            korean: "킹크랩",
            japanese: "キングラー",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        100,
        Pokemon {
            number: 100,
            english: "Voltorb",
            mandarin: "霹靂電球",
            french: "Voltorbe",
            german: "Voltobal",
            korean: "찌리리공",
            japanese: "ビリリダマ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        101,
        Pokemon {
            number: 101,
            english: "Electrode",
            mandarin: "頑皮雷彈",
            french: "Électrode",
            german: "Lektrobal",
            korean: "붐볼",
            japanese: "マルマイン",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        102,
        Pokemon {
            number: 102,
            english: "Exeggcute",
            mandarin: "蛋蛋",
            french: "Noeunoeuf",
            german: "Owei",
            korean: "아라리",
            japanese: "タマタマ",
            types: Types::Dual(Type::Grass, Type::Psychic),
        },
    );
    m.insert(
        103,
        Pokemon {
            number: 103,
            english: "Exeggutor",
            mandarin: "椰蛋樹",
            french: "Noadkoko",
            german: "Kokowei",
            korean: "나시",
            japanese: "ナッシー",
            types: Types::Dual(Type::Grass, Type::Psychic),
        },
    );
    m.insert(
        104,
        Pokemon {
            number: 104,
            english: "Cubone",
            mandarin: "卡拉卡拉",
            french: "Osselait",
            german: "Tragosso",
            korean: "탕구리",
            japanese: "カラカラ",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        105,
        Pokemon {
            number: 105,
            english: "Marowak",
            mandarin: "嘎啦嘎啦",
            french: "Ossatueur",
            german: "Knogga",
            korean: "텅구리",
            japanese: "ガラガラ",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        106,
        Pokemon {
            number: 106,
            english: "Hitmonlee",
            mandarin: "飛腿郎",
            french: "Kicklee",
            german: "Kicklee",
            korean: "시라소몬",
            japanese: "サワムラー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        107,
        Pokemon {
            number: 107,
            english: "Hitmonchan",
            mandarin: "快拳郎",
            french: "Tygnon",
            german: "Nockchan",
            korean: "홍수몬",
            japanese: "エビワラー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        108,
        Pokemon {
            number: 108,
            english: "Lickitung",
            mandarin: "大舌頭",
            french: "Excelangue",
            german: "Schlurp",
            korean: "내루미",
            japanese: "ベロリンガ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        109,
        Pokemon {
            number: 109,
            english: "Koffing",
            mandarin: "瓦斯彈",
            french: "Smogo",
            german: "Smogon",
            korean: "또가스",
            japanese: "ドガース",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        110,
        Pokemon {
            number: 110,
            english: "Weezing",
            mandarin: "雙彈瓦斯",
            french: "Smogogo",
            german: "Smogmog",
            korean: "또도가스",
            japanese: "マタドガス",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        111,
        Pokemon {
            number: 111,
            english: "Rhyhorn",
            mandarin: "獨角犀牛",
            french: "Rhinocorne",
            german: "Rihorn",
            korean: "뿔카노",
            japanese: "サイホーン",
            types: Types::Dual(Type::Ground, Type::Rock),
        },
    );
    m.insert(
        112,
        Pokemon {
            number: 112,
            english: "Rhydon",
            mandarin: "鑽角犀獸",
            french: "Rhinoféros",
            german: "Rizeros",
            korean: "코뿌리",
            japanese: "サイドン",
            types: Types::Dual(Type::Ground, Type::Rock),
        },
    );
    m.insert(
        113,
        Pokemon {
            number: 113,
            english: "Chansey",
            mandarin: "吉利蛋",
            french: "Leveinard",
            german: "Chaneira",
            korean: "럭키",
            japanese: "ラッキー",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        114,
        Pokemon {
            number: 114,
            english: "Tangela",
            mandarin: "蔓藤怪",
            french: "Saquedeneu",
            german: "Tangela",
            korean: "덩구리",
            japanese: "モンジャラ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        115,
        Pokemon {
            number: 115,
            english: "Kangaskhan",
            mandarin: "袋獸",
            french: "Kangourex",
            german: "Kangama",
            korean: "캥카",
            japanese: "ガルーラ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        116,
        Pokemon {
            number: 116,
            english: "Horsea",
            mandarin: "墨海馬",
            french: "Hypotrempe",
            german: "Seeper",
            korean: "쏘드라",
            japanese: "タッツー",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        117,
        Pokemon {
            number: 117,
            english: "Seadra",
            mandarin: "海刺龍",
            french: "Hypocéan",
            german: "Seemon",
            korean: "시드라",
            japanese: "シードラ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        118,
        Pokemon {
            number: 118,
            english: "Goldeen",
            mandarin: "角金魚",
            french: "Poissirène",
            german: "Goldini",
            korean: "콘치",
            japanese: "トサキント",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        119,
        Pokemon {
            number: 119,
            english: "Seaking",
            mandarin: "金魚王",
            french: "Poissoroy",
            german: "Golking",
            korean: "왕콘치",
            japanese: "アズマオウ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        120,
        Pokemon {
            number: 120,
            english: "Staryu",
            mandarin: "海星星",
            french: "Stari",
            german: "Sterndu",
            korean: "별가사리",
            japanese: "ヒトデマン",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        121,
        Pokemon {
            number: 121,
            english: "Starmie",
            mandarin: "寶石海星",
            french: "Staross",
            german: "Starmie",
            korean: "아쿠스타",
            japanese: "スターミー",
            types: Types::Dual(Type::Water, Type::Psychic),
        },
    );
    m.insert(
        122,
        Pokemon {
            number: 122,
            english: "Mr. Mime",
            mandarin: "魔牆人偶",
            french: "M. Mime",
            german: "Pantimos",
            korean: "마임맨",
            japanese: "バリヤード",
            types: Types::Dual(Type::Psychic, Type::Fairy),
        },
    );
    m.insert(
        123,
        Pokemon {
            number: 123,
            english: "Scyther",
            mandarin: "飛天螳螂",
            french: "Insécateur",
            german: "Sichlor",
            korean: "스라크",
            japanese: "ストライク",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        124,
        Pokemon {
            number: 124,
            english: "Jynx",
            mandarin: "迷唇姐",
            french: "Lippoutou",
            german: "Rossana",
            korean: "루주라",
            japanese: "ルージュラ",
            types: Types::Dual(Type::Ice, Type::Psychic),
        },
    );
    m.insert(
        125,
        Pokemon {
            number: 125,
            english: "Electabuzz",
            mandarin: "電擊獸",
            french: "Élektek",
            german: "Elektek",
            korean: "에레브",
            japanese: "エレブー",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        126,
        Pokemon {
            number: 126,
            english: "Magmar",
            mandarin: "鴨嘴火獸",
            french: "Magmar",
            german: "Magmar",
            korean: "마그마",
            japanese: "ブーバー",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        127,
        Pokemon {
            number: 127,
            english: "Pinsir",
            mandarin: "凱羅斯",
            french: "Scarabrute",
            german: "Pinsir",
            korean: "쁘사이저",
            japanese: "カイロス",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        128,
        Pokemon {
            number: 128,
            english: "Tauros",
            mandarin: "肯泰羅",
            french: "Tauros",
            german: "Tauros",
            korean: "켄타로스",
            japanese: "ケンタロス",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        129,
        Pokemon {
            number: 129,
            english: "Magikarp",
            mandarin: "鯉魚王",
            french: "Magicarpe",
            german: "Karpador",
            korean: "잉어킹",
            japanese: "コイキング",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        130,
        Pokemon {
            number: 130,
            english: "Gyarados",
            mandarin: "暴鯉龍",
            french: "Léviator",
            german: "Garados",
            korean: "갸라도스",
            japanese: "ギャラドス",
            types: Types::Dual(Type::Water, Type::Flying),
        },
    );
    m.insert(
        131,
        Pokemon {
            number: 131,
            english: "Lapras",
            mandarin: "拉普拉斯",
            french: "Lokhlass",
            german: "Lapras",
            korean: "라프라스",
            japanese: "ラプラス",
            types: Types::Dual(Type::Water, Type::Ice),
        },
    );
    m.insert(
        132,
        Pokemon {
            number: 132,
            english: "Ditto",
            mandarin: "百變怪",
            french: "Métamorph",
            german: "Ditto",
            korean: "메타몽",
            japanese: "メタモン",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        133,
        Pokemon {
            number: 133,
            english: "Eevee",
            mandarin: "伊布",
            french: "Évoli",
            german: "Evoli",
            korean: "이브이",
            japanese: "イーブイ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        134,
        Pokemon {
            number: 134,
            english: "Vaporeon",
            mandarin: "水伊布",
            french: "Aquali",
            german: "Aquana",
            korean: "샤미드",
            japanese: "シャワーズ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        135,
        Pokemon {
            number: 135,
            english: "Jolteon",
            mandarin: "雷伊布",
            french: "Voltali",
            german: "Blitza",
            korean: "쥬피썬더",
            japanese: "サンダース",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        136,
        Pokemon {
            number: 136,
            english: "Flareon",
            mandarin: "火伊布",
            french: "Pyroli",
            german: "Flamara",
            korean: "부스터",
            japanese: "ブースター",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        137,
        Pokemon {
            number: 137,
            english: "Porygon",
            mandarin: "多邊獸",
            french: "Porygon",
            german: "Porygon",
            korean: "폴리곤",
            japanese: "ポリゴン",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        138,
        Pokemon {
            number: 138,
            english: "Omanyte",
            mandarin: "菊石獸",
            french: "Amonita",
            german: "Amonitas",
            korean: "암나이트",
            japanese: "オムナイト",
            types: Types::Dual(Type::Rock, Type::Water),
        },
    );
    m.insert(
        139,
        Pokemon {
            number: 139,
            english: "Omastar",
            mandarin: "多刺菊石獸",
            french: "Amonistar",
            german: "Amoroso",
            korean: "암스타",
            japanese: "オムスター",
            types: Types::Dual(Type::Rock, Type::Water),
        },
    );
    m.insert(
        140,
        Pokemon {
            number: 140,
            english: "Kabuto",
            mandarin: "化石盔",
            french: "Kabuto",
            german: "Kabuto",
            korean: "투구",
            japanese: "カブト",
            types: Types::Dual(Type::Rock, Type::Water),
        },
    );
    m.insert(
        141,
        Pokemon {
            number: 141,
            english: "Kabutops",
            mandarin: "鐮刀盔",
            french: "Kabutops",
            german: "Kabutops",
            korean: "투구푸스",
            japanese: "カブトプス",
            types: Types::Dual(Type::Rock, Type::Water),
        },
    );
    m.insert(
        142,
        Pokemon {
            number: 142,
            english: "Aerodactyl",
            mandarin: "化石翼龍",
            french: "Ptéra",
            german: "Aerodactyl",
            korean: "프테라",
            japanese: "プテラ",
            types: Types::Dual(Type::Rock, Type::Flying),
        },
    );
    m.insert(
        143,
        Pokemon {
            number: 143,
            english: "Snorlax",
            mandarin: "卡比獸",
            french: "Ronflex",
            german: "Relaxo",
            korean: "잠만보",
            japanese: "カビゴン",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        144,
        Pokemon {
            number: 144,
            english: "Articuno",
            mandarin: "急凍鳥",
            french: "Artikodin",
            german: "Arktos",
            korean: "프리져",
            japanese: "フリーザー",
            types: Types::Dual(Type::Ice, Type::Flying),
        },
    );
    m.insert(
        145,
        Pokemon {
            number: 145,
            english: "Zapdos",
            mandarin: "閃電鳥",
            french: "Électhor",
            german: "Zapdos",
            korean: "썬더",
            japanese: "サンダー",
            types: Types::Dual(Type::Electric, Type::Flying),
        },
    );
    m.insert(
        146,
        Pokemon {
            number: 146,
            english: "Moltres",
            mandarin: "火焰鳥",
            french: "Sulfura",
            german: "Lavados",
            korean: "파이어",
            japanese: "ファイヤー",
            types: Types::Dual(Type::Fire, Type::Flying),
        },
    );
    m.insert(
        147,
        Pokemon {
            number: 147,
            english: "Dratini",
            mandarin: "迷你龍",
            french: "Minidraco",
            german: "Dratini",
            korean: "미뇽",
            japanese: "ミニリュウ",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        148,
        Pokemon {
            number: 148,
            english: "Dragonair",
            mandarin: "哈克龍",
            french: "Draco",
            german: "Dragonir",
            korean: "신뇽",
            japanese: "ハクリュー",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        149,
        Pokemon {
            number: 149,
            english: "Dragonite",
            mandarin: "快龍",
            french: "Dracolosse",
            german: "Dragoran",
            korean: "망나뇽",
            japanese: "カイリュー",
            types: Types::Dual(Type::Dragon, Type::Flying),
        },
    );
    m.insert(
        150,
        Pokemon {
            number: 150,
            english: "Mewtwo",
            mandarin: "超夢",
            french: "Mewtwo",
            german: "Mewtu",
            korean: "뮤츠",
            japanese: "ミュウツー",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        151,
        Pokemon {
            number: 151,
            english: "Mew",
            mandarin: "夢幻",
            french: "Mew",
            german: "Mew",
            korean: "뮤",
            japanese: "ミュウ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        152,
        Pokemon {
            number: 152,
            english: "Chikorita",
            mandarin: "菊草葉",
            french: "Germignon",
            german: "Endivie",
            korean: "치코리타",
            japanese: "チコリータ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        153,
        Pokemon {
            number: 153,
            english: "Bayleef",
            mandarin: "月桂葉",
            french: "Macronium",
            german: "Lorblatt",
            korean: "베이리프",
            japanese: "ベイリーフ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        154,
        Pokemon {
            number: 154,
            english: "Meganium",
            mandarin: "大竺葵",
            french: "Méganium",
            german: "Meganie",
            korean: "메가니움",
            japanese: "メガニウム",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        155,
        Pokemon {
            number: 155,
            english: "Cyndaquil",
            mandarin: "火球鼠",
            french: "Héricendre",
            german: "Feurigel",
            korean: "브케인",
            japanese: "ヒノアラシ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        156,
        Pokemon {
            number: 156,
            english: "Quilava",
            mandarin: "火岩鼠",
            french: "Feurisson",
            german: "Igelavar",
            korean: "마그케인",
            japanese: "マグマラシ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        157,
        Pokemon {
            number: 157,
            english: "Typhlosion",
            mandarin: "火爆獸",
            french: "Typhlosion",
            german: "Tornupto",
            korean: "블레이범",
            japanese: "バクフーン",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        158,
        Pokemon {
            number: 158,
            english: "Totodile",
            mandarin: "小鋸鱷",
            french: "Kaiminus",
            german: "Karnimani",
            korean: "리아코",
            japanese: "ワニノコ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        159,
        Pokemon {
            number: 159,
            english: "Croconaw",
            mandarin: "藍鱷",
            french: "Crocrodil",
            german: "Tyracroc",
            korean: "엘리게이",
            japanese: "アリゲイツ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        160,
        Pokemon {
            number: 160,
            english: "Feraligatr",
            mandarin: "大力鱷",
            french: "Aligatueur",
            german: "Impergator",
            korean: "장크로다일",
            japanese: "オーダイル",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        161,
        Pokemon {
            number: 161,
            english: "Sentret",
            mandarin: "尾立",
            french: "Fouinette",
            german: "Wiesor",
            korean: "꼬리선",
            japanese: "オタチ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        162,
        Pokemon {
            number: 162,
            english: "Furret",
            mandarin: "大尾立",
            french: "Fouinar",
            german: "Wiesenior",
            korean: "다꼬리",
            japanese: "オオタチ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        163,
        Pokemon {
            number: 163,
            english: "Hoothoot",
            mandarin: "咕咕",
            french: "Hoothoot",
            german: "Hoothoot",
            korean: "부우부",
            japanese: "ホーホー",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        164,
        Pokemon {
            number: 164,
            english: "Noctowl",
            mandarin: "貓頭夜鷹",
            french: "Noarfang",
            german: "Noctuh",
            korean: "야부엉",
            japanese: "ヨルノズク",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        165,
        Pokemon {
            number: 165,
            english: "Ledyba",
            mandarin: "芭瓢蟲",
            french: "Coxy",
            german: "Ledyba",
            korean: "레디바",
            japanese: "レディバ",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        166,
        Pokemon {
            number: 166,
            english: "Ledian",
            mandarin: "安瓢蟲",
            french: "Coxyclaque",
            german: "Ledian",
            korean: "레디안",
            japanese: "レディアン",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        167,
        Pokemon {
            number: 167,
            english: "Spinarak",
            mandarin: "圓絲蛛",
            french: "Mimigal",
            german: "Webarak",
            korean: "페이검",
            japanese: "イトマル",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        168,
        Pokemon {
            number: 168,
            english: "Ariados",
            mandarin: "阿利多斯",
            french: "Migalos",
            german: "Ariados",
            korean: "아리아도스",
            japanese: "アリアドス",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        169,
        Pokemon {
            number: 169,
            english: "Crobat",
            mandarin: "叉字蝠",
            french: "Nostenfer",
            german: "Iksbat",
            korean: "크로뱃",
            japanese: "クロバット",
            types: Types::Dual(Type::Poison, Type::Flying),
        },
    );
    m.insert(
        170,
        Pokemon {
            number: 170,
            english: "Chinchou",
            mandarin: "燈籠魚",
            french: "Loupio",
            german: "Lampi",
            korean: "초라기",
            japanese: "チョンチー",
            types: Types::Dual(Type::Water, Type::Electric),
        },
    );
    m.insert(
        171,
        Pokemon {
            number: 171,
            english: "Lanturn",
            mandarin: "電燈怪",
            french: "Lanturn",
            german: "Lanturn",
            korean: "랜턴",
            japanese: "ランターン",
            types: Types::Dual(Type::Water, Type::Electric),
        },
    );
    m.insert(
        172,
        Pokemon {
            number: 172,
            english: "Pichu",
            mandarin: "皮丘",
            french: "Pichu",
            german: "Pichu",
            korean: "피츄",
            japanese: "ピチュー",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        173,
        Pokemon {
            number: 173,
            english: "Cleffa",
            mandarin: "皮寶寶",
            french: "Mélo",
            german: "Pii",
            korean: "삐",
            japanese: "ピィ",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        174,
        Pokemon {
            number: 174,
            english: "Igglybuff",
            mandarin: "寶寶丁",
            french: "Toudoudou",
            german: "Fluffeluff",
            korean: "푸푸린",
            japanese: "ププリン",
            types: Types::Dual(Type::Normal, Type::Fairy),
        },
    );
    m.insert(
        175,
        Pokemon {
            number: 175,
            english: "Togepi",
            mandarin: "波克比",
            french: "Togepi",
            german: "Togepi",
            korean: "토게피",
            japanese: "トゲピー",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        176,
        Pokemon {
            number: 176,
            english: "Togetic",
            mandarin: "波克基古",
            french: "Togetic",
            german: "Togetic",
            korean: "토게틱",
            japanese: "トゲチック",
            types: Types::Dual(Type::Fairy, Type::Flying),
        },
    );
    m.insert(
        177,
        Pokemon {
            number: 177,
            english: "Natu",
            mandarin: "天然雀",
            french: "Natu",
            german: "Natu",
            korean: "네이티",
            japanese: "ネイティ",
            types: Types::Dual(Type::Psychic, Type::Flying),
        },
    );
    m.insert(
        178,
        Pokemon {
            number: 178,
            english: "Xatu",
            mandarin: "天然鳥",
            french: "Xatu",
            german: "Xatu",
            korean: "네이티오",
            japanese: "ネイティオ",
            types: Types::Dual(Type::Psychic, Type::Flying),
        },
    );
    m.insert(
        179,
        Pokemon {
            number: 179,
            english: "Mareep",
            mandarin: "咩利羊",
            french: "Wattouat",
            german: "Voltilamm",
            korean: "메리프",
            japanese: "メリープ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        180,
        Pokemon {
            number: 180,
            english: "Flaaffy",
            mandarin: "茸茸羊",
            french: "Lainergie",
            german: "Waaty",
            korean: "보송송",
            japanese: "モココ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        181,
        Pokemon {
            number: 181,
            english: "Ampharos",
            mandarin: "電龍",
            french: "Pharamp",
            german: "Ampharos",
            korean: "전룡",
            japanese: "デンリュウ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        182,
        Pokemon {
            number: 182,
            english: "Bellossom",
            mandarin: "美麗花",
            french: "Joliflor",
            german: "Blubella",
            korean: "아르코",
            japanese: "キレイハナ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        183,
        Pokemon {
            number: 183,
            english: "Marill",
            mandarin: "瑪力露",
            french: "Marill",
            german: "Marill",
            korean: "마릴",
            japanese: "マリル",
            types: Types::Dual(Type::Water, Type::Fairy),
        },
    );
    m.insert(
        184,
        Pokemon {
            number: 184,
            english: "Azumarill",
            mandarin: "瑪力露麗",
            french: "Azumarill",
            german: "Azumarill",
            korean: "마릴리",
            japanese: "マリルリ",
            types: Types::Dual(Type::Water, Type::Fairy),
        },
    );
    m.insert(
        185,
        Pokemon {
            number: 185,
            english: "Sudowoodo",
            mandarin: "樹才怪",
            french: "Simularbre",
            german: "Mogelbaum",
            korean: "꼬지모",
            japanese: "ウソッキー",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        186,
        Pokemon {
            number: 186,
            english: "Politoed",
            mandarin: "蚊香蛙皇",
            french: "Tarpaud",
            german: "Quaxo",
            korean: "왕구리",
            japanese: "ニョロトノ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        187,
        Pokemon {
            number: 187,
            english: "Hoppip",
            mandarin: "毽子草",
            french: "Granivol",
            german: "Hoppspross",
            korean: "통통코",
            japanese: "ハネッコ",
            types: Types::Dual(Type::Grass, Type::Flying),
        },
    );
    m.insert(
        188,
        Pokemon {
            number: 188,
            english: "Skiploom",
            mandarin: "毽子花",
            french: "Floravol",
            german: "Hubelupf",
            korean: "두코",
            japanese: "ポポッコ",
            types: Types::Dual(Type::Grass, Type::Flying),
        },
    );
    m.insert(
        189,
        Pokemon {
            number: 189,
            english: "Jumpluff",
            mandarin: "毽子棉",
            french: "Cotovol",
            german: "Papungha",
            korean: "솜솜코",
            japanese: "ワタッコ",
            types: Types::Dual(Type::Grass, Type::Flying),
        },
    );
    m.insert(
        190,
        Pokemon {
            number: 190,
            english: "Aipom",
            mandarin: "長尾怪手",
            french: "Capumain",
            german: "Griffel",
            korean: "에이팜",
            japanese: "エイパム",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        191,
        Pokemon {
            number: 191,
            english: "Sunkern",
            mandarin: "向日種子",
            french: "Tournegrin",
            german: "Sonnkern",
            korean: "해너츠",
            japanese: "ヒマナッツ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        192,
        Pokemon {
            number: 192,
            english: "Sunflora",
            mandarin: "向日花怪",
            french: "Héliatronc",
            german: "Sonnflora",
            korean: "해루미",
            japanese: "キマワリ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        193,
        Pokemon {
            number: 193,
            english: "Yanma",
            mandarin: "蜻蜻蜓",
            french: "Yanma",
            german: "Yanma",
            korean: "왕자리",
            japanese: "ヤンヤンマ",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        194,
        Pokemon {
            number: 194,
            english: "Wooper",
            mandarin: "烏波",
            french: "Axoloto",
            german: "Felino",
            korean: "우파",
            japanese: "ウパー",
            types: Types::Dual(Type::Water, Type::Ground),
        },
    );
    m.insert(
        195,
        Pokemon {
            number: 195,
            english: "Quagsire",
            mandarin: "沼王",
            french: "Maraiste",
            german: "Morlord",
            korean: "누오",
            japanese: "ヌオー",
            types: Types::Dual(Type::Water, Type::Ground),
        },
    );
    m.insert(
        196,
        Pokemon {
            number: 196,
            english: "Espeon",
            mandarin: "太陽伊布",
            french: "Mentali",
            german: "Psiana",
            korean: "에브이",
            japanese: "エーフィ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        197,
        Pokemon {
            number: 197,
            english: "Umbreon",
            mandarin: "月亮伊布",
            french: "Noctali",
            german: "Nachtara",
            korean: "블래키",
            japanese: "ブラッキー",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        198,
        Pokemon {
            number: 198,
            english: "Murkrow",
            mandarin: "黑暗鴉",
            french: "Cornèbre",
            german: "Kramurx",
            korean: "니로우",
            japanese: "ヤミカラス",
            types: Types::Dual(Type::Dark, Type::Flying),
        },
    );
    m.insert(
        199,
        Pokemon {
            number: 199,
            english: "Slowking",
            mandarin: "呆呆王",
            french: "Roigada",
            german: "Laschoking",
            korean: "야도킹",
            japanese: "ヤドキング",
            types: Types::Dual(Type::Water, Type::Psychic),
        },
    );
    m.insert(
        200,
        Pokemon {
            number: 200,
            english: "Misdreavus",
            mandarin: "夢妖",
            french: "Feuforêve",
            german: "Traunfugil",
            korean: "무우마",
            japanese: "ムウマ",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        201,
        Pokemon {
            number: 201,
            english: "Unown",
            mandarin: "未知圖騰",
            french: "Zarbi",
            german: "Icognito",
            korean: "안농",
            japanese: "アンノーン",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        202,
        Pokemon {
            number: 202,
            english: "Wobbuffet",
            mandarin: "果然翁",
            french: "Qulbutoké",
            german: "Woingenau",
            korean: "마자용",
            japanese: "ソーナンス",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        203,
        Pokemon {
            number: 203,
            english: "Girafarig",
            mandarin: "麒麟奇",
            french: "Girafarig",
            german: "Girafarig",
            korean: "키링키",
            japanese: "キリンリキ",
            types: Types::Dual(Type::Normal, Type::Psychic),
        },
    );
    m.insert(
        204,
        Pokemon {
            number: 204,
            english: "Pineco",
            mandarin: "榛果球",
            french: "Pomdepik",
            german: "Tannza",
            korean: "피콘",
            japanese: "クヌギダマ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        205,
        Pokemon {
            number: 205,
            english: "Forretress",
            mandarin: "佛烈托斯",
            french: "Foretress",
            german: "Forstellka",
            korean: "쏘콘",
            japanese: "フォレトス",
            types: Types::Dual(Type::Bug, Type::Steel),
        },
    );
    m.insert(
        206,
        Pokemon {
            number: 206,
            english: "Dunsparce",
            mandarin: "土龍弟弟",
            french: "Insolourdo",
            german: "Dummisel",
            korean: "노고치",
            japanese: "ノコッチ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        207,
        Pokemon {
            number: 207,
            english: "Gligar",
            mandarin: "天蠍",
            french: "Scorplane",
            german: "Skorgla",
            korean: "글라이거",
            japanese: "グライガー",
            types: Types::Dual(Type::Ground, Type::Flying),
        },
    );
    m.insert(
        208,
        Pokemon {
            number: 208,
            english: "Steelix",
            mandarin: "大鋼蛇",
            french: "Steelix",
            german: "Stahlos",
            korean: "강철톤",
            japanese: "ハガネール",
            types: Types::Dual(Type::Steel, Type::Ground),
        },
    );
    m.insert(
        209,
        Pokemon {
            number: 209,
            english: "Snubbull",
            mandarin: "布魯",
            french: "Snubbull",
            german: "Snubbull",
            korean: "블루",
            japanese: "ブルー",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        210,
        Pokemon {
            number: 210,
            english: "Granbull",
            mandarin: "布魯皇",
            french: "Granbull",
            german: "Granbull",
            korean: "그랑블루",
            japanese: "グランブル",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        211,
        Pokemon {
            number: 211,
            english: "Qwilfish",
            mandarin: "千針魚",
            french: "Qwilfish",
            german: "Baldorfish",
            korean: "침바루",
            japanese: "ハリーセン",
            types: Types::Dual(Type::Water, Type::Poison),
        },
    );
    m.insert(
        212,
        Pokemon {
            number: 212,
            english: "Scizor",
            mandarin: "巨鉗螳螂",
            french: "Cizayox",
            german: "Scherox",
            korean: "핫삼",
            japanese: "ハッサム",
            types: Types::Dual(Type::Bug, Type::Steel),
        },
    );
    m.insert(
        213,
        Pokemon {
            number: 213,
            english: "Shuckle",
            mandarin: "壺壺",
            french: "Caratroc",
            german: "Pottrott",
            korean: "단단지",
            japanese: "ツボツボ",
            types: Types::Dual(Type::Bug, Type::Rock),
        },
    );
    m.insert(
        214,
        Pokemon {
            number: 214,
            english: "Heracross",
            mandarin: "赫拉克羅斯",
            french: "Scarhino",
            german: "Skaraborn",
            korean: "헤라크로스",
            japanese: "ヘラクロス",
            types: Types::Dual(Type::Bug, Type::Fighting),
        },
    );
    m.insert(
        215,
        Pokemon {
            number: 215,
            english: "Sneasel",
            mandarin: "狃拉",
            french: "Farfuret",
            german: "Sniebel",
            korean: "포푸니",
            japanese: "ニューラ",
            types: Types::Dual(Type::Dark, Type::Ice),
        },
    );
    m.insert(
        216,
        Pokemon {
            number: 216,
            english: "Teddiursa",
            mandarin: "熊寶寶",
            french: "Teddiursa",
            german: "Teddiursa",
            korean: "깜지곰",
            japanese: "ヒメグマ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        217,
        Pokemon {
            number: 217,
            english: "Ursaring",
            mandarin: "圈圈熊",
            french: "Ursaring",
            german: "Ursaring",
            korean: "링곰",
            japanese: "リングマ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        218,
        Pokemon {
            number: 218,
            english: "Slugma",
            mandarin: "熔岩蟲",
            french: "Limagma",
            german: "Schneckmag",
            korean: "마그마그",
            japanese: "マグマッグ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        219,
        Pokemon {
            number: 219,
            english: "Magcargo",
            mandarin: "熔岩蝸牛",
            french: "Volcaropod",
            german: "Magcargo",
            korean: "마그카르고",
            japanese: "マグカルゴ",
            types: Types::Dual(Type::Fire, Type::Rock),
        },
    );
    m.insert(
        220,
        Pokemon {
            number: 220,
            english: "Swinub",
            mandarin: "小山豬",
            french: "Marcacrin",
            german: "Quiekel",
            korean: "꾸꾸리",
            japanese: "ウリムー",
            types: Types::Dual(Type::Ice, Type::Ground),
        },
    );
    m.insert(
        221,
        Pokemon {
            number: 221,
            english: "Piloswine",
            mandarin: "長毛豬",
            french: "Cochignon",
            german: "Keifel",
            korean: "메꾸리",
            japanese: "イノムー",
            types: Types::Dual(Type::Ice, Type::Ground),
        },
    );
    m.insert(
        222,
        Pokemon {
            number: 222,
            english: "Corsola",
            mandarin: "太陽珊瑚",
            french: "Corayon",
            german: "Corasonn",
            korean: "코산호",
            japanese: "サニーゴ",
            types: Types::Dual(Type::Water, Type::Rock),
        },
    );
    m.insert(
        223,
        Pokemon {
            number: 223,
            english: "Remoraid",
            mandarin: "鐵炮魚",
            french: "Rémoraid",
            german: "Remoraid",
            korean: "총어",
            japanese: "テッポウオ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        224,
        Pokemon {
            number: 224,
            english: "Octillery",
            mandarin: "章魚桶",
            french: "Octillery",
            german: "Octillery",
            korean: "대포무노",
            japanese: "オクタン",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        225,
        Pokemon {
            number: 225,
            english: "Delibird",
            mandarin: "信使鳥",
            french: "Cadoizo",
            german: "Botogel",
            korean: "딜리버드",
            japanese: "デリバード",
            types: Types::Dual(Type::Ice, Type::Flying),
        },
    );
    m.insert(
        226,
        Pokemon {
            number: 226,
            english: "Mantine",
            mandarin: "巨翅飛魚",
            french: "Démanta",
            german: "Mantax",
            korean: "만타인",
            japanese: "マンタイン",
            types: Types::Dual(Type::Water, Type::Flying),
        },
    );
    m.insert(
        227,
        Pokemon {
            number: 227,
            english: "Skarmory",
            mandarin: "盔甲鳥",
            french: "Airmure",
            german: "Panzaeron",
            korean: "무장조",
            japanese: "エアームド",
            types: Types::Dual(Type::Steel, Type::Flying),
        },
    );
    m.insert(
        228,
        Pokemon {
            number: 228,
            english: "Houndour",
            mandarin: "戴魯比",
            french: "Malosse",
            german: "Hunduster",
            korean: "델빌",
            japanese: "デルビル",
            types: Types::Dual(Type::Dark, Type::Fire),
        },
    );
    m.insert(
        229,
        Pokemon {
            number: 229,
            english: "Houndoom",
            mandarin: "黑魯加",
            french: "Démolosse",
            german: "Hundemon",
            korean: "헬가",
            japanese: "ヘルガー",
            types: Types::Dual(Type::Dark, Type::Fire),
        },
    );
    m.insert(
        230,
        Pokemon {
            number: 230,
            english: "Kingdra",
            mandarin: "刺龍王",
            french: "Hyporoi",
            german: "Seedraking",
            korean: "킹드라",
            japanese: "キングドラ",
            types: Types::Dual(Type::Water, Type::Dragon),
        },
    );
    m.insert(
        231,
        Pokemon {
            number: 231,
            english: "Phanpy",
            mandarin: "小小象",
            french: "Phanpy",
            german: "Phanpy",
            korean: "코코리",
            japanese: "ゴマゾウ",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        232,
        Pokemon {
            number: 232,
            english: "Donphan",
            mandarin: "頓甲",
            french: "Donphan",
            german: "Donphan",
            korean: "코리갑",
            japanese: "ドンファン",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        233,
        Pokemon {
            number: 233,
            english: "Porygon2",
            mandarin: "多邊獸Ⅱ",
            french: "Porygon2",
            german: "Porygon2",
            korean: "폴리곤2",
            japanese: "ポリゴン２",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        234,
        Pokemon {
            number: 234,
            english: "Stantler",
            mandarin: "驚角鹿",
            french: "Cerfrousse",
            german: "Damhirplex",
            korean: "노라키",
            japanese: "オドシシ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        235,
        Pokemon {
            number: 235,
            english: "Smeargle",
            mandarin: "圖圖犬",
            french: "Queulorior",
            german: "Farbeagle",
            korean: "루브도",
            japanese: "ドーブル",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        236,
        Pokemon {
            number: 236,
            english: "Tyrogue",
            mandarin: "無畏小子",
            french: "Debugant",
            german: "Rabauz",
            korean: "배루키",
            japanese: "バルキー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        237,
        Pokemon {
            number: 237,
            english: "Hitmontop",
            mandarin: "戰舞郎",
            french: "Kapoera",
            german: "Kapoera",
            korean: "카포에라",
            japanese: "カポエラー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        238,
        Pokemon {
            number: 238,
            english: "Smoochum",
            mandarin: "迷唇娃",
            french: "Lippouti",
            german: "Kussilla",
            korean: "뽀뽀라",
            japanese: "ムチュール",
            types: Types::Dual(Type::Ice, Type::Psychic),
        },
    );
    m.insert(
        239,
        Pokemon {
            number: 239,
            english: "Elekid",
            mandarin: "電擊怪",
            french: "Élekid",
            german: "Elekid",
            korean: "에레키드",
            japanese: "エレキッド",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        240,
        Pokemon {
            number: 240,
            english: "Magby",
            mandarin: "鴨嘴寶寶",
            french: "Magby",
            german: "Magby",
            korean: "마그비",
            japanese: "ブビィ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        241,
        Pokemon {
            number: 241,
            english: "Miltank",
            mandarin: "大奶罐",
            french: "Écrémeuh",
            german: "Miltank",
            korean: "밀탱크",
            japanese: "ミルタンク",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        242,
        Pokemon {
            number: 242,
            english: "Blissey",
            mandarin: "幸福蛋",
            french: "Leuphorie",
            german: "Heiteira",
            korean: "해피너스",
            japanese: "ハピナス",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        243,
        Pokemon {
            number: 243,
            english: "Raikou",
            mandarin: "雷公",
            french: "Raikou",
            german: "Raikou",
            korean: "라이코",
            japanese: "ライコウ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        244,
        Pokemon {
            number: 244,
            english: "Entei",
            mandarin: "炎帝",
            french: "Entei",
            german: "Entei",
            korean: "앤테이",
            japanese: "エンテイ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        245,
        Pokemon {
            number: 245,
            english: "Suicune",
            mandarin: "水君",
            french: "Suicune",
            german: "Suicune",
            korean: "스이쿤",
            japanese: "スイクン",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        246,
        Pokemon {
            number: 246,
            english: "Larvitar",
            mandarin: "幼基拉斯",
            french: "Embrylex",
            german: "Larvitar",
            korean: "애버라스",
            japanese: "ヨーギラス",
            types: Types::Dual(Type::Rock, Type::Ground),
        },
    );
    m.insert(
        247,
        Pokemon {
            number: 247,
            english: "Pupitar",
            mandarin: "沙基拉斯",
            french: "Ymphect",
            german: "Pupitar",
            korean: "데기라스",
            japanese: "サナギラス",
            types: Types::Dual(Type::Rock, Type::Ground),
        },
    );
    m.insert(
        248,
        Pokemon {
            number: 248,
            english: "Tyranitar",
            mandarin: "班基拉斯",
            french: "Tyranocif",
            german: "Despotar",
            korean: "마기라스",
            japanese: "バンギラス",
            types: Types::Dual(Type::Rock, Type::Dark),
        },
    );
    m.insert(
        249,
        Pokemon {
            number: 249,
            english: "Lugia",
            mandarin: "洛奇亞",
            french: "Lugia",
            german: "Lugia",
            korean: "루기아",
            japanese: "ルギア",
            types: Types::Dual(Type::Psychic, Type::Flying),
        },
    );
    m.insert(
        250,
        Pokemon {
            number: 250,
            english: "Ho-Oh",
            mandarin: "鳳王",
            french: "Ho-Oh",
            german: "Ho-Oh",
            korean: "칠색조",
            japanese: "ホウオウ",
            types: Types::Dual(Type::Fire, Type::Flying),
        },
    );
    m.insert(
        251,
        Pokemon {
            number: 251,
            english: "Celebi",
            mandarin: "時拉比",
            french: "Celebi",
            german: "Celebi",
            korean: "세레비",
            japanese: "セレビィ",
            types: Types::Dual(Type::Psychic, Type::Grass),
        },
    );
    m.insert(
        252,
        Pokemon {
            number: 252,
            english: "Treecko",
            mandarin: "木守宮",
            french: "Arcko",
            german: "Geckarbor",
            korean: "나무지기",
            japanese: "キモリ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        253,
        Pokemon {
            number: 253,
            english: "Grovyle",
            mandarin: "森林蜥蜴",
            french: "Massko",
            german: "Reptain",
            korean: "나무돌이",
            japanese: "ジュプトル",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        254,
        Pokemon {
            number: 254,
            english: "Sceptile",
            mandarin: "蜥蜴王",
            french: "Jungko",
            german: "Gewaldro",
            korean: "나무킹",
            japanese: "ジュカイン",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        255,
        Pokemon {
            number: 255,
            english: "Torchic",
            mandarin: "火稚雞",
            french: "Poussifeu",
            german: "Flemmli",
            korean: "아차모",
            japanese: "アチャモ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        256,
        Pokemon {
            number: 256,
            english: "Combusken",
            mandarin: "力壯雞",
            french: "Galifeu",
            german: "Jungglut",
            korean: "영치코",
            japanese: "ワカシャモ",
            types: Types::Dual(Type::Fire, Type::Fighting),
        },
    );
    m.insert(
        257,
        Pokemon {
            number: 257,
            english: "Blaziken",
            mandarin: "火焰雞",
            french: "Braségali",
            german: "Lohgock",
            korean: "번치코",
            japanese: "バシャーモ",
            types: Types::Dual(Type::Fire, Type::Fighting),
        },
    );
    m.insert(
        258,
        Pokemon {
            number: 258,
            english: "Mudkip",
            mandarin: "水躍魚",
            french: "Gobou",
            german: "Hydropi",
            korean: "물짱이",
            japanese: "ミズゴロウ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        259,
        Pokemon {
            number: 259,
            english: "Marshtomp",
            mandarin: "沼躍魚",
            french: "Flobio",
            german: "Moorabbel",
            korean: "늪짱이",
            japanese: "ヌマクロー",
            types: Types::Dual(Type::Water, Type::Ground),
        },
    );
    m.insert(
        260,
        Pokemon {
            number: 260,
            english: "Swampert",
            mandarin: "巨沼怪",
            french: "Laggron",
            german: "Sumpex",
            korean: "대짱이",
            japanese: "ラグラージ",
            types: Types::Dual(Type::Water, Type::Ground),
        },
    );
    m.insert(
        261,
        Pokemon {
            number: 261,
            english: "Poochyena",
            mandarin: "土狼犬",
            french: "Medhyèna",
            german: "Fiffyen",
            korean: "포챠나",
            japanese: "ポチエナ",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        262,
        Pokemon {
            number: 262,
            english: "Mightyena",
            mandarin: "大狼犬",
            french: "Grahyèna",
            german: "Magnayen",
            korean: "그라에나",
            japanese: "グラエナ",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        263,
        Pokemon {
            number: 263,
            english: "Zigzagoon",
            mandarin: "蛇紋熊",
            french: "Zigzaton",
            german: "Zigzachs",
            korean: "지그제구리",
            japanese: "ジグザグマ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        264,
        Pokemon {
            number: 264,
            english: "Linoone",
            mandarin: "直衝熊",
            french: "Linéon",
            german: "Geradaks",
            korean: "직구리",
            japanese: "マッスグマ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        265,
        Pokemon {
            number: 265,
            english: "Wurmple",
            mandarin: "刺尾蟲",
            french: "Chenipotte",
            german: "Waumpel",
            korean: "개무소",
            japanese: "ケムッソ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        266,
        Pokemon {
            number: 266,
            english: "Silcoon",
            mandarin: "甲殼繭",
            french: "Armulys",
            german: "Schaloko",
            korean: "실쿤",
            japanese: "カラサリス",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        267,
        Pokemon {
            number: 267,
            english: "Beautifly",
            mandarin: "狩獵鳳蝶",
            french: "Charmillon",
            german: "Papinella",
            korean: "뷰티플라이",
            japanese: "アゲハント",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        268,
        Pokemon {
            number: 268,
            english: "Cascoon",
            mandarin: "盾甲繭",
            french: "Blindalys",
            german: "Panekon",
            korean: "카스쿤",
            japanese: "マユルド",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        269,
        Pokemon {
            number: 269,
            english: "Dustox",
            mandarin: "毒粉蛾",
            french: "Papinox",
            german: "Pudox",
            korean: "독케일",
            japanese: "ドクケイル",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        270,
        Pokemon {
            number: 270,
            english: "Lotad",
            mandarin: "蓮葉童子",
            french: "Nénupiot",
            german: "Loturzel",
            korean: "연꽃몬",
            japanese: "ハスボー",
            types: Types::Dual(Type::Water, Type::Grass),
        },
    );
    m.insert(
        271,
        Pokemon {
            number: 271,
            english: "Lombre",
            mandarin: "蓮帽小童",
            french: "Lombre",
            german: "Lombrero",
            korean: "로토스",
            japanese: "ハスブレロ",
            types: Types::Dual(Type::Water, Type::Grass),
        },
    );
    m.insert(
        272,
        Pokemon {
            number: 272,
            english: "Ludicolo",
            mandarin: "樂天河童",
            french: "Ludicolo",
            german: "Kappalores",
            korean: "로파파",
            japanese: "ルンパッパ",
            types: Types::Dual(Type::Water, Type::Grass),
        },
    );
    m.insert(
        273,
        Pokemon {
            number: 273,
            english: "Seedot",
            mandarin: "橡實果",
            french: "Grainipiot",
            german: "Samurzel",
            korean: "도토링",
            japanese: "タネボー",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        274,
        Pokemon {
            number: 274,
            english: "Nuzleaf",
            mandarin: "長鼻葉",
            french: "Pifeuil",
            german: "Blanas",
            korean: "잎새코",
            japanese: "コノハナ",
            types: Types::Dual(Type::Grass, Type::Dark),
        },
    );
    m.insert(
        275,
        Pokemon {
            number: 275,
            english: "Shiftry",
            mandarin: "狡猾天狗",
            french: "Tengalice",
            german: "Tengulist",
            korean: "다탱구",
            japanese: "ダーテング",
            types: Types::Dual(Type::Grass, Type::Dark),
        },
    );
    m.insert(
        276,
        Pokemon {
            number: 276,
            english: "Taillow",
            mandarin: "傲骨燕",
            french: "Nirondelle",
            german: "Schwalbini",
            korean: "테일로",
            japanese: "スバメ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        277,
        Pokemon {
            number: 277,
            english: "Swellow",
            mandarin: "大王燕",
            french: "Hélédelle",
            german: "Schwalboss",
            korean: "스왈로",
            japanese: "オオスバメ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        278,
        Pokemon {
            number: 278,
            english: "Wingull",
            mandarin: "長翅鷗",
            french: "Goélise",
            german: "Wingull",
            korean: "갈모매",
            japanese: "キャモメ",
            types: Types::Dual(Type::Water, Type::Flying),
        },
    );
    m.insert(
        279,
        Pokemon {
            number: 279,
            english: "Pelipper",
            mandarin: "大嘴鷗",
            french: "Bekipan",
            german: "Pelipper",
            korean: "패리퍼",
            japanese: "ペリッパー",
            types: Types::Dual(Type::Water, Type::Flying),
        },
    );
    m.insert(
        280,
        Pokemon {
            number: 280,
            english: "Ralts",
            mandarin: "拉魯拉絲",
            french: "Tarsal",
            german: "Trasla",
            korean: "랄토스",
            japanese: "ラルトス",
            types: Types::Dual(Type::Psychic, Type::Fairy),
        },
    );
    m.insert(
        281,
        Pokemon {
            number: 281,
            english: "Kirlia",
            mandarin: "奇魯莉安",
            french: "Kirlia",
            german: "Kirlia",
            korean: "킬리아",
            japanese: "キルリア",
            types: Types::Dual(Type::Psychic, Type::Fairy),
        },
    );
    m.insert(
        282,
        Pokemon {
            number: 282,
            english: "Gardevoir",
            mandarin: "沙奈朵",
            french: "Gardevoir",
            german: "Guardevoir",
            korean: "가디안",
            japanese: "サーナイト",
            types: Types::Dual(Type::Psychic, Type::Fairy),
        },
    );
    m.insert(
        283,
        Pokemon {
            number: 283,
            english: "Surskit",
            mandarin: "溜溜糖球",
            french: "Arakdo",
            german: "Gehweiher",
            korean: "비구술",
            japanese: "アメタマ",
            types: Types::Dual(Type::Bug, Type::Water),
        },
    );
    m.insert(
        284,
        Pokemon {
            number: 284,
            english: "Masquerain",
            mandarin: "雨翅蛾",
            french: "Maskadra",
            german: "Maskeregen",
            korean: "비나방",
            japanese: "アメモース",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        285,
        Pokemon {
            number: 285,
            english: "Shroomish",
            mandarin: "蘑蘑菇",
            french: "Balignon",
            german: "Knilz",
            korean: "버섯꼬",
            japanese: "キノココ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        286,
        Pokemon {
            number: 286,
            english: "Breloom",
            mandarin: "斗笠菇",
            french: "Chapignon",
            german: "Kapilz",
            korean: "버섯모",
            japanese: "キノガッサ",
            types: Types::Dual(Type::Grass, Type::Fighting),
        },
    );
    m.insert(
        287,
        Pokemon {
            number: 287,
            english: "Slakoth",
            mandarin: "懶人獺",
            french: "Parecool",
            german: "Bummelz",
            korean: "게을로",
            japanese: "ナマケロ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        288,
        Pokemon {
            number: 288,
            english: "Vigoroth",
            mandarin: "過動猿",
            french: "Vigoroth",
            german: "Muntier",
            korean: "발바로",
            japanese: "ヤルキモノ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        289,
        Pokemon {
            number: 289,
            english: "Slaking",
            mandarin: "請假王",
            french: "Monaflèmit",
            german: "Letarking",
            korean: "게을킹",
            japanese: "ケッキング",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        290,
        Pokemon {
            number: 290,
            english: "Nincada",
            mandarin: "土居忍士",
            french: "Ningale",
            german: "Nincada",
            korean: "토중몬",
            japanese: "ツチニン",
            types: Types::Dual(Type::Bug, Type::Ground),
        },
    );
    m.insert(
        291,
        Pokemon {
            number: 291,
            english: "Ninjask",
            mandarin: "鐵面忍者",
            french: "Ninjask",
            german: "Ninjask",
            korean: "아이스크",
            japanese: "テッカニン",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        292,
        Pokemon {
            number: 292,
            english: "Shedinja",
            mandarin: "脫殼忍者",
            french: "Munja",
            german: "Ninjatom",
            korean: "껍질몬",
            japanese: "ヌケニン",
            types: Types::Dual(Type::Bug, Type::Ghost),
        },
    );
    m.insert(
        293,
        Pokemon {
            number: 293,
            english: "Whismur",
            mandarin: "咕妞妞",
            french: "Chuchmur",
            german: "Flurmel",
            korean: "소곤룡",
            japanese: "ゴニョニョ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        294,
        Pokemon {
            number: 294,
            english: "Loudred",
            mandarin: "吼爆彈",
            french: "Ramboum",
            german: "Krakeelo",
            korean: "노공룡",
            japanese: "ドゴーム",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        295,
        Pokemon {
            number: 295,
            english: "Exploud",
            mandarin: "爆音怪",
            french: "Brouhabam",
            german: "Krawumms",
            korean: "폭음룡",
            japanese: "バクオング",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        296,
        Pokemon {
            number: 296,
            english: "Makuhita",
            mandarin: "幕下力士",
            french: "Makuhita",
            german: "Makuhita",
            korean: "마크탕",
            japanese: "マクノシタ",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        297,
        Pokemon {
            number: 297,
            english: "Hariyama",
            mandarin: "鐵掌力士",
            french: "Hariyama",
            german: "Hariyama",
            korean: "하리뭉",
            japanese: "ハリテヤマ",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        298,
        Pokemon {
            number: 298,
            english: "Azurill",
            mandarin: "露力麗",
            french: "Azurill",
            german: "Azurill",
            korean: "루리리",
            japanese: "ルリリ",
            types: Types::Dual(Type::Normal, Type::Fairy),
        },
    );
    m.insert(
        299,
        Pokemon {
            number: 299,
            english: "Nosepass",
            mandarin: "朝北鼻",
            french: "Tarinor",
            german: "Nasgnet",
            korean: "코코파스",
            japanese: "ノズパス",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        300,
        Pokemon {
            number: 300,
            english: "Skitty",
            mandarin: "向尾喵",
            french: "Skitty",
            german: "Eneco",
            korean: "에나비",
            japanese: "エネコ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        301,
        Pokemon {
            number: 301,
            english: "Delcatty",
            mandarin: "優雅貓",
            french: "Delcatty",
            german: "Enekoro",
            korean: "델케티",
            japanese: "エネコロロ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        302,
        Pokemon {
            number: 302,
            english: "Sableye",
            mandarin: "勾魂眼",
            french: "Ténéfix",
            german: "Zobiris",
            korean: "깜까미",
            japanese: "ヤミラミ",
            types: Types::Dual(Type::Dark, Type::Ghost),
        },
    );
    m.insert(
        303,
        Pokemon {
            number: 303,
            english: "Mawile",
            mandarin: "大嘴娃",
            french: "Mysdibule",
            german: "Flunkifer",
            korean: "입치트",
            japanese: "クチート",
            types: Types::Dual(Type::Steel, Type::Fairy),
        },
    );
    m.insert(
        304,
        Pokemon {
            number: 304,
            english: "Aron",
            mandarin: "可可多拉",
            french: "Galekid",
            german: "Stollunior",
            korean: "가보리",
            japanese: "ココドラ",
            types: Types::Dual(Type::Steel, Type::Rock),
        },
    );
    m.insert(
        305,
        Pokemon {
            number: 305,
            english: "Lairon",
            mandarin: "可多拉",
            french: "Galegon",
            german: "Stollrak",
            korean: "갱도라",
            japanese: "コドラ",
            types: Types::Dual(Type::Steel, Type::Rock),
        },
    );
    m.insert(
        306,
        Pokemon {
            number: 306,
            english: "Aggron",
            mandarin: "波士可多拉",
            french: "Galeking",
            german: "Stolloss",
            korean: "보스로라",
            japanese: "ボスゴドラ",
            types: Types::Dual(Type::Steel, Type::Rock),
        },
    );
    m.insert(
        307,
        Pokemon {
            number: 307,
            english: "Meditite",
            mandarin: "瑪沙那",
            french: "Méditikka",
            german: "Meditie",
            korean: "요가랑",
            japanese: "アサナン",
            types: Types::Dual(Type::Fighting, Type::Psychic),
        },
    );
    m.insert(
        308,
        Pokemon {
            number: 308,
            english: "Medicham",
            mandarin: "恰雷姆",
            french: "Charmina",
            german: "Meditalis",
            korean: "요가램",
            japanese: "チャーレム",
            types: Types::Dual(Type::Fighting, Type::Psychic),
        },
    );
    m.insert(
        309,
        Pokemon {
            number: 309,
            english: "Electrike",
            mandarin: "落雷獸",
            french: "Dynavolt",
            german: "Frizelbliz",
            korean: "썬더라이",
            japanese: "ラクライ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        310,
        Pokemon {
            number: 310,
            english: "Manectric",
            mandarin: "雷電獸",
            french: "Élecsprint",
            german: "Voltenso",
            korean: "썬더볼트",
            japanese: "ライボルト",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        311,
        Pokemon {
            number: 311,
            english: "Plusle",
            mandarin: "正電拍拍",
            french: "Posipi",
            german: "Plusle",
            korean: "플러시",
            japanese: "プラスル",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        312,
        Pokemon {
            number: 312,
            english: "Minun",
            mandarin: "負電拍拍",
            french: "Négapi",
            german: "Minun",
            korean: "마이농",
            japanese: "マイナン",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        313,
        Pokemon {
            number: 313,
            english: "Volbeat",
            mandarin: "電螢蟲",
            french: "Muciole",
            german: "Volbeat",
            korean: "볼비트",
            japanese: "バルビート",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        314,
        Pokemon {
            number: 314,
            english: "Illumise",
            mandarin: "甜甜螢",
            french: "Lumivole",
            german: "Illumise",
            korean: "네오비트",
            japanese: "イルミーゼ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        315,
        Pokemon {
            number: 315,
            english: "Roselia",
            mandarin: "毒薔薇",
            french: "Rosélia",
            german: "Roselia",
            korean: "로젤리아",
            japanese: "ロゼリア",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        316,
        Pokemon {
            number: 316,
            english: "Gulpin",
            mandarin: "溶食獸",
            french: "Gloupti",
            german: "Schluppuck",
            korean: "꼴깍몬",
            japanese: "ゴクリン",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        317,
        Pokemon {
            number: 317,
            english: "Swalot",
            mandarin: "吞食獸",
            french: "Avaltout",
            german: "Schlukwech",
            korean: "꿀꺽몬",
            japanese: "マルノーム",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        318,
        Pokemon {
            number: 318,
            english: "Carvanha",
            mandarin: "利牙魚",
            french: "Carvanha",
            german: "Kanivanha",
            korean: "샤프니아",
            japanese: "キバニア",
            types: Types::Dual(Type::Water, Type::Dark),
        },
    );
    m.insert(
        319,
        Pokemon {
            number: 319,
            english: "Sharpedo",
            mandarin: "巨牙鯊",
            french: "Sharpedo",
            german: "Tohaido",
            korean: "샤크니아",
            japanese: "サメハダー",
            types: Types::Dual(Type::Water, Type::Dark),
        },
    );
    m.insert(
        320,
        Pokemon {
            number: 320,
            english: "Wailmer",
            mandarin: "吼吼鯨",
            french: "Wailmer",
            german: "Wailmer",
            korean: "고래왕자",
            japanese: "ホエルコ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        321,
        Pokemon {
            number: 321,
            english: "Wailord",
            mandarin: "吼鯨王",
            french: "Wailord",
            german: "Wailord",
            korean: "고래왕",
            japanese: "ホエルオー",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        322,
        Pokemon {
            number: 322,
            english: "Numel",
            mandarin: "呆火駝",
            french: "Chamallot",
            german: "Camaub",
            korean: "둔타",
            japanese: "ドンメル",
            types: Types::Dual(Type::Fire, Type::Ground),
        },
    );
    m.insert(
        323,
        Pokemon {
            number: 323,
            english: "Camerupt",
            mandarin: "噴火駝",
            french: "Camérupt",
            german: "Camerupt",
            korean: "폭타",
            japanese: "バクーダ",
            types: Types::Dual(Type::Fire, Type::Ground),
        },
    );
    m.insert(
        324,
        Pokemon {
            number: 324,
            english: "Torkoal",
            mandarin: "煤炭龜",
            french: "Chartor",
            german: "Qurtel",
            korean: "코터스",
            japanese: "コータス",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        325,
        Pokemon {
            number: 325,
            english: "Spoink",
            mandarin: "跳跳豬",
            french: "Spoink",
            german: "Spoink",
            korean: "피그점프",
            japanese: "バネブー",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        326,
        Pokemon {
            number: 326,
            english: "Grumpig",
            mandarin: "噗噗豬",
            french: "Groret",
            german: "Groink",
            korean: "피그킹",
            japanese: "ブーピッグ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        327,
        Pokemon {
            number: 327,
            english: "Spinda",
            mandarin: "晃晃斑",
            french: "Spinda",
            german: "Pandir",
            korean: "얼루기",
            japanese: "パッチール",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        328,
        Pokemon {
            number: 328,
            english: "Trapinch",
            mandarin: "大顎蟻",
            french: "Kraknoix",
            german: "Knacklion",
            korean: "톱치",
            japanese: "ナックラー",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        329,
        Pokemon {
            number: 329,
            english: "Vibrava",
            mandarin: "超音波幼蟲",
            french: "Vibraninf",
            german: "Vibrava",
            korean: "비브라바",
            japanese: "ビブラーバ",
            types: Types::Dual(Type::Ground, Type::Dragon),
        },
    );
    m.insert(
        330,
        Pokemon {
            number: 330,
            english: "Flygon",
            mandarin: "沙漠蜻蜓",
            french: "Libégon",
            german: "Libelldra",
            korean: "플라이곤",
            japanese: "フライゴン",
            types: Types::Dual(Type::Ground, Type::Dragon),
        },
    );
    m.insert(
        331,
        Pokemon {
            number: 331,
            english: "Cacnea",
            mandarin: "刺球仙人掌",
            french: "Cacnea",
            german: "Tuska",
            korean: "선인왕",
            japanese: "サボネア",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        332,
        Pokemon {
            number: 332,
            english: "Cacturne",
            mandarin: "夢歌仙人掌",
            french: "Cacturne",
            german: "Noktuska",
            korean: "밤선인",
            japanese: "ノクタス",
            types: Types::Dual(Type::Grass, Type::Dark),
        },
    );
    m.insert(
        333,
        Pokemon {
            number: 333,
            english: "Swablu",
            mandarin: "青綿鳥",
            french: "Tylton",
            german: "Wablu",
            korean: "파비코",
            japanese: "チルット",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        334,
        Pokemon {
            number: 334,
            english: "Altaria",
            mandarin: "七夕青鳥",
            french: "Altaria",
            german: "Altaria",
            korean: "파비코리",
            japanese: "チルタリス",
            types: Types::Dual(Type::Dragon, Type::Flying),
        },
    );
    m.insert(
        335,
        Pokemon {
            number: 335,
            english: "Zangoose",
            mandarin: "貓鼬斬",
            french: "Mangriff",
            german: "Sengo",
            korean: "쟝고",
            japanese: "ザングース",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        336,
        Pokemon {
            number: 336,
            english: "Seviper",
            mandarin: "飯匙蛇",
            french: "Séviper",
            german: "Vipitis",
            korean: "세비퍼",
            japanese: "ハブネーク",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        337,
        Pokemon {
            number: 337,
            english: "Lunatone",
            mandarin: "月石",
            french: "Séléroc",
            german: "Lunastein",
            korean: "루나톤",
            japanese: "ルナトーン",
            types: Types::Dual(Type::Rock, Type::Psychic),
        },
    );
    m.insert(
        338,
        Pokemon {
            number: 338,
            english: "Solrock",
            mandarin: "太陽岩",
            french: "Solaroc",
            german: "Sonnfel",
            korean: "솔록",
            japanese: "ソルロック",
            types: Types::Dual(Type::Rock, Type::Psychic),
        },
    );
    m.insert(
        339,
        Pokemon {
            number: 339,
            english: "Barboach",
            mandarin: "泥泥鰍",
            french: "Barloche",
            german: "Schmerbe",
            korean: "미꾸리",
            japanese: "ドジョッチ",
            types: Types::Dual(Type::Water, Type::Ground),
        },
    );
    m.insert(
        340,
        Pokemon {
            number: 340,
            english: "Whiscash",
            mandarin: "鯰魚王",
            french: "Barbicha",
            german: "Welsar",
            korean: "메깅",
            japanese: "ナマズン",
            types: Types::Dual(Type::Water, Type::Ground),
        },
    );
    m.insert(
        341,
        Pokemon {
            number: 341,
            english: "Corphish",
            mandarin: "龍蝦小兵",
            french: "Écrapince",
            german: "Krebscorps",
            korean: "가재군",
            japanese: "ヘイガニ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        342,
        Pokemon {
            number: 342,
            english: "Crawdaunt",
            mandarin: "鐵螯龍蝦",
            french: "Colhomard",
            german: "Krebutack",
            korean: "가재장군",
            japanese: "シザリガー",
            types: Types::Dual(Type::Water, Type::Dark),
        },
    );
    m.insert(
        343,
        Pokemon {
            number: 343,
            english: "Baltoy",
            mandarin: "天秤偶",
            french: "Balbuto",
            german: "Puppance",
            korean: "오뚝군",
            japanese: "ヤジロン",
            types: Types::Dual(Type::Ground, Type::Psychic),
        },
    );
    m.insert(
        344,
        Pokemon {
            number: 344,
            english: "Claydol",
            mandarin: "念力土偶",
            french: "Kaorine",
            german: "Lepumentas",
            korean: "점토도리",
            japanese: "ネンドール",
            types: Types::Dual(Type::Ground, Type::Psychic),
        },
    );
    m.insert(
        345,
        Pokemon {
            number: 345,
            english: "Lileep",
            mandarin: "觸手百合",
            french: "Lilia",
            german: "Liliep",
            korean: "릴링",
            japanese: "リリーラ",
            types: Types::Dual(Type::Rock, Type::Grass),
        },
    );
    m.insert(
        346,
        Pokemon {
            number: 346,
            english: "Cradily",
            mandarin: "搖籃百合",
            french: "Vacilys",
            german: "Wielie",
            korean: "릴리요",
            japanese: "ユレイドル",
            types: Types::Dual(Type::Rock, Type::Grass),
        },
    );
    m.insert(
        347,
        Pokemon {
            number: 347,
            english: "Anorith",
            mandarin: "太古羽蟲",
            french: "Anorith",
            german: "Anorith",
            korean: "아노딥스",
            japanese: "アノプス",
            types: Types::Dual(Type::Rock, Type::Bug),
        },
    );
    m.insert(
        348,
        Pokemon {
            number: 348,
            english: "Armaldo",
            mandarin: "太古盔甲",
            french: "Armaldo",
            german: "Armaldo",
            korean: "아말도",
            japanese: "アーマルド",
            types: Types::Dual(Type::Rock, Type::Bug),
        },
    );
    m.insert(
        349,
        Pokemon {
            number: 349,
            english: "Feebas",
            mandarin: "醜醜魚",
            french: "Barpau",
            german: "Barschwa",
            korean: "빈티나",
            japanese: "ヒンバス",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        350,
        Pokemon {
            number: 350,
            english: "Milotic",
            mandarin: "美納斯",
            french: "Milobellus",
            german: "Milotic",
            korean: "밀로틱",
            japanese: "ミロカロス",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        351,
        Pokemon {
            number: 351,
            english: "Castform",
            mandarin: "飄浮泡泡",
            french: "Morphéo",
            german: "Formeo",
            korean: "캐스퐁",
            japanese: "ポワルン",
            types: Types::Variants(vec![
                Types::Single(Type::Normal),
                Types::Single(Type::Fire),
                Types::Single(Type::Water),
                Types::Single(Type::Ice),
            ]),
        },
    );
    m.insert(
        352,
        Pokemon {
            number: 352,
            english: "Kecleon",
            mandarin: "變隱龍",
            french: "Kecleon",
            german: "Kecleon",
            korean: "켈리몬",
            japanese: "カクレオン",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        353,
        Pokemon {
            number: 353,
            english: "Shuppet",
            mandarin: "怨影娃娃",
            french: "Polichombr",
            german: "Shuppet",
            korean: "어둠대신",
            japanese: "カゲボウズ",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        354,
        Pokemon {
            number: 354,
            english: "Banette",
            mandarin: "詛咒娃娃",
            french: "Branette",
            german: "Banette",
            korean: "다크펫",
            japanese: "ジュペッタ",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        355,
        Pokemon {
            number: 355,
            english: "Duskull",
            mandarin: "夜巡靈",
            french: "Skelénox",
            german: "Zwirrlicht",
            korean: "해골몽",
            japanese: "ヨマワル",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        356,
        Pokemon {
            number: 356,
            english: "Dusclops",
            mandarin: "彷徨夜靈",
            french: "Téraclope",
            german: "Zwirrklop",
            korean: "미라몽",
            japanese: "サマヨール",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        357,
        Pokemon {
            number: 357,
            english: "Tropius",
            mandarin: "熱帶龍",
            french: "Tropius",
            german: "Tropius",
            korean: "트로피우스",
            japanese: "トロピウス",
            types: Types::Dual(Type::Grass, Type::Flying),
        },
    );
    m.insert(
        358,
        Pokemon {
            number: 358,
            english: "Chimecho",
            mandarin: "風鈴鈴",
            french: "Éoko",
            german: "Palimpalim",
            korean: "치렁",
            japanese: "チリーン",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        359,
        Pokemon {
            number: 359,
            english: "Absol",
            mandarin: "阿勃梭魯",
            french: "Absol",
            german: "Absol",
            korean: "앱솔",
            japanese: "アブソル",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        360,
        Pokemon {
            number: 360,
            english: "Wynaut",
            mandarin: "小果然",
            french: "Okéoké",
            german: "Isso",
            korean: "마자",
            japanese: "ソーナノ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        361,
        Pokemon {
            number: 361,
            english: "Snorunt",
            mandarin: "雪童子",
            french: "Stalgamin",
            german: "Schneppke",
            korean: "눈꼬마",
            japanese: "ユキワラシ",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        362,
        Pokemon {
            number: 362,
            english: "Glalie",
            mandarin: "冰鬼護",
            french: "Oniglali",
            german: "Firnontor",
            korean: "얼음귀신",
            japanese: "オニゴーリ",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        363,
        Pokemon {
            number: 363,
            english: "Spheal",
            mandarin: "海豹球",
            french: "Obalie",
            german: "Seemops",
            korean: "대굴레오",
            japanese: "タマザラシ",
            types: Types::Dual(Type::Ice, Type::Water),
        },
    );
    m.insert(
        364,
        Pokemon {
            number: 364,
            english: "Sealeo",
            mandarin: "海魔獅",
            french: "Phogleur",
            german: "Seejong",
            korean: "씨레오",
            japanese: "トドグラー",
            types: Types::Dual(Type::Ice, Type::Water),
        },
    );
    m.insert(
        365,
        Pokemon {
            number: 365,
            english: "Walrein",
            mandarin: "帝牙海獅",
            french: "Kaimorse",
            german: "Walraisa",
            korean: "씨카이저",
            japanese: "トドゼルガ",
            types: Types::Dual(Type::Ice, Type::Water),
        },
    );
    m.insert(
        366,
        Pokemon {
            number: 366,
            english: "Clamperl",
            mandarin: "珍珠貝",
            french: "Coquiperl",
            german: "Perlu",
            korean: "진주몽",
            japanese: "パールル",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        367,
        Pokemon {
            number: 367,
            english: "Huntail",
            mandarin: "獵斑魚",
            french: "Serpang",
            german: "Aalabyss",
            korean: "헌테일",
            japanese: "ハンテール",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        368,
        Pokemon {
            number: 368,
            english: "Gorebyss",
            mandarin: "櫻花魚",
            french: "Rosabyss",
            german: "Saganabyss",
            korean: "분홍장이",
            japanese: "サクラビス",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        369,
        Pokemon {
            number: 369,
            english: "Relicanth",
            mandarin: "古空棘魚",
            french: "Relicanth",
            german: "Relicanth",
            korean: "시라칸",
            japanese: "ジーランス",
            types: Types::Dual(Type::Water, Type::Rock),
        },
    );
    m.insert(
        370,
        Pokemon {
            number: 370,
            english: "Luvdisc",
            mandarin: "愛心魚",
            french: "Lovdisc",
            german: "Liebiskus",
            korean: "사랑동이",
            japanese: "ラブカス",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        371,
        Pokemon {
            number: 371,
            english: "Bagon",
            mandarin: "寶貝龍",
            french: "Draby",
            german: "Kindwurm",
            korean: "아공이",
            japanese: "タツベイ",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        372,
        Pokemon {
            number: 372,
            english: "Shelgon",
            mandarin: "甲殼龍",
            french: "Drackhaus",
            german: "Draschel",
            korean: "쉘곤",
            japanese: "コモルー",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        373,
        Pokemon {
            number: 373,
            english: "Salamence",
            mandarin: "暴飛龍",
            french: "Drattak",
            german: "Brutalanda",
            korean: "보만다",
            japanese: "ボーマンダ",
            types: Types::Dual(Type::Dragon, Type::Flying),
        },
    );
    m.insert(
        374,
        Pokemon {
            number: 374,
            english: "Beldum",
            mandarin: "鐵啞鈴",
            french: "Terhal",
            german: "Tanhel",
            korean: "메탕",
            japanese: "ダンバル",
            types: Types::Dual(Type::Steel, Type::Psychic),
        },
    );
    m.insert(
        375,
        Pokemon {
            number: 375,
            english: "Metang",
            mandarin: "金屬怪",
            french: "Métang",
            german: "Metang",
            korean: "메탕구",
            japanese: "メタング",
            types: Types::Dual(Type::Steel, Type::Psychic),
        },
    );
    m.insert(
        376,
        Pokemon {
            number: 376,
            english: "Metagross",
            mandarin: "巨金怪",
            french: "Métalosse",
            german: "Metagross",
            korean: "메타그로스",
            japanese: "メタグロス",
            types: Types::Dual(Type::Steel, Type::Psychic),
        },
    );
    m.insert(
        377,
        Pokemon {
            number: 377,
            english: "Regirock",
            mandarin: "雷吉洛克",
            french: "Regirock",
            german: "Regirock",
            korean: "레지락",
            japanese: "レジロック",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        378,
        Pokemon {
            number: 378,
            english: "Regice",
            mandarin: "雷吉艾斯",
            french: "Regice",
            german: "Regice",
            korean: "레지아이스",
            japanese: "レジアイス",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        379,
        Pokemon {
            number: 379,
            english: "Registeel",
            mandarin: "雷吉斯奇魯",
            french: "Registeel",
            german: "Registeel",
            korean: "레지스틸",
            japanese: "レジスチル",
            types: Types::Single(Type::Steel),
        },
    );
    m.insert(
        380,
        Pokemon {
            number: 380,
            english: "Latias",
            mandarin: "拉帝亞斯",
            french: "Latias",
            german: "Latias",
            korean: "라티아스",
            japanese: "ラティアス",
            types: Types::Dual(Type::Dragon, Type::Psychic),
        },
    );
    m.insert(
        381,
        Pokemon {
            number: 381,
            english: "Latios",
            mandarin: "拉帝歐斯",
            french: "Latios",
            german: "Latios",
            korean: "라티오스",
            japanese: "ラティオス",
            types: Types::Dual(Type::Dragon, Type::Psychic),
        },
    );
    m.insert(
        382,
        Pokemon {
            number: 382,
            english: "Kyogre",
            mandarin: "蓋歐卡",
            french: "Kyogre",
            german: "Kyogre",
            korean: "가이오가",
            japanese: "カイオーガ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        383,
        Pokemon {
            number: 383,
            english: "Groudon",
            mandarin: "固拉多",
            french: "Groudon",
            german: "Groudon",
            korean: "그란돈",
            japanese: "グラードン",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        384,
        Pokemon {
            number: 384,
            english: "Rayquaza",
            mandarin: "烈空坐",
            french: "Rayquaza",
            german: "Rayquaza",
            korean: "레쿠쟈",
            japanese: "レックウザ",
            types: Types::Dual(Type::Dragon, Type::Flying),
        },
    );
    m.insert(
        385,
        Pokemon {
            number: 385,
            english: "Jirachi",
            mandarin: "基拉祈",
            french: "Jirachi",
            german: "Jirachi",
            korean: "지라치",
            japanese: "ジラーチ",
            types: Types::Dual(Type::Steel, Type::Psychic),
        },
    );
    m.insert(
        386,
        Pokemon {
            number: 386,
            english: "Deoxys",
            mandarin: "代歐奇希斯",
            french: "Deoxys",
            german: "Deoxys",
            korean: "테오키스",
            japanese: "デオキシス",
            types: Types::Variants(vec![
                Types::Single(Type::Psychic),
                Types::Single(Type::Psychic),
                Types::Single(Type::Psychic),
                Types::Single(Type::Psychic),
            ]),
        },
    );
    m.insert(
        387,
        Pokemon {
            number: 387,
            english: "Turtwig",
            mandarin: "草苗龜",
            french: "Tortipouss",
            german: "Chelast",
            korean: "모부기",
            japanese: "ナエトル",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        388,
        Pokemon {
            number: 388,
            english: "Grotle",
            mandarin: "樹林龜",
            french: "Boskara",
            german: "Chelcarain",
            korean: "수풀부기",
            japanese: "ハヤシガメ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        389,
        Pokemon {
            number: 389,
            english: "Torterra",
            mandarin: "土台龜",
            french: "Torterra",
            german: "Chelterrar",
            korean: "토대부기",
            japanese: "ドダイトス",
            types: Types::Dual(Type::Grass, Type::Ground),
        },
    );
    m.insert(
        390,
        Pokemon {
            number: 390,
            english: "Chimchar",
            mandarin: "小火焰猴",
            french: "Ouisticram",
            german: "Panflam",
            korean: "불꽃숭이",
            japanese: "ヒコザル",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        391,
        Pokemon {
            number: 391,
            english: "Monferno",
            mandarin: "猛火猴",
            french: "Chimpenfeu",
            german: "Panpyro",
            korean: "파이숭이",
            japanese: "モウカザル",
            types: Types::Dual(Type::Fire, Type::Fighting),
        },
    );
    m.insert(
        392,
        Pokemon {
            number: 392,
            english: "Infernape",
            mandarin: "烈焰猴",
            french: "Simiabraz",
            german: "Panferno",
            korean: "초염몽",
            japanese: "ゴウカザル",
            types: Types::Dual(Type::Fire, Type::Fighting),
        },
    );
    m.insert(
        393,
        Pokemon {
            number: 393,
            english: "Piplup",
            mandarin: "波加曼",
            french: "Tiplouf",
            german: "Plinfa",
            korean: "팽도리",
            japanese: "ポッチャマ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        394,
        Pokemon {
            number: 394,
            english: "Prinplup",
            mandarin: "波皇子",
            french: "Prinplouf",
            german: "Pliprin",
            korean: "팽태자",
            japanese: "ポッタイシ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        395,
        Pokemon {
            number: 395,
            english: "Empoleon",
            mandarin: "帝王拿波",
            french: "Pingoléon",
            german: "Impoleon",
            korean: "엠페르트",
            japanese: "エンペルト",
            types: Types::Dual(Type::Water, Type::Steel),
        },
    );
    m.insert(
        396,
        Pokemon {
            number: 396,
            english: "Starly",
            mandarin: "姆克兒",
            french: "Étourmi",
            german: "Staralili",
            korean: "찌르꼬",
            japanese: "ムックル",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        397,
        Pokemon {
            number: 397,
            english: "Staravia",
            mandarin: "姆克鳥",
            french: "Étourvol",
            german: "Staravia",
            korean: "찌르버드",
            japanese: "ムクバード",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        398,
        Pokemon {
            number: 398,
            english: "Staraptor",
            mandarin: "姆克鷹",
            french: "Étouraptor",
            german: "Staraptor",
            korean: "찌르호크",
            japanese: "ムクホーク",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        399,
        Pokemon {
            number: 399,
            english: "Bidoof",
            mandarin: "大牙狸",
            french: "Keunotor",
            german: "Bidiza",
            korean: "비버니",
            japanese: "ビッパ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        400,
        Pokemon {
            number: 400,
            english: "Bibarel",
            mandarin: "大尾狸",
            french: "Castorno",
            german: "Bidifas",
            korean: "비버통",
            japanese: "ビーダル",
            types: Types::Dual(Type::Normal, Type::Water),
        },
    );
    m.insert(
        401,
        Pokemon {
            number: 401,
            english: "Kricketot",
            mandarin: "圓法師",
            french: "Crikzik",
            german: "Zirpurze",
            korean: "귀뚤뚜기",
            japanese: "コロボーシ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        402,
        Pokemon {
            number: 402,
            english: "Kricketune",
            mandarin: "音箱蟀",
            french: "Mélokrik",
            german: "Zirpeise",
            korean: "귀뚤톡크",
            japanese: "コロトック",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        403,
        Pokemon {
            number: 403,
            english: "Shinx",
            mandarin: "小貓怪",
            french: "Lixy",
            german: "Sheinux",
            korean: "꼬링크",
            japanese: "コリンク",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        404,
        Pokemon {
            number: 404,
            english: "Luxio",
            mandarin: "勒克貓",
            french: "Luxio",
            german: "Luxio",
            korean: "럭시오",
            japanese: "ルクシオ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        405,
        Pokemon {
            number: 405,
            english: "Luxray",
            mandarin: "倫琴貓",
            french: "Luxray",
            german: "Luxtra",
            korean: "렌트라",
            japanese: "レントラー",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        406,
        Pokemon {
            number: 406,
            english: "Budew",
            mandarin: "含羞苞",
            french: "Rozbouton",
            german: "Knospi",
            korean: "꼬몽울",
            japanese: "スボミー",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        407,
        Pokemon {
            number: 407,
            english: "Roserade",
            mandarin: "羅絲雷朵",
            french: "Roserade",
            german: "Roserade",
            korean: "로즈레이드",
            japanese: "ロズレイド",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        408,
        Pokemon {
            number: 408,
            english: "Cranidos",
            mandarin: "頭蓋龍",
            french: "Kranidos",
            german: "Koknodon",
            korean: "두개도스",
            japanese: "ズガイドス",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        409,
        Pokemon {
            number: 409,
            english: "Rampardos",
            mandarin: "戰槌龍",
            french: "Charkos",
            german: "Rameidon",
            korean: "램펄드",
            japanese: "ラムパルド",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        410,
        Pokemon {
            number: 410,
            english: "Shieldon",
            mandarin: "盾甲龍",
            french: "Dinoclier",
            german: "Schilterus",
            korean: "방패톱스",
            japanese: "タテトプス",
            types: Types::Dual(Type::Rock, Type::Steel),
        },
    );
    m.insert(
        411,
        Pokemon {
            number: 411,
            english: "Bastiodon",
            mandarin: "護城龍",
            french: "Bastiodon",
            german: "Bollterus",
            korean: "바리톱스",
            japanese: "トリデプス",
            types: Types::Dual(Type::Rock, Type::Steel),
        },
    );
    m.insert(
        412,
        Pokemon {
            number: 412,
            english: "Burmy",
            mandarin: "結草兒",
            french: "Cheniti",
            german: "Burmy",
            korean: "도롱충이",
            japanese: "ミノムッチ",
            types: Types::Variants(vec![
                Types::Single(Type::Bug),
                Types::Single(Type::Bug),
                Types::Single(Type::Bug),
            ]),
        },
    );
    m.insert(
        413,
        Pokemon {
            number: 413,
            english: "Wormadam",
            mandarin: "結草貴婦",
            french: "Cheniselle",
            german: "Burmadame",
            korean: "도롱마담",
            japanese: "ミノマダム",
            types: Types::Variants(vec![
                Types::Dual(Type::Bug, Type::Grass),
                Types::Dual(Type::Bug, Type::Ground),
                Types::Dual(Type::Bug, Type::Steel),
            ]),
        },
    );
    m.insert(
        414,
        Pokemon {
            number: 414,
            english: "Mothim",
            mandarin: "紳士蛾",
            french: "Papilord",
            german: "Moterpel",
            korean: "나메일",
            japanese: "ガーメイル",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        415,
        Pokemon {
            number: 415,
            english: "Combee",
            mandarin: "三蜜蜂",
            french: "Apitrini",
            german: "Wadribie",
            korean: "세꿀버리",
            japanese: "ミツハニー",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        416,
        Pokemon {
            number: 416,
            english: "Vespiquen",
            mandarin: "蜂女王",
            french: "Apireine",
            german: "Honweisel",
            korean: "비퀸",
            japanese: "ビークイン",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        417,
        Pokemon {
            number: 417,
            english: "Pachirisu",
            mandarin: "帕奇利茲",
            french: "Pachirisu",
            german: "Pachirisu",
            korean: "파치리스",
            japanese: "パチリス",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        418,
        Pokemon {
            number: 418,
            english: "Buizel",
            mandarin: "泳圈鼬",
            french: "Mustébouée",
            german: "Bamelin",
            korean: "브이젤",
            japanese: "ブイゼル",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        419,
        Pokemon {
            number: 419,
            english: "Floatzel",
            mandarin: "浮潛鼬",
            french: "Mustéflott",
            german: "Bojelin",
            korean: "플로젤",
            japanese: "フローゼル",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        420,
        Pokemon {
            number: 420,
            english: "Cherubi",
            mandarin: "櫻花寶",
            french: "Ceribou",
            german: "Kikugi",
            korean: "체리버",
            japanese: "チェリンボ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        421,
        Pokemon {
            number: 421,
            english: "Cherrim",
            mandarin: "櫻花兒",
            french: "Ceriflor",
            german: "Kinoso",
            korean: "체리꼬",
            japanese: "チェリム",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        422,
        Pokemon {
            number: 422,
            english: "Shellos",
            mandarin: "無殼海兔",
            french: "Sancoki",
            german: "Schalellos",
            korean: "깝질무",
            japanese: "カラナクシ",
            types: Types::Variants(vec![Types::Single(Type::Water), Types::Single(Type::Water)]),
        },
    );
    m.insert(
        423,
        Pokemon {
            number: 423,
            english: "Gastrodon",
            mandarin: "海兔獸",
            french: "Tritosor",
            german: "Gastrodon",
            korean: "트리토돈",
            japanese: "トリトドン",
            types: Types::Variants(vec![
                Types::Dual(Type::Water, Type::Ground),
                Types::Dual(Type::Water, Type::Ground),
            ]),
        },
    );
    m.insert(
        424,
        Pokemon {
            number: 424,
            english: "Ambipom",
            mandarin: "雙尾怪手",
            french: "Capidextre",
            german: "Ambidiffel",
            korean: "겟핸보숭",
            japanese: "エテボース",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        425,
        Pokemon {
            number: 425,
            english: "Drifloon",
            mandarin: "飄飄球",
            french: "Baudrive",
            german: "Driftlon",
            korean: "흔들풍손",
            japanese: "フワンテ",
            types: Types::Dual(Type::Ghost, Type::Flying),
        },
    );
    m.insert(
        426,
        Pokemon {
            number: 426,
            english: "Drifblim",
            mandarin: "隨風球",
            french: "Grodrive",
            german: "Drifzepeli",
            korean: "둥실라이드",
            japanese: "フワライド",
            types: Types::Dual(Type::Ghost, Type::Flying),
        },
    );
    m.insert(
        427,
        Pokemon {
            number: 427,
            english: "Buneary",
            mandarin: "捲捲耳",
            french: "Laporeille",
            german: "Haspiror",
            korean: "이어롤",
            japanese: "ミミロル",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        428,
        Pokemon {
            number: 428,
            english: "Lopunny",
            mandarin: "長耳兔",
            french: "Lockpin",
            german: "Schlapor",
            korean: "이어롭",
            japanese: "ミミロップ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        429,
        Pokemon {
            number: 429,
            english: "Mismagius",
            mandarin: "夢妖魔",
            french: "Magirêve",
            german: "Traunmagil",
            korean: "무우마직",
            japanese: "ムウマージ",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        430,
        Pokemon {
            number: 430,
            english: "Honchkrow",
            mandarin: "烏鴉頭頭",
            french: "Corboss",
            german: "Kramshef",
            korean: "돈크로우",
            japanese: "ドンカラス",
            types: Types::Dual(Type::Dark, Type::Flying),
        },
    );
    m.insert(
        431,
        Pokemon {
            number: 431,
            english: "Glameow",
            mandarin: "魅力喵",
            french: "Chaglam",
            german: "Charmian",
            korean: "나옹마",
            japanese: "ニャルマー",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        432,
        Pokemon {
            number: 432,
            english: "Purugly",
            mandarin: "東施喵",
            french: "Chaffreux",
            german: "Shnurgarst",
            korean: "몬냥이",
            japanese: "ブニャット",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        433,
        Pokemon {
            number: 433,
            english: "Chingling",
            mandarin: "鈴鐺響",
            french: "Korillon",
            german: "Klingplim",
            korean: "랑딸랑",
            japanese: "リーシャン",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        434,
        Pokemon {
            number: 434,
            english: "Stunky",
            mandarin: "臭鼬噗",
            french: "Moufouette",
            german: "Skunkapuh",
            korean: "스컹뿡",
            japanese: "スカンプー",
            types: Types::Dual(Type::Poison, Type::Dark),
        },
    );
    m.insert(
        435,
        Pokemon {
            number: 435,
            english: "Skuntank",
            mandarin: "坦克臭鼬",
            french: "Moufflair",
            german: "Skuntank",
            korean: "스컹탱크",
            japanese: "スカタンク",
            types: Types::Dual(Type::Poison, Type::Dark),
        },
    );
    m.insert(
        436,
        Pokemon {
            number: 436,
            english: "Bronzor",
            mandarin: "銅鏡怪",
            french: "Archéomire",
            german: "Bronzel",
            korean: "동미러",
            japanese: "ドーミラー",
            types: Types::Dual(Type::Steel, Type::Psychic),
        },
    );
    m.insert(
        437,
        Pokemon {
            number: 437,
            english: "Bronzong",
            mandarin: "青銅鐘",
            french: "Archéodong",
            german: "Bronzong",
            korean: "동탁군",
            japanese: "ドータクン",
            types: Types::Dual(Type::Steel, Type::Psychic),
        },
    );
    m.insert(
        438,
        Pokemon {
            number: 438,
            english: "Bonsly",
            mandarin: "盆才怪",
            french: "Manzaï",
            german: "Mobai",
            korean: "꼬지지",
            japanese: "ウソハチ",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        439,
        Pokemon {
            number: 439,
            english: "Mime Jr.",
            mandarin: "魔尼尼",
            french: "Mime Jr.",
            german: "Pantimimi",
            korean: "흉내내",
            japanese: "マネネ",
            types: Types::Dual(Type::Psychic, Type::Fairy),
        },
    );
    m.insert(
        440,
        Pokemon {
            number: 440,
            english: "Happiny",
            mandarin: "小福蛋",
            french: "Ptiravi",
            german: "Wonneira",
            korean: "핑복",
            japanese: "ピンプク",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        441,
        Pokemon {
            number: 441,
            english: "Chatot",
            mandarin: "聒噪鳥",
            french: "Pijako",
            german: "Plaudagei",
            korean: "페라페",
            japanese: "ペラップ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        442,
        Pokemon {
            number: 442,
            english: "Spiritomb",
            mandarin: "花岩怪",
            french: "Spiritomb",
            german: "Kryppuk",
            korean: "화강돌",
            japanese: "ミカルゲ",
            types: Types::Dual(Type::Ghost, Type::Dark),
        },
    );
    m.insert(
        443,
        Pokemon {
            number: 443,
            english: "Gible",
            mandarin: "圓陸鯊",
            french: "Griknot",
            german: "Kaumalat",
            korean: "딥상어동",
            japanese: "フカマル",
            types: Types::Dual(Type::Dragon, Type::Ground),
        },
    );
    m.insert(
        444,
        Pokemon {
            number: 444,
            english: "Gabite",
            mandarin: "尖牙陸鯊",
            french: "Carmache",
            german: "Knarksel",
            korean: "한바이트",
            japanese: "ガバイト",
            types: Types::Dual(Type::Dragon, Type::Ground),
        },
    );
    m.insert(
        445,
        Pokemon {
            number: 445,
            english: "Garchomp",
            mandarin: "烈咬陸鯊",
            french: "Carchacrok",
            german: "Knakrack",
            korean: "한카리아스",
            japanese: "ガブリアス",
            types: Types::Dual(Type::Dragon, Type::Ground),
        },
    );
    m.insert(
        446,
        Pokemon {
            number: 446,
            english: "Munchlax",
            mandarin: "小卡比獸",
            french: "Goinfrex",
            german: "Mampfaxo",
            korean: "먹고자",
            japanese: "ゴンベ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        447,
        Pokemon {
            number: 447,
            english: "Riolu",
            mandarin: "利歐路",
            french: "Riolu",
            german: "Riolu",
            korean: "리오르",
            japanese: "リオル",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        448,
        Pokemon {
            number: 448,
            english: "Lucario",
            mandarin: "路卡利歐",
            french: "Lucario",
            german: "Lucario",
            korean: "루카리오",
            japanese: "ルカリオ",
            types: Types::Dual(Type::Fighting, Type::Steel),
        },
    );
    m.insert(
        449,
        Pokemon {
            number: 449,
            english: "Hippopotas",
            mandarin: "沙河馬",
            french: "Hippopotas",
            german: "Hippopotas",
            korean: "히포포타스",
            japanese: "ヒポポタス",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        450,
        Pokemon {
            number: 450,
            english: "Hippowdon",
            mandarin: "河馬獸",
            french: "Hippodocus",
            german: "Hippoterus",
            korean: "하마돈",
            japanese: "カバルドン",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        451,
        Pokemon {
            number: 451,
            english: "Skorupi",
            mandarin: "鉗尾蠍",
            french: "Rapion",
            german: "Pionskora",
            korean: "스콜피",
            japanese: "スコルピ",
            types: Types::Dual(Type::Poison, Type::Bug),
        },
    );
    m.insert(
        452,
        Pokemon {
            number: 452,
            english: "Drapion",
            mandarin: "龍王蠍",
            french: "Drascore",
            german: "Piondragi",
            korean: "드래피온",
            japanese: "ドラピオン",
            types: Types::Dual(Type::Poison, Type::Dark),
        },
    );
    m.insert(
        453,
        Pokemon {
            number: 453,
            english: "Croagunk",
            mandarin: "不良蛙",
            french: "Cradopaud",
            german: "Glibunkel",
            korean: "삐딱구리",
            japanese: "グレッグル",
            types: Types::Dual(Type::Poison, Type::Fighting),
        },
    );
    m.insert(
        454,
        Pokemon {
            number: 454,
            english: "Toxicroak",
            mandarin: "毒骷蛙",
            french: "Coatox",
            german: "Toxiquak",
            korean: "독개굴",
            japanese: "ドクロッグ",
            types: Types::Dual(Type::Poison, Type::Fighting),
        },
    );
    m.insert(
        455,
        Pokemon {
            number: 455,
            english: "Carnivine",
            mandarin: "尖牙籠",
            french: "Vortente",
            german: "Venuflibis",
            korean: "무스틈니",
            japanese: "マスキッパ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        456,
        Pokemon {
            number: 456,
            english: "Finneon",
            mandarin: "螢光魚",
            french: "Écayon",
            german: "Finneon",
            korean: "형광어",
            japanese: "ケイコウオ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        457,
        Pokemon {
            number: 457,
            english: "Lumineon",
            mandarin: "霓虹魚",
            french: "Luminéon",
            german: "Lumineon",
            korean: "네오라이트",
            japanese: "ネオラント",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        458,
        Pokemon {
            number: 458,
            english: "Mantyke",
            mandarin: "小球飛魚",
            french: "Babimanta",
            german: "Mantirps",
            korean: "타만타",
            japanese: "タマンタ",
            types: Types::Dual(Type::Water, Type::Flying),
        },
    );
    m.insert(
        459,
        Pokemon {
            number: 459,
            english: "Snover",
            mandarin: "雪笠怪",
            french: "Blizzi",
            german: "Shnebedeck",
            korean: "눈쓰개",
            japanese: "ユキカブリ",
            types: Types::Dual(Type::Grass, Type::Ice),
        },
    );
    m.insert(
        460,
        Pokemon {
            number: 460,
            english: "Abomasnow",
            mandarin: "暴雪王",
            french: "Blizzaroi",
            german: "Rexblisar",
            korean: "눈설왕",
            japanese: "ユキノオー",
            types: Types::Dual(Type::Grass, Type::Ice),
        },
    );
    m.insert(
        461,
        Pokemon {
            number: 461,
            english: "Weavile",
            mandarin: "瑪狃拉",
            french: "Dimoret",
            german: "Snibunna",
            korean: "포푸니라",
            japanese: "マニューラ",
            types: Types::Dual(Type::Dark, Type::Ice),
        },
    );
    m.insert(
        462,
        Pokemon {
            number: 462,
            english: "Magnezone",
            mandarin: "自爆磁怪",
            french: "Magnézone",
            german: "Magnezone",
            korean: "자포코일",
            japanese: "ジバコイル",
            types: Types::Dual(Type::Electric, Type::Steel),
        },
    );
    m.insert(
        463,
        Pokemon {
            number: 463,
            english: "Lickilicky",
            mandarin: "大舌舔",
            french: "Coudlangue",
            german: "Schlurplek",
            korean: "내룸벨트",
            japanese: "ベロベルト",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        464,
        Pokemon {
            number: 464,
            english: "Rhyperior",
            mandarin: "超甲狂犀",
            french: "Rhinastoc",
            german: "Rihornior",
            korean: "거대코뿌리",
            japanese: "ドサイドン",
            types: Types::Dual(Type::Ground, Type::Rock),
        },
    );
    m.insert(
        465,
        Pokemon {
            number: 465,
            english: "Tangrowth",
            mandarin: "巨蔓藤",
            french: "Bouldeneu",
            german: "Tangoloss",
            korean: "덩쿠림보",
            japanese: "モジャンボ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        466,
        Pokemon {
            number: 466,
            english: "Electivire",
            mandarin: "電擊魔獸",
            french: "Élekable",
            german: "Elevoltek",
            korean: "에레키블",
            japanese: "エレキブル",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        467,
        Pokemon {
            number: 467,
            english: "Magmortar",
            mandarin: "鴨嘴炎獸",
            french: "Maganon",
            german: "Magbrant",
            korean: "마그마번",
            japanese: "ブーバーン",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        468,
        Pokemon {
            number: 468,
            english: "Togekiss",
            mandarin: "波克基斯",
            french: "Togekiss",
            german: "Togekiss",
            korean: "토게키스",
            japanese: "トゲキッス",
            types: Types::Dual(Type::Fairy, Type::Flying),
        },
    );
    m.insert(
        469,
        Pokemon {
            number: 469,
            english: "Yanmega",
            mandarin: "遠古巨蜓",
            french: "Yanmega",
            german: "Yanmega",
            korean: "메가자리",
            japanese: "メガヤンマ",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        470,
        Pokemon {
            number: 470,
            english: "Leafeon",
            mandarin: "葉伊布",
            french: "Phyllali",
            german: "Folipurba",
            korean: "리피아",
            japanese: "リーフィア",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        471,
        Pokemon {
            number: 471,
            english: "Glaceon",
            mandarin: "冰伊布",
            french: "Givrali",
            german: "Glaziola",
            korean: "글레이시아",
            japanese: "グレイシア",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        472,
        Pokemon {
            number: 472,
            english: "Gliscor",
            mandarin: "天蠍王",
            french: "Scorvol",
            german: "Skorgro",
            korean: "글라이온",
            japanese: "グライオン",
            types: Types::Dual(Type::Ground, Type::Flying),
        },
    );
    m.insert(
        473,
        Pokemon {
            number: 473,
            english: "Mamoswine",
            mandarin: "象牙豬",
            french: "Mammochon",
            german: "Mamutel",
            korean: "맘모꾸리",
            japanese: "マンムー",
            types: Types::Dual(Type::Ice, Type::Ground),
        },
    );
    m.insert(
        474,
        Pokemon {
            number: 474,
            english: "Porygon-Z",
            mandarin: "多邊獸Ｚ",
            french: "Porygon-Z",
            german: "Porygon-Z",
            korean: "폴리곤Z",
            japanese: "ポリゴンＺ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        475,
        Pokemon {
            number: 475,
            english: "Gallade",
            mandarin: "艾路雷朵",
            french: "Gallame",
            german: "Galagladi",
            korean: "엘레이드",
            japanese: "エルレイド",
            types: Types::Dual(Type::Psychic, Type::Fighting),
        },
    );
    m.insert(
        476,
        Pokemon {
            number: 476,
            english: "Probopass",
            mandarin: "大朝北鼻",
            french: "Tarinorme",
            german: "Voluminas",
            korean: "대코파스",
            japanese: "ダイノーズ",
            types: Types::Dual(Type::Rock, Type::Steel),
        },
    );
    m.insert(
        477,
        Pokemon {
            number: 477,
            english: "Dusknoir",
            mandarin: "黑夜魔靈",
            french: "Noctunoir",
            german: "Zwirrfinst",
            korean: "야느와르몽",
            japanese: "ヨノワール",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        478,
        Pokemon {
            number: 478,
            english: "Froslass",
            mandarin: "雪妖女",
            french: "Momartik",
            german: "Frosdedje",
            korean: "눈여아",
            japanese: "ユキメノコ",
            types: Types::Dual(Type::Ice, Type::Ghost),
        },
    );
    m.insert(
        479,
        Pokemon {
            number: 479,
            english: "Rotom",
            mandarin: "洛托姆",
            french: "Motisma",
            german: "Rotom",
            korean: "로토무",
            japanese: "ロトム",
            types: Types::Variants(vec![
                Types::Dual(Type::Electric, Type::Ghost),
                Types::Dual(Type::Electric, Type::Fire),
                Types::Dual(Type::Electric, Type::Water),
                Types::Dual(Type::Electric, Type::Ice),
                Types::Dual(Type::Electric, Type::Flying),
                Types::Dual(Type::Electric, Type::Grass),
            ]),
        },
    );
    m.insert(
        480,
        Pokemon {
            number: 480,
            english: "Uxie",
            mandarin: "由克希",
            french: "Créhelf",
            german: "Selfe",
            korean: "유크시",
            japanese: "ユクシー",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        481,
        Pokemon {
            number: 481,
            english: "Mesprit",
            mandarin: "艾姆利多",
            french: "Créfollet",
            german: "Vesprit",
            korean: "엠라이트",
            japanese: "エムリット",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        482,
        Pokemon {
            number: 482,
            english: "Azelf",
            mandarin: "亞克諾姆",
            french: "Créfadet",
            german: "Tobutz",
            korean: "아그놈",
            japanese: "アグノム",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        483,
        Pokemon {
            number: 483,
            english: "Dialga",
            mandarin: "帝牙盧卡",
            french: "Dialga",
            german: "Dialga",
            korean: "디아루가",
            japanese: "ディアルガ",
            types: Types::Dual(Type::Steel, Type::Dragon),
        },
    );
    m.insert(
        484,
        Pokemon {
            number: 484,
            english: "Palkia",
            mandarin: "帕路奇亞",
            french: "Palkia",
            german: "Palkia",
            korean: "펄기아",
            japanese: "パルキア",
            types: Types::Dual(Type::Water, Type::Dragon),
        },
    );
    m.insert(
        485,
        Pokemon {
            number: 485,
            english: "Heatran",
            mandarin: "席多藍恩",
            french: "Heatran",
            german: "Heatran",
            korean: "히드런",
            japanese: "ヒードラン",
            types: Types::Dual(Type::Fire, Type::Steel),
        },
    );
    m.insert(
        486,
        Pokemon {
            number: 486,
            english: "Regigigas",
            mandarin: "雷吉奇卡斯",
            french: "Regigigas",
            german: "Regigigas",
            korean: "레지기가스",
            japanese: "レジギガス",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        487,
        Pokemon {
            number: 487,
            english: "Giratina",
            mandarin: "騎拉帝納",
            french: "Giratina",
            german: "Giratina",
            korean: "기라티나",
            japanese: "ギラティナ",
            types: Types::Variants(vec![
                Types::Dual(Type::Ghost, Type::Dragon),
                Types::Dual(Type::Ghost, Type::Dragon),
            ]),
        },
    );
    m.insert(
        488,
        Pokemon {
            number: 488,
            english: "Cresselia",
            mandarin: "克雷色利亞",
            french: "Cresselia",
            german: "Cresselia",
            korean: "크레세리아",
            japanese: "クレセリア",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        489,
        Pokemon {
            number: 489,
            english: "Phione",
            mandarin: "霏歐納",
            french: "Phione",
            german: "Phione",
            korean: "피오네",
            japanese: "フィオネ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        490,
        Pokemon {
            number: 490,
            english: "Manaphy",
            mandarin: "瑪納霏",
            french: "Manaphy",
            german: "Manaphy",
            korean: "마나피",
            japanese: "マナフィ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        491,
        Pokemon {
            number: 491,
            english: "Darkrai",
            mandarin: "達克萊伊",
            french: "Darkrai",
            german: "Darkrai",
            korean: "다크라이",
            japanese: "ダークライ",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        492,
        Pokemon {
            number: 492,
            english: "Shaymin",
            mandarin: "謝米",
            french: "Shaymin",
            german: "Shaymin",
            korean: "쉐이미",
            japanese: "シェイミ",
            types: Types::Variants(vec![
                Types::Single(Type::Grass),
                Types::Dual(Type::Grass, Type::Flying),
            ]),
        },
    );
    m.insert(
        493,
        Pokemon {
            number: 493,
            english: "Arceus",
            mandarin: "阿爾宙斯",
            french: "Arceus",
            german: "Arceus",
            korean: "아르세우스",
            japanese: "アルセウス",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        494,
        Pokemon {
            number: 494,
            english: "Victini",
            mandarin: "比克提尼",
            french: "Victini",
            german: "Victini",
            korean: "비크티니",
            japanese: "ビクティニ",
            types: Types::Dual(Type::Psychic, Type::Fire),
        },
    );
    m.insert(
        495,
        Pokemon {
            number: 495,
            english: "Snivy",
            mandarin: "藤藤蛇",
            french: "Vipélierre",
            german: "Serpifeu",
            korean: "주리비얀",
            japanese: "ツタージャ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        496,
        Pokemon {
            number: 496,
            english: "Servine",
            mandarin: "青藤蛇",
            french: "Lianaja",
            german: "Efoserp",
            korean: "샤비",
            japanese: "ジャノビー",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        497,
        Pokemon {
            number: 497,
            english: "Serperior",
            mandarin: "君主蛇",
            french: "Majaspic",
            german: "Serpiroyal",
            korean: "샤로다",
            japanese: "ジャローダ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        498,
        Pokemon {
            number: 498,
            english: "Tepig",
            mandarin: "暖暖豬",
            french: "Gruikui",
            german: "Floink",
            korean: "뚜꾸리",
            japanese: "ポカブ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        499,
        Pokemon {
            number: 499,
            english: "Pignite",
            mandarin: "炒炒豬",
            french: "Grotichon",
            german: "Ferkokel",
            korean: "챠오꿀",
            japanese: "チャオブー",
            types: Types::Dual(Type::Fire, Type::Fighting),
        },
    );
    m.insert(
        500,
        Pokemon {
            number: 500,
            english: "Emboar",
            mandarin: "炎武王",
            french: "Roitiflam",
            german: "Flambirex",
            korean: "염무왕",
            japanese: "エンブオー",
            types: Types::Dual(Type::Fire, Type::Fighting),
        },
    );
    m.insert(
        501,
        Pokemon {
            number: 501,
            english: "Oshawott",
            mandarin: "水水獺",
            french: "Moustillon",
            german: "Ottaro",
            korean: "수댕이",
            japanese: "ミジュマル",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        502,
        Pokemon {
            number: 502,
            english: "Dewott",
            mandarin: "雙刃丸",
            french: "Mateloutre",
            german: "Zwottronin",
            korean: "쌍검자비",
            japanese: "フタチマル",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        503,
        Pokemon {
            number: 503,
            english: "Samurott",
            mandarin: "大劍鬼",
            french: "Clamiral",
            german: "Admurai",
            korean: "대검귀",
            japanese: "ダイケンキ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        504,
        Pokemon {
            number: 504,
            english: "Patrat",
            mandarin: "探探鼠",
            french: "Ratentif",
            german: "Nagelotz",
            korean: "보르쥐",
            japanese: "ミネズミ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        505,
        Pokemon {
            number: 505,
            english: "Watchog",
            mandarin: "步哨鼠",
            french: "Miradar",
            german: "Kukmarda",
            korean: "보르그",
            japanese: "ミルホッグ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        506,
        Pokemon {
            number: 506,
            english: "Lillipup",
            mandarin: "小約克",
            french: "Ponchiot",
            german: "Yorkleff",
            korean: "요테리",
            japanese: "ヨーテリー",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        507,
        Pokemon {
            number: 507,
            english: "Herdier",
            mandarin: "哈約克",
            french: "Ponchien",
            german: "Terribark",
            korean: "하데리어",
            japanese: "ハーデリア",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        508,
        Pokemon {
            number: 508,
            english: "Stoutland",
            mandarin: "長毛狗",
            french: "Mastouffe",
            german: "Bissbark",
            korean: "바랜드",
            japanese: "ムーランド",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        509,
        Pokemon {
            number: 509,
            english: "Purrloin",
            mandarin: "扒手貓",
            french: "Chacripan",
            german: "Felilou",
            korean: "쌔비냥",
            japanese: "チョロネコ",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        510,
        Pokemon {
            number: 510,
            english: "Liepard",
            mandarin: "酷豹",
            french: "Léopardus",
            german: "Kleoparda",
            korean: "레파르다스",
            japanese: "レパルダス",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        511,
        Pokemon {
            number: 511,
            english: "Pansage",
            mandarin: "花椰猴",
            french: "Feuillajou",
            german: "Vegimak",
            korean: "야나프",
            japanese: "ヤナップ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        512,
        Pokemon {
            number: 512,
            english: "Simisage",
            mandarin: "花椰猿",
            french: "Feuiloutan",
            german: "Vegichita",
            korean: "야나키",
            japanese: "ヤナッキー",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        513,
        Pokemon {
            number: 513,
            english: "Pansear",
            mandarin: "爆香猴",
            french: "Flamajou",
            german: "Grillmak",
            korean: "바오프",
            japanese: "バオップ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        514,
        Pokemon {
            number: 514,
            english: "Simisear",
            mandarin: "爆香猿",
            french: "Flamoutan",
            german: "Grillchita",
            korean: "바오키",
            japanese: "バオッキー",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        515,
        Pokemon {
            number: 515,
            english: "Panpour",
            mandarin: "冷水猴",
            french: "Flotajou",
            german: "Sodamak",
            korean: "앗차프",
            japanese: "ヒヤップ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        516,
        Pokemon {
            number: 516,
            english: "Simipour",
            mandarin: "冷水猿",
            french: "Flotoutan",
            german: "Sodachita",
            korean: "앗차키",
            japanese: "ヒヤッキー",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        517,
        Pokemon {
            number: 517,
            english: "Munna",
            mandarin: "食夢夢",
            french: "Munna",
            german: "Somniam",
            korean: "몽나",
            japanese: "ムンナ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        518,
        Pokemon {
            number: 518,
            english: "Musharna",
            mandarin: "夢夢蝕",
            french: "Mushana",
            german: "Somnivora",
            korean: "몽얌나",
            japanese: "ムシャーナ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        519,
        Pokemon {
            number: 519,
            english: "Pidove",
            mandarin: "豆豆鴿",
            french: "Poichigeon",
            german: "Dusselgurr",
            korean: "콩둘기",
            japanese: "マメパト",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        520,
        Pokemon {
            number: 520,
            english: "Tranquill",
            mandarin: "咕咕鴿",
            french: "Colombeau",
            german: "Navitaub",
            korean: "유토브",
            japanese: "ハトーボー",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        521,
        Pokemon {
            number: 521,
            english: "Unfezant",
            mandarin: "高傲雉雞",
            french: "Déflaisan",
            german: "Fasasnob",
            korean: "켄호로우",
            japanese: "ケンホロウ",
            types: Types::Variants(vec![
                Types::Dual(Type::Normal, Type::Flying),
                Types::Dual(Type::Normal, Type::Flying),
            ]),
        },
    );
    m.insert(
        522,
        Pokemon {
            number: 522,
            english: "Blitzle",
            mandarin: "斑斑馬",
            french: "Zébibron",
            german: "Elezeba",
            korean: "줄뮤마",
            japanese: "シママ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        523,
        Pokemon {
            number: 523,
            english: "Zebstrika",
            mandarin: "雷電斑馬",
            french: "Zéblitz",
            german: "Zebritz",
            korean: "제브라이카",
            japanese: "ゼブライカ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        524,
        Pokemon {
            number: 524,
            english: "Roggenrola",
            mandarin: "石丸子",
            french: "Nodulithe",
            german: "Kiesling",
            korean: "단굴",
            japanese: "ダンゴロ",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        525,
        Pokemon {
            number: 525,
            english: "Boldore",
            mandarin: "地幔岩",
            french: "Géolithe",
            german: "Sedimantur",
            korean: "암트르",
            japanese: "ガントル",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        526,
        Pokemon {
            number: 526,
            english: "Gigalith",
            mandarin: "龐岩怪",
            french: "Gigalithe",
            german: "Brockoloss",
            korean: "기가이어스",
            japanese: "ギガイアス",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        527,
        Pokemon {
            number: 527,
            english: "Woobat",
            mandarin: "滾滾蝙蝠",
            french: "Chovsourir",
            german: "Fleknoil",
            korean: "또르박쥐",
            japanese: "コロモリ",
            types: Types::Dual(Type::Psychic, Type::Flying),
        },
    );
    m.insert(
        528,
        Pokemon {
            number: 528,
            english: "Swoobat",
            mandarin: "心蝙蝠",
            french: "Rhinolove",
            german: "Fletiamo",
            korean: "맘박쥐",
            japanese: "ココロモリ",
            types: Types::Dual(Type::Psychic, Type::Flying),
        },
    );
    m.insert(
        529,
        Pokemon {
            number: 529,
            english: "Drilbur",
            mandarin: "螺釘地鼠",
            french: "Rototaupe",
            german: "Rotomurf",
            korean: "두더류",
            japanese: "モグリュー",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        530,
        Pokemon {
            number: 530,
            english: "Excadrill",
            mandarin: "龍頭地鼠",
            french: "Minotaupe",
            german: "Stalobor",
            korean: "몰드류",
            japanese: "ドリュウズ",
            types: Types::Dual(Type::Ground, Type::Steel),
        },
    );
    m.insert(
        531,
        Pokemon {
            number: 531,
            english: "Audino",
            mandarin: "差不多娃娃",
            french: "Nanméouïe",
            german: "Ohrdoch",
            korean: "다부니",
            japanese: "タブンネ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        532,
        Pokemon {
            number: 532,
            english: "Timburr",
            mandarin: "搬運小匠",
            french: "Charpenti",
            german: "Praktibalk",
            korean: "으랏차",
            japanese: "ドッコラー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        533,
        Pokemon {
            number: 533,
            english: "Gurdurr",
            mandarin: "鐵骨土人",
            french: "Ouvrifier",
            german: "Strepoli",
            korean: "토쇠골",
            japanese: "ドテッコツ",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        534,
        Pokemon {
            number: 534,
            english: "Conkeldurr",
            mandarin: "修建老匠",
            french: "Bétochef",
            german: "Meistagrif",
            korean: "노보청",
            japanese: "ローブシン",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        535,
        Pokemon {
            number: 535,
            english: "Tympole",
            mandarin: "圓蝌蚪",
            french: "Tritonde",
            german: "Schallquap",
            korean: "동챙이",
            japanese: "オタマロ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        536,
        Pokemon {
            number: 536,
            english: "Palpitoad",
            mandarin: "藍蟾蜍",
            french: "Batracné",
            german: "Mebrana",
            korean: "두까비",
            japanese: "ガマガル",
            types: Types::Dual(Type::Water, Type::Ground),
        },
    );
    m.insert(
        537,
        Pokemon {
            number: 537,
            english: "Seismitoad",
            mandarin: "蟾蜍王",
            french: "Crapustule",
            german: "Branawarz",
            korean: "두빅굴",
            japanese: "ガマゲロゲ",
            types: Types::Dual(Type::Water, Type::Ground),
        },
    );
    m.insert(
        538,
        Pokemon {
            number: 538,
            english: "Throh",
            mandarin: "投摔鬼",
            french: "Judokrak",
            german: "Jiutesto",
            korean: "던지미",
            japanese: "ナゲキ",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        539,
        Pokemon {
            number: 539,
            english: "Sawk",
            mandarin: "打擊鬼",
            french: "Karaclée",
            german: "Karadonis",
            korean: "타격귀",
            japanese: "ダゲキ",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        540,
        Pokemon {
            number: 540,
            english: "Sewaddle",
            mandarin: "蟲寶包",
            french: "Larveyette",
            german: "Strawickl",
            korean: "두르보",
            japanese: "クルミル",
            types: Types::Dual(Type::Bug, Type::Grass),
        },
    );
    m.insert(
        541,
        Pokemon {
            number: 541,
            english: "Swadloon",
            mandarin: "寶包繭",
            french: "Couverdure",
            german: "Folikon",
            korean: "두르쿤",
            japanese: "クルマユ",
            types: Types::Dual(Type::Bug, Type::Grass),
        },
    );
    m.insert(
        542,
        Pokemon {
            number: 542,
            english: "Leavanny",
            mandarin: "保母蟲",
            french: "Manternel",
            german: "Matrifol",
            korean: "모아머",
            japanese: "ハハコモリ",
            types: Types::Dual(Type::Bug, Type::Grass),
        },
    );
    m.insert(
        543,
        Pokemon {
            number: 543,
            english: "Venipede",
            mandarin: "百足蜈蚣",
            french: "Venipatte",
            german: "Toxiped",
            korean: "마디네",
            japanese: "フシデ",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        544,
        Pokemon {
            number: 544,
            english: "Whirlipede",
            mandarin: "車輪毬",
            french: "Scobolide",
            german: "Rollum",
            korean: "휠구",
            japanese: "ホイーガ",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        545,
        Pokemon {
            number: 545,
            english: "Scolipede",
            mandarin: "蜈蚣王",
            french: "Brutapode",
            german: "Cerapendra",
            korean: "펜드라",
            japanese: "ペンドラー",
            types: Types::Dual(Type::Bug, Type::Poison),
        },
    );
    m.insert(
        546,
        Pokemon {
            number: 546,
            english: "Cottonee",
            mandarin: "木棉球",
            french: "Doudouvet",
            german: "Waumboll",
            korean: "소미안",
            japanese: "モンメン",
            types: Types::Dual(Type::Grass, Type::Fairy),
        },
    );
    m.insert(
        547,
        Pokemon {
            number: 547,
            english: "Whimsicott",
            mandarin: "風妖精",
            french: "Farfaduvet",
            german: "Elfun",
            korean: "엘풍",
            japanese: "エルフーン",
            types: Types::Dual(Type::Grass, Type::Fairy),
        },
    );
    m.insert(
        548,
        Pokemon {
            number: 548,
            english: "Petilil",
            mandarin: "百合根娃娃",
            french: "Chlorobule",
            german: "Lilminip",
            korean: "치릴리",
            japanese: "チュリネ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        549,
        Pokemon {
            number: 549,
            english: "Lilligant",
            mandarin: "裙兒小姐",
            french: "Fragilady",
            german: "Dressella",
            korean: "드레디어",
            japanese: "ドレディア",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        550,
        Pokemon {
            number: 550,
            english: "Basculin",
            mandarin: "野蠻鱸魚",
            french: "Bargantua",
            german: "Barschuft",
            korean: "배쓰나이",
            japanese: "バスラオ",
            types: Types::Variants(vec![Types::Single(Type::Water), Types::Single(Type::Water)]),
        },
    );
    m.insert(
        551,
        Pokemon {
            number: 551,
            english: "Sandile",
            mandarin: "黑眼鱷",
            french: "Mascaïman",
            german: "Ganovil",
            korean: "깜눈크",
            japanese: "メグロコ",
            types: Types::Dual(Type::Ground, Type::Dark),
        },
    );
    m.insert(
        552,
        Pokemon {
            number: 552,
            english: "Krokorok",
            mandarin: "混混鱷",
            french: "Escroco",
            german: "Rokkaiman",
            korean: "악비르",
            japanese: "ワルビル",
            types: Types::Dual(Type::Ground, Type::Dark),
        },
    );
    m.insert(
        553,
        Pokemon {
            number: 553,
            english: "Krookodile",
            mandarin: "流氓鱷",
            french: "Crocorible",
            german: "Rabigator",
            korean: "악비아르",
            japanese: "ワルビアル",
            types: Types::Dual(Type::Ground, Type::Dark),
        },
    );
    m.insert(
        554,
        Pokemon {
            number: 554,
            english: "Darumaka",
            mandarin: "火紅不倒翁",
            french: "Darumarond",
            german: "Flampion",
            korean: "달막화",
            japanese: "ダルマッカ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        555,
        Pokemon {
            number: 555,
            english: "Darmanitan",
            mandarin: "達摩狒狒",
            french: "Darumacho",
            german: "Flampivian",
            korean: "불비달마",
            japanese: "ヒヒダルマ",
            types: Types::Variants(vec![
                Types::Single(Type::Fire),
                Types::Dual(Type::Fire, Type::Psychic),
            ]),
        },
    );
    m.insert(
        556,
        Pokemon {
            number: 556,
            english: "Maractus",
            mandarin: "沙鈴仙人掌",
            french: "Maracachi",
            german: "Maracamba",
            korean: "마라카치",
            japanese: "マラカッチ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        557,
        Pokemon {
            number: 557,
            english: "Dwebble",
            mandarin: "石居蟹",
            french: "Crabicoque",
            german: "Lithomith",
            korean: "돌살이",
            japanese: "イシズマイ",
            types: Types::Dual(Type::Bug, Type::Rock),
        },
    );
    m.insert(
        558,
        Pokemon {
            number: 558,
            english: "Crustle",
            mandarin: "岩殿居蟹",
            french: "Crabaraque",
            german: "Castellith",
            korean: "암팰리스",
            japanese: "イワパレス",
            types: Types::Dual(Type::Bug, Type::Rock),
        },
    );
    m.insert(
        559,
        Pokemon {
            number: 559,
            english: "Scraggy",
            mandarin: "滑滑小子",
            french: "Baggiguane",
            german: "Zurrokex",
            korean: "곤율랭",
            japanese: "ズルッグ",
            types: Types::Dual(Type::Dark, Type::Fighting),
        },
    );
    m.insert(
        560,
        Pokemon {
            number: 560,
            english: "Scrafty",
            mandarin: "頭巾混混",
            french: "Baggaïd",
            german: "Irokex",
            korean: "곤율거니",
            japanese: "ズルズキン",
            types: Types::Dual(Type::Dark, Type::Fighting),
        },
    );
    m.insert(
        561,
        Pokemon {
            number: 561,
            english: "Sigilyph",
            mandarin: "象徵鳥",
            french: "Cryptéro",
            german: "Symvolara",
            korean: "심보러",
            japanese: "シンボラー",
            types: Types::Dual(Type::Psychic, Type::Flying),
        },
    );
    m.insert(
        562,
        Pokemon {
            number: 562,
            english: "Yamask",
            mandarin: "哭哭面具",
            french: "Tutafeh",
            german: "Makabaja",
            korean: "데스마스",
            japanese: "デスマス",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        563,
        Pokemon {
            number: 563,
            english: "Cofagrigus",
            mandarin: "死神棺",
            french: "Tutankafer",
            german: "Echnatoll",
            korean: "데스니칸",
            japanese: "デスカーン",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        564,
        Pokemon {
            number: 564,
            english: "Tirtouga",
            mandarin: "原蓋海龜",
            french: "Carapagos",
            german: "Galapaflos",
            korean: "프로토가",
            japanese: "プロトーガ",
            types: Types::Dual(Type::Water, Type::Rock),
        },
    );
    m.insert(
        565,
        Pokemon {
            number: 565,
            english: "Carracosta",
            mandarin: "肋骨海龜",
            french: "Mégapagos",
            german: "Karippas",
            korean: "늑골라",
            japanese: "アバゴーラ",
            types: Types::Dual(Type::Water, Type::Rock),
        },
    );
    m.insert(
        566,
        Pokemon {
            number: 566,
            english: "Archen",
            mandarin: "始祖小鳥",
            french: "Arkéapti",
            german: "Flapteryx",
            korean: "아켄",
            japanese: "アーケン",
            types: Types::Dual(Type::Rock, Type::Flying),
        },
    );
    m.insert(
        567,
        Pokemon {
            number: 567,
            english: "Archeops",
            mandarin: "始祖大鳥",
            french: "Aéroptéryx",
            german: "Aeropteryx",
            korean: "아케오스",
            japanese: "アーケオス",
            types: Types::Dual(Type::Rock, Type::Flying),
        },
    );
    m.insert(
        568,
        Pokemon {
            number: 568,
            english: "Trubbish",
            mandarin: "破破袋",
            french: "Miamiasme",
            german: "Unratütox",
            korean: "깨봉이",
            japanese: "ヤブクロン",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        569,
        Pokemon {
            number: 569,
            english: "Garbodor",
            mandarin: "灰塵山",
            french: "Miasmax",
            german: "Deponitox",
            korean: "더스트나",
            japanese: "ダストダス",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        570,
        Pokemon {
            number: 570,
            english: "Zorua",
            mandarin: "索羅亞",
            french: "Zorua",
            german: "Zorua",
            korean: "조로아",
            japanese: "ゾロア",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        571,
        Pokemon {
            number: 571,
            english: "Zoroark",
            mandarin: "索羅亞克",
            french: "Zoroark",
            german: "Zoroark",
            korean: "조로아크",
            japanese: "ゾロアーク",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        572,
        Pokemon {
            number: 572,
            english: "Minccino",
            mandarin: "泡沫栗鼠",
            french: "Chinchidou",
            german: "Picochilla",
            korean: "치라미",
            japanese: "チラーミィ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        573,
        Pokemon {
            number: 573,
            english: "Cinccino",
            mandarin: "奇諾栗鼠",
            french: "Pashmilla",
            german: "Chillabell",
            korean: "치라치노",
            japanese: "チラチーノ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        574,
        Pokemon {
            number: 574,
            english: "Gothita",
            mandarin: "哥德寶寶",
            french: "Scrutella",
            german: "Mollimorba",
            korean: "고디탱",
            japanese: "ゴチム",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        575,
        Pokemon {
            number: 575,
            english: "Gothorita",
            mandarin: "哥德小童",
            french: "Mesmérella",
            german: "Hypnomorba",
            korean: "고디보미",
            japanese: "ゴチミル",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        576,
        Pokemon {
            number: 576,
            english: "Gothitelle",
            mandarin: "哥德小姐",
            french: "Sidérella",
            german: "Morbitesse",
            korean: "고디모아젤",
            japanese: "ゴチルゼル",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        577,
        Pokemon {
            number: 577,
            english: "Solosis",
            mandarin: "單卵細胞球",
            french: "Nucléos",
            german: "Monozyto",
            korean: "유니란",
            japanese: "ユニラン",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        578,
        Pokemon {
            number: 578,
            english: "Duosion",
            mandarin: "雙卵細胞球",
            french: "Méios",
            german: "Mitodos",
            korean: "듀란",
            japanese: "ダブラン",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        579,
        Pokemon {
            number: 579,
            english: "Reuniclus",
            mandarin: "人造細胞卵",
            french: "Symbios",
            german: "Zytomega",
            korean: "란쿨루스",
            japanese: "ランクルス",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        580,
        Pokemon {
            number: 580,
            english: "Ducklett",
            mandarin: "鴨寶寶",
            french: "Couaneton",
            german: "Piccolente",
            korean: "꼬지보리",
            japanese: "コアルヒー",
            types: Types::Dual(Type::Water, Type::Flying),
        },
    );
    m.insert(
        581,
        Pokemon {
            number: 581,
            english: "Swanna",
            mandarin: "舞天鵝",
            french: "Lakmécygne",
            german: "Swaroness",
            korean: "스완나",
            japanese: "スワンナ",
            types: Types::Dual(Type::Water, Type::Flying),
        },
    );
    m.insert(
        582,
        Pokemon {
            number: 582,
            english: "Vanillite",
            mandarin: "迷你冰",
            french: "Sorbébé",
            german: "Gelatini",
            korean: "바닐프티",
            japanese: "バニプッチ",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        583,
        Pokemon {
            number: 583,
            english: "Vanillish",
            mandarin: "多多冰",
            french: "Sorboul",
            german: "Gelatroppo",
            korean: "바닐리치",
            japanese: "バニリッチ",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        584,
        Pokemon {
            number: 584,
            english: "Vanilluxe",
            mandarin: "雙倍多多冰",
            french: "Sorbouboul",
            german: "Gelatwino",
            korean: "배바닐라",
            japanese: "バイバニラ",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        585,
        Pokemon {
            number: 585,
            english: "Deerling",
            mandarin: "四季鹿",
            french: "Vivaldaim",
            german: "Sesokitz",
            korean: "사철록",
            japanese: "シキジカ",
            types: Types::Dual(Type::Normal, Type::Grass),
        },
    );
    m.insert(
        586,
        Pokemon {
            number: 586,
            english: "Sawsbuck",
            mandarin: "萌芽鹿",
            french: "Haydaim",
            german: "Kronjuwild",
            korean: "바라철록",
            japanese: "メブキジカ",
            types: Types::Dual(Type::Normal, Type::Grass),
        },
    );
    m.insert(
        587,
        Pokemon {
            number: 587,
            english: "Emolga",
            mandarin: "電飛鼠",
            french: "Emolga",
            german: "Emolga",
            korean: "에몽가",
            japanese: "エモンガ",
            types: Types::Dual(Type::Electric, Type::Flying),
        },
    );
    m.insert(
        588,
        Pokemon {
            number: 588,
            english: "Karrablast",
            mandarin: "蓋蓋蟲",
            french: "Carabing",
            german: "Laukaps",
            korean: "딱정곤",
            japanese: "カブルモ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        589,
        Pokemon {
            number: 589,
            english: "Escavalier",
            mandarin: "騎士蝸牛",
            french: "Lançargot",
            german: "Cavalanzas",
            korean: "슈바르고",
            japanese: "シュバルゴ",
            types: Types::Dual(Type::Bug, Type::Steel),
        },
    );
    m.insert(
        590,
        Pokemon {
            number: 590,
            english: "Foongus",
            mandarin: "哎呀球菇",
            french: "Trompignon",
            german: "Tarnpignon",
            korean: "깜놀버슬",
            japanese: "タマゲタケ",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        591,
        Pokemon {
            number: 591,
            english: "Amoonguss",
            mandarin: "敗露球菇",
            french: "Gaulet",
            german: "Hutsassa",
            korean: "뽀록나",
            japanese: "モロバレル",
            types: Types::Dual(Type::Grass, Type::Poison),
        },
    );
    m.insert(
        592,
        Pokemon {
            number: 592,
            english: "Frillish",
            mandarin: "輕飄飄",
            french: "Viskuse",
            german: "Quabbel",
            korean: "탱그릴",
            japanese: "プルリル",
            types: Types::Variants(vec![
                Types::Dual(Type::Water, Type::Ghost),
                Types::Dual(Type::Water, Type::Ghost),
            ]),
        },
    );
    m.insert(
        593,
        Pokemon {
            number: 593,
            english: "Jellicent",
            mandarin: "胖嘟嘟",
            french: "Moyade",
            german: "Apoquallyp",
            korean: "탱탱겔",
            japanese: "ブルンゲル",
            types: Types::Variants(vec![
                Types::Dual(Type::Water, Type::Ghost),
                Types::Dual(Type::Water, Type::Ghost),
            ]),
        },
    );
    m.insert(
        594,
        Pokemon {
            number: 594,
            english: "Alomomola",
            mandarin: "保母曼波",
            french: "Mamanbo",
            german: "Mamolida",
            korean: "맘복치",
            japanese: "ママンボウ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        595,
        Pokemon {
            number: 595,
            english: "Joltik",
            mandarin: "電電蟲",
            french: "Statitik",
            german: "Wattzapf",
            korean: "피쪼옥",
            japanese: "バチュル",
            types: Types::Dual(Type::Bug, Type::Electric),
        },
    );
    m.insert(
        596,
        Pokemon {
            number: 596,
            english: "Galvantula",
            mandarin: "電蜘蛛",
            french: "Mygavolt",
            german: "Voltula",
            korean: "전툴라",
            japanese: "デンチュラ",
            types: Types::Dual(Type::Bug, Type::Electric),
        },
    );
    m.insert(
        597,
        Pokemon {
            number: 597,
            english: "Ferroseed",
            mandarin: "種子鐵球",
            french: "Grindur",
            german: "Kastadur",
            korean: "철시드",
            japanese: "テッシード",
            types: Types::Dual(Type::Grass, Type::Steel),
        },
    );
    m.insert(
        598,
        Pokemon {
            number: 598,
            english: "Ferrothorn",
            mandarin: "堅果啞鈴",
            french: "Noacier",
            german: "Tentantel",
            korean: "너트령",
            japanese: "ナットレイ",
            types: Types::Dual(Type::Grass, Type::Steel),
        },
    );
    m.insert(
        599,
        Pokemon {
            number: 599,
            english: "Klink",
            mandarin: "齒輪兒",
            french: "Tic",
            german: "Klikk",
            korean: "기어르",
            japanese: "ギアル",
            types: Types::Single(Type::Steel),
        },
    );
    m.insert(
        600,
        Pokemon {
            number: 600,
            english: "Klang",
            mandarin: "齒輪組",
            french: "Clic",
            german: "Kliklak",
            korean: "기기어르",
            japanese: "ギギアル",
            types: Types::Single(Type::Steel),
        },
    );
    m.insert(
        601,
        Pokemon {
            number: 601,
            english: "Klinklang",
            mandarin: "齒輪怪",
            french: "Cliticlic",
            german: "Klikdiklak",
            korean: "기기기어르",
            japanese: "ギギギアル",
            types: Types::Single(Type::Steel),
        },
    );
    m.insert(
        602,
        Pokemon {
            number: 602,
            english: "Tynamo",
            mandarin: "麻麻小魚",
            french: "Anchwatt",
            german: "Zapplardin",
            korean: "저리어",
            japanese: "シビシラス",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        603,
        Pokemon {
            number: 603,
            english: "Eelektrik",
            mandarin: "麻麻鰻",
            french: "Lampéroie",
            german: "Zapplalek",
            korean: "저리릴",
            japanese: "シビビール",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        604,
        Pokemon {
            number: 604,
            english: "Eelektross",
            mandarin: "麻麻鰻魚王",
            french: "Ohmassacre",
            german: "Zapplarang",
            korean: "저리더프",
            japanese: "シビルドン",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        605,
        Pokemon {
            number: 605,
            english: "Elgyem",
            mandarin: "小灰怪",
            french: "Lewsor",
            german: "Pygraulon",
            korean: "리그레",
            japanese: "リグレー",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        606,
        Pokemon {
            number: 606,
            english: "Beheeyem",
            mandarin: "大宇怪",
            french: "Neitram",
            german: "Megalon",
            korean: "벰크",
            japanese: "オーベム",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        607,
        Pokemon {
            number: 607,
            english: "Litwick",
            mandarin: "燭光靈",
            french: "Funécire",
            german: "Lichtel",
            korean: "불켜미",
            japanese: "ヒトモシ",
            types: Types::Dual(Type::Ghost, Type::Fire),
        },
    );
    m.insert(
        608,
        Pokemon {
            number: 608,
            english: "Lampent",
            mandarin: "燈火幽靈",
            french: "Mélancolux",
            german: "Laternecto",
            korean: "램프라",
            japanese: "ランプラー",
            types: Types::Dual(Type::Ghost, Type::Fire),
        },
    );
    m.insert(
        609,
        Pokemon {
            number: 609,
            english: "Chandelure",
            mandarin: "水晶燈火靈",
            french: "Lugulabre",
            german: "Skelabra",
            korean: "샹델라",
            japanese: "シャンデラ",
            types: Types::Dual(Type::Ghost, Type::Fire),
        },
    );
    m.insert(
        610,
        Pokemon {
            number: 610,
            english: "Axew",
            mandarin: "牙牙",
            french: "Coupenotte",
            german: "Milza",
            korean: "터검니",
            japanese: "キバゴ",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        611,
        Pokemon {
            number: 611,
            english: "Fraxure",
            mandarin: "斧牙龍",
            french: "Incisache",
            german: "Sharfax",
            korean: "액슨도",
            japanese: "オノンド",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        612,
        Pokemon {
            number: 612,
            english: "Haxorus",
            mandarin: "雙斧戰龍",
            french: "Tranchodon",
            german: "Maxax",
            korean: "액스라이즈",
            japanese: "オノノクス",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        613,
        Pokemon {
            number: 613,
            english: "Cubchoo",
            mandarin: "噴嚏熊",
            french: "Polarhume",
            german: "Petznief",
            korean: "코고미",
            japanese: "クマシュン",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        614,
        Pokemon {
            number: 614,
            english: "Beartic",
            mandarin: "凍原熊",
            french: "Polagriffe",
            german: "Siberio",
            korean: "툰베어",
            japanese: "ツンベアー",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        615,
        Pokemon {
            number: 615,
            english: "Cryogonal",
            mandarin: "幾何雪花",
            french: "Hexagel",
            german: "Frigometri",
            korean: "프리지오",
            japanese: "フリージオ",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        616,
        Pokemon {
            number: 616,
            english: "Shelmet",
            mandarin: "小嘴蝸",
            french: "Escargaume",
            german: "Schnuthelm",
            korean: "쪼마리",
            japanese: "チョボマキ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        617,
        Pokemon {
            number: 617,
            english: "Accelgor",
            mandarin: "敏捷蟲",
            french: "Limaspeed",
            german: "Hydragil",
            korean: "어지리더",
            japanese: "アギルダー",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        618,
        Pokemon {
            number: 618,
            english: "Stunfisk",
            mandarin: "泥巴魚",
            french: "Limonde",
            german: "Flunschlik",
            korean: "메더",
            japanese: "マッギョ",
            types: Types::Dual(Type::Ground, Type::Electric),
        },
    );
    m.insert(
        619,
        Pokemon {
            number: 619,
            english: "Mienfoo",
            mandarin: "功夫鼬",
            french: "Kungfouine",
            german: "Lin-Fu",
            korean: "비조푸",
            japanese: "コジョフー",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        620,
        Pokemon {
            number: 620,
            english: "Mienshao",
            mandarin: "師父鼬",
            french: "Shaofouine",
            german: "Wie-Shu",
            korean: "비조도",
            japanese: "コジョンド",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        621,
        Pokemon {
            number: 621,
            english: "Druddigon",
            mandarin: "赤面龍",
            french: "Drakkarmin",
            german: "Shardrago",
            korean: "크리만",
            japanese: "クリムガン",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        622,
        Pokemon {
            number: 622,
            english: "Golett",
            mandarin: "泥偶小人",
            french: "Gringolem",
            german: "Golbit",
            korean: "골비람",
            japanese: "ゴビット",
            types: Types::Dual(Type::Ground, Type::Ghost),
        },
    );
    m.insert(
        623,
        Pokemon {
            number: 623,
            english: "Golurk",
            mandarin: "泥偶巨人",
            french: "Golemastoc",
            german: "Golgantes",
            korean: "골루그",
            japanese: "ゴルーグ",
            types: Types::Dual(Type::Ground, Type::Ghost),
        },
    );
    m.insert(
        624,
        Pokemon {
            number: 624,
            english: "Pawniard",
            mandarin: "駒刀小兵",
            french: "Scalpion",
            german: "Gladiantri",
            korean: "자망칼",
            japanese: "コマタナ",
            types: Types::Dual(Type::Dark, Type::Steel),
        },
    );
    m.insert(
        625,
        Pokemon {
            number: 625,
            english: "Bisharp",
            mandarin: "劈斬司令",
            french: "Scalproie",
            german: "Caesurio",
            korean: "절각참",
            japanese: "キリキザン",
            types: Types::Dual(Type::Dark, Type::Steel),
        },
    );
    m.insert(
        626,
        Pokemon {
            number: 626,
            english: "Bouffalant",
            mandarin: "爆炸頭水牛",
            french: "Frison",
            german: "Bisofank",
            korean: "버프론",
            japanese: "バッフロン",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        627,
        Pokemon {
            number: 627,
            english: "Rufflet",
            mandarin: "毛頭小鷹",
            french: "Furaiglon",
            german: "Geronimatz",
            korean: "수리둥보",
            japanese: "ワシボン",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        628,
        Pokemon {
            number: 628,
            english: "Braviary",
            mandarin: "勇士雄鷹",
            french: "Gueriaigle",
            german: "Washakwil",
            korean: "워글",
            japanese: "ウォーグル",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        629,
        Pokemon {
            number: 629,
            english: "Vullaby",
            mandarin: "禿鷹丫頭",
            french: "Vostourno",
            german: "Skallyk",
            korean: "벌차이",
            japanese: "バルチャイ",
            types: Types::Dual(Type::Dark, Type::Flying),
        },
    );
    m.insert(
        630,
        Pokemon {
            number: 630,
            english: "Mandibuzz",
            mandarin: "禿鷹娜",
            french: "Vaututrice",
            german: "Grypheldis",
            korean: "버랜지나",
            japanese: "バルジーナ",
            types: Types::Dual(Type::Dark, Type::Flying),
        },
    );
    m.insert(
        631,
        Pokemon {
            number: 631,
            english: "Heatmor",
            mandarin: "熔蟻獸",
            french: "Aflamanoir",
            german: "Furnifraß",
            korean: "앤티골",
            japanese: "クイタラン",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        632,
        Pokemon {
            number: 632,
            english: "Durant",
            mandarin: "鐵蟻",
            french: "Fermite",
            german: "Fermicula",
            korean: "아이앤트",
            japanese: "アイアント",
            types: Types::Dual(Type::Bug, Type::Steel),
        },
    );
    m.insert(
        633,
        Pokemon {
            number: 633,
            english: "Deino",
            mandarin: "單首龍",
            french: "Solochi",
            german: "Kapuno",
            korean: "모노두",
            japanese: "モノズ",
            types: Types::Dual(Type::Dark, Type::Dragon),
        },
    );
    m.insert(
        634,
        Pokemon {
            number: 634,
            english: "Zweilous",
            mandarin: "雙首暴龍",
            french: "Diamat",
            german: "Duodino",
            korean: "디헤드",
            japanese: "ジヘッド",
            types: Types::Dual(Type::Dark, Type::Dragon),
        },
    );
    m.insert(
        635,
        Pokemon {
            number: 635,
            english: "Hydreigon",
            mandarin: "三首惡龍",
            french: "Trioxhydre",
            german: "Trikephalo",
            korean: "삼삼드래",
            japanese: "サザンドラ",
            types: Types::Dual(Type::Dark, Type::Dragon),
        },
    );
    m.insert(
        636,
        Pokemon {
            number: 636,
            english: "Larvesta",
            mandarin: "燃燒蟲",
            french: "Pyronille",
            german: "Ignivor",
            korean: "활화르바",
            japanese: "メラルバ",
            types: Types::Dual(Type::Bug, Type::Fire),
        },
    );
    m.insert(
        637,
        Pokemon {
            number: 637,
            english: "Volcarona",
            mandarin: "火神蛾",
            french: "Pyrax",
            german: "Ramoth",
            korean: "불카모스",
            japanese: "ウルガモス",
            types: Types::Dual(Type::Bug, Type::Fire),
        },
    );
    m.insert(
        638,
        Pokemon {
            number: 638,
            english: "Cobalion",
            mandarin: "勾帕路翁",
            french: "Cobaltium",
            german: "Kobalium",
            korean: "코바르온",
            japanese: "コバルオン",
            types: Types::Dual(Type::Steel, Type::Fighting),
        },
    );
    m.insert(
        639,
        Pokemon {
            number: 639,
            english: "Terrakion",
            mandarin: "代拉基翁",
            french: "Terrakium",
            german: "Terrakium",
            korean: "테라키온",
            japanese: "テラキオン",
            types: Types::Dual(Type::Rock, Type::Fighting),
        },
    );
    m.insert(
        640,
        Pokemon {
            number: 640,
            english: "Virizion",
            mandarin: "畢力吉翁",
            french: "Viridium",
            german: "Viridium",
            korean: "비리디온",
            japanese: "ビリジオン",
            types: Types::Dual(Type::Grass, Type::Fighting),
        },
    );
    m.insert(
        641,
        Pokemon {
            number: 641,
            english: "Tornadus",
            mandarin: "龍捲雲",
            french: "Boréas",
            german: "Boreos",
            korean: "토네로스",
            japanese: "トルネロス",
            types: Types::Single(Type::Flying),
        },
    );
    m.insert(
        642,
        Pokemon {
            number: 642,
            english: "Thundurus",
            mandarin: "雷電雲",
            french: "Fulguris",
            german: "Voltolos",
            korean: "볼트로스",
            japanese: "ボルトロス",
            types: Types::Dual(Type::Electric, Type::Flying),
        },
    );
    m.insert(
        643,
        Pokemon {
            number: 643,
            english: "Reshiram",
            mandarin: "萊希拉姆",
            french: "Reshiram",
            german: "Reshiram",
            korean: "레시라무",
            japanese: "レシラム",
            types: Types::Dual(Type::Dragon, Type::Fire),
        },
    );
    m.insert(
        644,
        Pokemon {
            number: 644,
            english: "Zekrom",
            mandarin: "捷克羅姆",
            french: "Zekrom",
            german: "Zekrom",
            korean: "제크로무",
            japanese: "ゼクロム",
            types: Types::Dual(Type::Dragon, Type::Electric),
        },
    );
    m.insert(
        645,
        Pokemon {
            number: 645,
            english: "Landorus",
            mandarin: "土地雲",
            french: "Démétéros",
            german: "Demeteros",
            korean: "랜드로스",
            japanese: "ランドロス",
            types: Types::Dual(Type::Ground, Type::Flying),
        },
    );
    m.insert(
        646,
        Pokemon {
            number: 646,
            english: "Kyurem",
            mandarin: "酋雷姆",
            french: "Kyurem",
            german: "Kyurem",
            korean: "큐레무",
            japanese: "キュレム",
            types: Types::Dual(Type::Dragon, Type::Ice),
        },
    );
    m.insert(
        647,
        Pokemon {
            number: 647,
            english: "Keldeo",
            mandarin: "凱路迪歐",
            french: "Keldeo",
            german: "Keldeo",
            korean: "케르디오",
            japanese: "ケルディオ",
            types: Types::Dual(Type::Water, Type::Fighting),
        },
    );
    m.insert(
        648,
        Pokemon {
            number: 648,
            english: "Meloetta",
            mandarin: "美洛耶塔",
            french: "Meloetta",
            german: "Meloetta",
            korean: "메로엣타",
            japanese: "メロエッタ",
            types: Types::Variants(vec![
                Types::Dual(Type::Normal, Type::Psychic),
                Types::Dual(Type::Normal, Type::Fighting),
            ]),
        },
    );
    m.insert(
        649,
        Pokemon {
            number: 649,
            english: "Genesect",
            mandarin: "蓋諾賽克特",
            french: "Genesect",
            german: "Genesect",
            korean: "게노세크트",
            japanese: "ゲノセクト",
            types: Types::Dual(Type::Bug, Type::Steel),
        },
    );
    m.insert(
        650,
        Pokemon {
            number: 650,
            english: "Chespin",
            mandarin: "哈力栗",
            french: "Marisson",
            german: "Igamaro",
            korean: "도치마론",
            japanese: "ハリマロン",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        651,
        Pokemon {
            number: 651,
            english: "Quilladin",
            mandarin: "胖胖哈力",
            french: "Boguérisse",
            german: "Igastarnish",
            korean: "도치보구",
            japanese: "ハリボーグ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        652,
        Pokemon {
            number: 652,
            english: "Chesnaught",
            mandarin: "布里卡隆",
            french: "Blindépique",
            german: "Brigaron",
            korean: "브리가론",
            japanese: "ブリガロン",
            types: Types::Dual(Type::Grass, Type::Fighting),
        },
    );
    m.insert(
        653,
        Pokemon {
            number: 653,
            english: "Fennekin",
            mandarin: "火狐狸",
            french: "Feunnec",
            german: "Fynx",
            korean: "푸호꼬",
            japanese: "フォッコ",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        654,
        Pokemon {
            number: 654,
            english: "Braixen",
            mandarin: "長尾火狐",
            french: "Roussil",
            german: "Rutena",
            korean: "테르나",
            japanese: "テールナー",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        655,
        Pokemon {
            number: 655,
            english: "Delphox",
            mandarin: "妖火紅狐",
            french: "Goupelin",
            german: "Fennexis",
            korean: "마폭시",
            japanese: "マフォクシー",
            types: Types::Dual(Type::Fire, Type::Psychic),
        },
    );
    m.insert(
        656,
        Pokemon {
            number: 656,
            english: "Froakie",
            mandarin: "呱呱泡蛙",
            french: "Grenousse",
            german: "Froxy",
            korean: "개구마르",
            japanese: "ケロマツ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        657,
        Pokemon {
            number: 657,
            english: "Frogadier",
            mandarin: "呱頭蛙",
            french: "Croâporal",
            german: "Amphizel",
            korean: "개굴반장",
            japanese: "ゲコガシラ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        658,
        Pokemon {
            number: 658,
            english: "Greninja",
            mandarin: "甲賀忍蛙",
            french: "Amphinobi",
            german: "Quajutsu",
            korean: "개굴닌자",
            japanese: "ゲッコウガ",
            types: Types::Dual(Type::Water, Type::Dark),
        },
    );
    m.insert(
        659,
        Pokemon {
            number: 659,
            english: "Bunnelby",
            mandarin: "掘掘兔",
            french: "Sapereau",
            german: "Scoppel",
            korean: "파르빗",
            japanese: "ホルビー",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        660,
        Pokemon {
            number: 660,
            english: "Diggersby",
            mandarin: "掘地兔",
            french: "Excavarenne",
            german: "Grebbit",
            korean: "파르토",
            japanese: "ホルード",
            types: Types::Dual(Type::Normal, Type::Ground),
        },
    );
    m.insert(
        661,
        Pokemon {
            number: 661,
            english: "Fletchling",
            mandarin: "小箭雀",
            french: "Passerouge",
            german: "Dartiri",
            korean: "화살꼬빈",
            japanese: "ヤヤコマ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        662,
        Pokemon {
            number: 662,
            english: "Fletchinder",
            mandarin: "火箭雀",
            french: "Braisillon",
            german: "Dartignis",
            korean: "불화살빈",
            japanese: "ヒノヤコマ",
            types: Types::Dual(Type::Fire, Type::Flying),
        },
    );
    m.insert(
        663,
        Pokemon {
            number: 663,
            english: "Talonflame",
            mandarin: "烈箭鷹",
            french: "Flambusard",
            german: "Fiaro",
            korean: "파이어로",
            japanese: "ファイアロー",
            types: Types::Dual(Type::Fire, Type::Flying),
        },
    );
    m.insert(
        664,
        Pokemon {
            number: 664,
            english: "Scatterbug",
            mandarin: "粉蝶蟲",
            french: "Lépidonille",
            german: "Purmel",
            korean: "분이벌레",
            japanese: "コフキムシ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        665,
        Pokemon {
            number: 665,
            english: "Spewpa",
            mandarin: "粉蝶蛹",
            french: "Pérégrain",
            german: "Puponcho",
            korean: "분떠도리",
            japanese: "コフーライ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        666,
        Pokemon {
            number: 666,
            english: "Vivillon",
            mandarin: "彩粉蝶",
            french: "Prismillon",
            german: "Vivillon",
            korean: "비비용",
            japanese: "ビビヨン",
            types: Types::Dual(Type::Bug, Type::Flying),
        },
    );
    m.insert(
        667,
        Pokemon {
            number: 667,
            english: "Litleo",
            mandarin: "小獅獅",
            french: "Hélionceau",
            german: "Leufeo",
            korean: "레오꼬",
            japanese: "シシコ",
            types: Types::Dual(Type::Fire, Type::Normal),
        },
    );
    m.insert(
        668,
        Pokemon {
            number: 668,
            english: "Pyroar",
            mandarin: "火炎獅",
            french: "Némélios",
            german: "Pyroleo",
            korean: "화염레오",
            japanese: "カエンジシ",
            types: Types::Dual(Type::Fire, Type::Normal),
        },
    );
    m.insert(
        669,
        Pokemon {
            number: 669,
            english: "Flabébé",
            mandarin: "花蓓蓓",
            french: "Flabébé",
            german: "Flabébé",
            korean: "플라베베",
            japanese: "フラベベ",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        670,
        Pokemon {
            number: 670,
            english: "Floette",
            mandarin: "花葉蒂",
            french: "Floette",
            german: "Floette",
            korean: "플라엣테",
            japanese: "フラエッテ",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        671,
        Pokemon {
            number: 671,
            english: "Florges",
            mandarin: "花潔夫人",
            french: "Florges",
            german: "Florges",
            korean: "플라제스",
            japanese: "フラージェス",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        672,
        Pokemon {
            number: 672,
            english: "Skiddo",
            mandarin: "坐騎小羊",
            french: "Cabriolaine",
            german: "Mähikel",
            korean: "메이클",
            japanese: "メェークル",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        673,
        Pokemon {
            number: 673,
            english: "Gogoat",
            mandarin: "坐騎山羊",
            french: "Chevroum",
            german: "Chevrumm",
            korean: "고고트",
            japanese: "ゴーゴート",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        674,
        Pokemon {
            number: 674,
            english: "Pancham",
            mandarin: "頑皮熊貓",
            french: "Pandespiègle",
            german: "Pam-Pam",
            korean: "판짱",
            japanese: "ヤンチャム",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        675,
        Pokemon {
            number: 675,
            english: "Pangoro",
            mandarin: "流氓熊貓",
            french: "Pandarbare",
            german: "Pandagro",
            korean: "부란다",
            japanese: "ゴロンダ",
            types: Types::Dual(Type::Fighting, Type::Dark),
        },
    );
    m.insert(
        676,
        Pokemon {
            number: 676,
            english: "Furfrou",
            mandarin: "多麗米亞",
            french: "Couafarel",
            german: "Coiffwaff",
            korean: "트리미앙",
            japanese: "トリミアン",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        677,
        Pokemon {
            number: 677,
            english: "Espurr",
            mandarin: "妙喵",
            french: "Psystigri",
            german: "Psiau",
            korean: "냐스퍼",
            japanese: "ニャスパー",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        678,
        Pokemon {
            number: 678,
            english: "Meowstic",
            mandarin: "超能妙喵",
            french: "Mistigrix",
            german: "Psiaugon",
            korean: "냐오닉스",
            japanese: "ニャオニクス",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        679,
        Pokemon {
            number: 679,
            english: "Honedge",
            mandarin: "獨劍鞘",
            french: "Monorpale",
            german: "Gramokles",
            korean: "단칼빙",
            japanese: "ヒトツキ",
            types: Types::Dual(Type::Steel, Type::Ghost),
        },
    );
    m.insert(
        680,
        Pokemon {
            number: 680,
            english: "Doublade",
            mandarin: "雙劍鞘",
            french: "Dimoclès",
            german: "Duokles",
            korean: "쌍검킬",
            japanese: "ニダンギル",
            types: Types::Dual(Type::Steel, Type::Ghost),
        },
    );
    m.insert(
        681,
        Pokemon {
            number: 681,
            english: "Aegislash",
            mandarin: "堅盾劍怪",
            french: "Exagide",
            german: "Durengard",
            korean: "킬가르도",
            japanese: "ギルガルド",
            types: Types::Dual(Type::Steel, Type::Ghost),
        },
    );
    m.insert(
        682,
        Pokemon {
            number: 682,
            english: "Spritzee",
            mandarin: "粉香香",
            french: "Fluvetin",
            german: "Parfi",
            korean: "슈쁘",
            japanese: "シュシュプ",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        683,
        Pokemon {
            number: 683,
            english: "Aromatisse",
            mandarin: "芳香精",
            french: "Cocotine",
            german: "Parfinesse",
            korean: "프레프티르",
            japanese: "フレフワン",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        684,
        Pokemon {
            number: 684,
            english: "Swirlix",
            mandarin: "綿綿泡芙",
            french: "Sucroquin",
            german: "Flauschling",
            korean: "나룸퍼프",
            japanese: "ペロッパフ",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        685,
        Pokemon {
            number: 685,
            english: "Slurpuff",
            mandarin: "胖甜妮",
            french: "Cupcanaille",
            german: "Sabbaione",
            korean: "나루림",
            japanese: "ペロリーム",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        686,
        Pokemon {
            number: 686,
            english: "Inkay",
            mandarin: "好啦魷",
            french: "Sepiatop",
            german: "Iscalar",
            korean: "오케이징",
            japanese: "マーイーカ",
            types: Types::Dual(Type::Dark, Type::Psychic),
        },
    );
    m.insert(
        687,
        Pokemon {
            number: 687,
            english: "Malamar",
            mandarin: "烏賊王",
            french: "Sepiatroce",
            german: "Calamanero",
            korean: "칼라마네로",
            japanese: "カラマネロ",
            types: Types::Dual(Type::Dark, Type::Psychic),
        },
    );
    m.insert(
        688,
        Pokemon {
            number: 688,
            english: "Binacle",
            mandarin: "龜腳腳",
            french: "Opermine",
            german: "Bithora",
            korean: "거북손손",
            japanese: "カメテテ",
            types: Types::Dual(Type::Rock, Type::Water),
        },
    );
    m.insert(
        689,
        Pokemon {
            number: 689,
            english: "Barbaracle",
            mandarin: "龜足巨鎧",
            french: "Golgopathe",
            german: "Thanathora",
            korean: "거북손데스",
            japanese: "ガメノデス",
            types: Types::Dual(Type::Rock, Type::Water),
        },
    );
    m.insert(
        690,
        Pokemon {
            number: 690,
            english: "Skrelp",
            mandarin: "垃垃藻",
            french: "Venalgue",
            german: "Algitt",
            korean: "수레기",
            japanese: "クズモー",
            types: Types::Dual(Type::Poison, Type::Water),
        },
    );
    m.insert(
        691,
        Pokemon {
            number: 691,
            english: "Dragalge",
            mandarin: "毒藻龍",
            french: "Kravarech",
            german: "Tandrak",
            korean: "드래캄",
            japanese: "ドラミドロ",
            types: Types::Dual(Type::Poison, Type::Dragon),
        },
    );
    m.insert(
        692,
        Pokemon {
            number: 692,
            english: "Clauncher",
            mandarin: "鐵臂槍蝦",
            french: "Flingouste",
            german: "Scampisto",
            korean: "완철포",
            japanese: "ウデッポウ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        693,
        Pokemon {
            number: 693,
            english: "Clawitzer",
            mandarin: "鋼炮臂蝦",
            french: "Gamblast",
            german: "Wummer",
            korean: "블로스터",
            japanese: "ブロスター",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        694,
        Pokemon {
            number: 694,
            english: "Helioptile",
            mandarin: "傘電蜥",
            french: "Galvaran",
            german: "Eguana",
            korean: "목도리키텔",
            japanese: "エリキテル",
            types: Types::Dual(Type::Electric, Type::Normal),
        },
    );
    m.insert(
        695,
        Pokemon {
            number: 695,
            english: "Heliolisk",
            mandarin: "光電傘蜥",
            french: "Iguolta",
            german: "Elezard",
            korean: "일레도리자드",
            japanese: "エレザード",
            types: Types::Dual(Type::Electric, Type::Normal),
        },
    );
    m.insert(
        696,
        Pokemon {
            number: 696,
            english: "Tyrunt",
            mandarin: "寶寶暴龍",
            french: "Ptyranidur",
            german: "Balgoras",
            korean: "티고라스",
            japanese: "チゴラス",
            types: Types::Dual(Type::Rock, Type::Dragon),
        },
    );
    m.insert(
        697,
        Pokemon {
            number: 697,
            english: "Tyrantrum",
            mandarin: "怪顎龍",
            french: "Rexillius",
            german: "Monargoras",
            korean: "견고라스",
            japanese: "ガチゴラス",
            types: Types::Dual(Type::Rock, Type::Dragon),
        },
    );
    m.insert(
        698,
        Pokemon {
            number: 698,
            english: "Amaura",
            mandarin: "冰雪龍",
            french: "Amagara",
            german: "Amarino",
            korean: "아마루스",
            japanese: "アマルス",
            types: Types::Dual(Type::Rock, Type::Ice),
        },
    );
    m.insert(
        699,
        Pokemon {
            number: 699,
            english: "Aurorus",
            mandarin: "冰雪巨龍",
            french: "Dragmara",
            german: "Amagarga",
            korean: "아마루르가",
            japanese: "アマルルガ",
            types: Types::Dual(Type::Rock, Type::Ice),
        },
    );
    m.insert(
        700,
        Pokemon {
            number: 700,
            english: "Sylveon",
            mandarin: "仙子伊布",
            french: "Nymphali",
            german: "Feelinara",
            korean: "님피아",
            japanese: "ニンフィア",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        701,
        Pokemon {
            number: 701,
            english: "Hawlucha",
            mandarin: "摔角鷹人",
            french: "Brutalibré",
            german: "Resladero",
            korean: "루차불",
            japanese: "ルチャブル",
            types: Types::Dual(Type::Fighting, Type::Flying),
        },
    );
    m.insert(
        702,
        Pokemon {
            number: 702,
            english: "Dedenne",
            mandarin: "咚咚鼠",
            french: "Dedenne",
            german: "Dedenne",
            korean: "데덴네",
            japanese: "デデンネ",
            types: Types::Dual(Type::Electric, Type::Fairy),
        },
    );
    m.insert(
        703,
        Pokemon {
            number: 703,
            english: "Carbink",
            mandarin: "小碎鑽",
            french: "Strassie",
            german: "Rocara",
            korean: "멜리시",
            japanese: "メレシー",
            types: Types::Dual(Type::Rock, Type::Fairy),
        },
    );
    m.insert(
        704,
        Pokemon {
            number: 704,
            english: "Goomy",
            mandarin: "黏黏寶",
            french: "Mucuscule",
            german: "Viscora",
            korean: "미끄메라",
            japanese: "ヌメラ",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        705,
        Pokemon {
            number: 705,
            english: "Sliggoo",
            mandarin: "黏美兒",
            french: "Colimucus",
            german: "Viscargot",
            korean: "미끄네일",
            japanese: "ヌメイル",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        706,
        Pokemon {
            number: 706,
            english: "Goodra",
            mandarin: "黏美龍",
            french: "Muplodocus",
            german: "Viscogon",
            korean: "미끄래곤",
            japanese: "ヌメルゴン",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        707,
        Pokemon {
            number: 707,
            english: "Klefki",
            mandarin: "鑰圈兒",
            french: "Trousselin",
            german: "Clavion",
            korean: "클레피",
            japanese: "クレッフィ",
            types: Types::Dual(Type::Steel, Type::Fairy),
        },
    );
    m.insert(
        708,
        Pokemon {
            number: 708,
            english: "Phantump",
            mandarin: "小木靈",
            french: "Brocélôme",
            german: "Paragoni",
            korean: "나목령",
            japanese: "ボクレー",
            types: Types::Dual(Type::Ghost, Type::Grass),
        },
    );
    m.insert(
        709,
        Pokemon {
            number: 709,
            english: "Trevenant",
            mandarin: "朽木妖",
            french: "Desséliande",
            german: "Trombork",
            korean: "대로트",
            japanese: "オーロット",
            types: Types::Dual(Type::Ghost, Type::Grass),
        },
    );
    m.insert(
        710,
        Pokemon {
            number: 710,
            english: "Pumpkaboo",
            mandarin: "南瓜精",
            french: "Pitrouille",
            german: "Irrbis",
            korean: "호바귀",
            japanese: "バケッチャ",
            types: Types::Dual(Type::Ghost, Type::Grass),
        },
    );
    m.insert(
        711,
        Pokemon {
            number: 711,
            english: "Gourgeist",
            mandarin: "南瓜怪人",
            french: "Banshitrouye",
            german: "Pumpdjinn",
            korean: "펌킨인",
            japanese: "パンプジン",
            types: Types::Dual(Type::Ghost, Type::Grass),
        },
    );
    m.insert(
        712,
        Pokemon {
            number: 712,
            english: "Bergmite",
            mandarin: "冰寶",
            french: "Grelaçon",
            german: "Arktip",
            korean: "꽁어름",
            japanese: "カチコール",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        713,
        Pokemon {
            number: 713,
            english: "Avalugg",
            mandarin: "冰岩怪",
            french: "Séracrawl",
            german: "Arktilas",
            korean: "크레베이스",
            japanese: "クレベース",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        714,
        Pokemon {
            number: 714,
            english: "Noibat",
            mandarin: "嗡蝠",
            french: "Sonistrelle",
            german: "eF-eM",
            korean: "음뱃",
            japanese: "オンバット",
            types: Types::Dual(Type::Flying, Type::Dragon),
        },
    );
    m.insert(
        715,
        Pokemon {
            number: 715,
            english: "Noivern",
            mandarin: "音波龍",
            french: "Bruyverne",
            german: "UHaFnir",
            korean: "음번",
            japanese: "オンバーン",
            types: Types::Dual(Type::Flying, Type::Dragon),
        },
    );
    m.insert(
        716,
        Pokemon {
            number: 716,
            english: "Xerneas",
            mandarin: "哲爾尼亞斯",
            french: "Xerneas",
            german: "Xerneas",
            korean: "제르네아스",
            japanese: "ゼルネアス",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        717,
        Pokemon {
            number: 717,
            english: "Yveltal",
            mandarin: "伊裴爾塔爾",
            french: "Yveltal",
            german: "Yveltal",
            korean: "이벨타르",
            japanese: "イベルタル",
            types: Types::Dual(Type::Dark, Type::Flying),
        },
    );
    m.insert(
        718,
        Pokemon {
            number: 718,
            english: "Zygarde",
            mandarin: "基格爾德",
            french: "Zygarde",
            german: "Zygarde",
            korean: "지가르데",
            japanese: "ジガルデ",
            types: Types::Dual(Type::Dragon, Type::Ground),
        },
    );
    m.insert(
        719,
        Pokemon {
            number: 719,
            english: "Diancie",
            mandarin: "蒂安希",
            french: "Diancie",
            german: "Diancie",
            korean: "디안시",
            japanese: "ディアンシー",
            types: Types::Dual(Type::Rock, Type::Fairy),
        },
    );
    m.insert(
        720,
        Pokemon {
            number: 720,
            english: "Hoopa",
            mandarin: "胡帕",
            french: "Hoopa",
            german: "Hoopa",
            korean: "후파",
            japanese: "フーパ",
            types: Types::Variants(vec![
                Types::Dual(Type::Psychic, Type::Ghost),
                Types::Dual(Type::Psychic, Type::Dark),
            ]),
        },
    );
    m.insert(
        721,
        Pokemon {
            number: 721,
            english: "Volcanion",
            mandarin: "波爾凱尼恩",
            french: "Volcanion",
            german: "Volcanion",
            korean: "볼케니온",
            japanese: "ボルケニオン",
            types: Types::Dual(Type::Fire, Type::Water),
        },
    );
    m.insert(
        722,
        Pokemon {
            number: 722,
            english: "Rowlet",
            mandarin: "木木梟",
            french: "Brindibou",
            german: "Bauz",
            korean: "나몰빼미",
            japanese: "モクロー",
            types: Types::Dual(Type::Grass, Type::Flying),
        },
    );
    m.insert(
        723,
        Pokemon {
            number: 723,
            english: "Dartrix",
            mandarin: "投羽梟",
            french: "Efflèche",
            german: "Arboretoss",
            korean: "빼미스로우",
            japanese: "フクスロー",
            types: Types::Dual(Type::Grass, Type::Flying),
        },
    );
    m.insert(
        724,
        Pokemon {
            number: 724,
            english: "Decidueye",
            mandarin: "狙射樹梟",
            french: "Archéduc",
            german: "Silvarro",
            korean: "모크나이퍼",
            japanese: "ジュナイパー",
            types: Types::Dual(Type::Grass, Type::Ghost),
        },
    );
    m.insert(
        725,
        Pokemon {
            number: 725,
            english: "Litten",
            mandarin: "火斑喵",
            french: "Flamiaou",
            german: "Flamiau",
            korean: "냐오불",
            japanese: "ニャビー",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        726,
        Pokemon {
            number: 726,
            english: "Torracat",
            mandarin: "炎熱喵",
            french: "Matoufeu",
            german: "Miezunder",
            korean: "냐오히트",
            japanese: "ニャヒート",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        727,
        Pokemon {
            number: 727,
            english: "Incineroar",
            mandarin: "熾焰咆哮虎",
            french: "Félinferno",
            german: "Fuegro",
            korean: "어흥염",
            japanese: "ガオガエン",
            types: Types::Dual(Type::Fire, Type::Dark),
        },
    );
    m.insert(
        728,
        Pokemon {
            number: 728,
            english: "Popplio",
            mandarin: "球球海獅",
            french: "Otaquin",
            german: "Robball",
            korean: "누리공",
            japanese: "アシマリ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        729,
        Pokemon {
            number: 729,
            english: "Brionne",
            mandarin: "花漾海獅",
            french: "Otarlette",
            german: "Marikeck",
            korean: "키요공",
            japanese: "オシャマリ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        730,
        Pokemon {
            number: 730,
            english: "Primarina",
            mandarin: "西獅海壬",
            french: "Oratoria",
            german: "Primarene",
            korean: "누리레느",
            japanese: "アシレーヌ",
            types: Types::Dual(Type::Water, Type::Fairy),
        },
    );
    m.insert(
        731,
        Pokemon {
            number: 731,
            english: "Pikipek",
            mandarin: "小篤兒",
            french: "Picassaut",
            german: "Peppeck",
            korean: "콕코구리",
            japanese: "ツツケラ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        732,
        Pokemon {
            number: 732,
            english: "Trumbeak",
            mandarin: "喇叭啄鳥",
            french: "Piclairon",
            german: "Trompeck",
            korean: "크라파",
            japanese: "ケララッパ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        733,
        Pokemon {
            number: 733,
            english: "Toucannon",
            mandarin: "銃嘴大鳥",
            french: "Bazoucan",
            german: "Tukanon",
            korean: "왕큰부리",
            japanese: "ドデカバシ",
            types: Types::Dual(Type::Normal, Type::Flying),
        },
    );
    m.insert(
        734,
        Pokemon {
            number: 734,
            english: "Yungoos",
            mandarin: "貓鼬少",
            french: "Manglouton",
            german: "Mangunior",
            korean: "영구스",
            japanese: "ヤングース",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        735,
        Pokemon {
            number: 735,
            english: "Gumshoos",
            mandarin: "貓鼬探長",
            french: "Argouste",
            german: "Manguspektor",
            korean: "형사구스",
            japanese: "デカグース",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        736,
        Pokemon {
            number: 736,
            english: "Grubbin",
            mandarin: "強顎雞母蟲",
            french: "Larvibule",
            german: "Mabula",
            korean: "턱지충이",
            japanese: "アゴジムシ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        737,
        Pokemon {
            number: 737,
            english: "Charjabug",
            mandarin: "蟲電寶",
            french: "Chrysapile",
            german: "Akkup",
            korean: "전지충이",
            japanese: "デンヂムシ",
            types: Types::Dual(Type::Bug, Type::Electric),
        },
    );
    m.insert(
        738,
        Pokemon {
            number: 738,
            english: "Vikavolt",
            mandarin: "鍬農炮蟲",
            french: "Lucanon",
            german: "Donarion",
            korean: "투구뿌논",
            japanese: "クワガノン",
            types: Types::Dual(Type::Bug, Type::Electric),
        },
    );
    m.insert(
        739,
        Pokemon {
            number: 739,
            english: "Crabrawler",
            mandarin: "好勝蟹",
            french: "Crabagarre",
            german: "Krabbox",
            korean: "오기지게",
            japanese: "マケンカニ",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        740,
        Pokemon {
            number: 740,
            english: "Crabominable",
            mandarin: "好勝毛蟹",
            french: "Crabominable",
            german: "Krawell",
            korean: "모단단게",
            japanese: "ケケンカニ",
            types: Types::Dual(Type::Fighting, Type::Ice),
        },
    );
    m.insert(
        741,
        Pokemon {
            number: 741,
            english: "Oricorio",
            mandarin: "花舞鳥",
            french: "Plumeline",
            german: "Choreogel",
            korean: "춤추새",
            japanese: "オドリドリ",
            types: Types::Variants(vec![
                Types::Dual(Type::Fire, Type::Flying),
                Types::Dual(Type::Electric, Type::Flying),
                Types::Dual(Type::Psychic, Type::Flying),
                Types::Dual(Type::Ghost, Type::Flying),
            ]),
        },
    );
    m.insert(
        742,
        Pokemon {
            number: 742,
            english: "Cutiefly",
            mandarin: "萌虻",
            french: "Bombydou",
            german: "Wommel",
            korean: "에블리",
            japanese: "アブリー",
            types: Types::Dual(Type::Bug, Type::Fairy),
        },
    );
    m.insert(
        743,
        Pokemon {
            number: 743,
            english: "Ribombee",
            mandarin: "蝶結萌虻",
            french: "Rubombelle",
            german: "Bandelby",
            korean: "에리본",
            japanese: "アブリボン",
            types: Types::Dual(Type::Bug, Type::Fairy),
        },
    );
    m.insert(
        744,
        Pokemon {
            number: 744,
            english: "Rockruff",
            mandarin: "岩狗狗",
            french: "Rocabot",
            german: "Wuffels",
            korean: "암멍이",
            japanese: "イワンコ",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        745,
        Pokemon {
            number: 745,
            english: "Lycanroc",
            mandarin: "鬃岩狼人",
            french: "Lougaroc",
            german: "Wolwerock",
            korean: "루가루암",
            japanese: "ルガルガン",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        746,
        Pokemon {
            number: 746,
            english: "Wishiwashi",
            mandarin: "弱丁魚",
            french: "Froussardine",
            german: "Lusardin",
            korean: "약어리",
            japanese: "ヨワシ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        747,
        Pokemon {
            number: 747,
            english: "Mareanie",
            mandarin: "好壞星",
            french: "Vorastérie",
            german: "Garstella",
            korean: "시마사리",
            japanese: "ヒドイデ",
            types: Types::Dual(Type::Poison, Type::Water),
        },
    );
    m.insert(
        748,
        Pokemon {
            number: 748,
            english: "Toxapex",
            mandarin: "超壞星",
            french: "Prédastérie",
            german: "Aggrostella",
            korean: "더시마사리",
            japanese: "ドヒドイデ",
            types: Types::Dual(Type::Poison, Type::Water),
        },
    );
    m.insert(
        749,
        Pokemon {
            number: 749,
            english: "Mudbray",
            mandarin: "泥驢仔",
            french: "Tiboudet",
            german: "Pampuli",
            korean: "머드나기",
            japanese: "ドロバンコ",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        750,
        Pokemon {
            number: 750,
            english: "Mudsdale",
            mandarin: "重泥輓馬",
            french: "Bourrinos",
            german: "Pampross",
            korean: "만마드",
            japanese: "バンバドロ",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        751,
        Pokemon {
            number: 751,
            english: "Dewpider",
            mandarin: "滴蛛",
            french: "Araqua",
            german: "Araqua",
            korean: "물거미",
            japanese: "シズクモ",
            types: Types::Dual(Type::Water, Type::Bug),
        },
    );
    m.insert(
        752,
        Pokemon {
            number: 752,
            english: "Araquanid",
            mandarin: "滴蛛霸",
            french: "Tarenbulle",
            german: "Aranestro",
            korean: "깨비물거미",
            japanese: "オニシズクモ",
            types: Types::Dual(Type::Water, Type::Bug),
        },
    );
    m.insert(
        753,
        Pokemon {
            number: 753,
            english: "Fomantis",
            mandarin: "偽螳草",
            french: "Mimantis",
            german: "Imantis",
            korean: "짜랑랑",
            japanese: "カリキリ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        754,
        Pokemon {
            number: 754,
            english: "Lurantis",
            mandarin: "蘭螳花",
            french: "Floramantis",
            german: "Mantidea",
            korean: "라란티스",
            japanese: "ラランテス",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        755,
        Pokemon {
            number: 755,
            english: "Morelull",
            mandarin: "睡睡菇",
            french: "Spododo",
            german: "Bubungus",
            korean: "자마슈",
            japanese: "ネマシュ",
            types: Types::Dual(Type::Grass, Type::Fairy),
        },
    );
    m.insert(
        756,
        Pokemon {
            number: 756,
            english: "Shiinotic",
            mandarin: "燈罩夜菇",
            french: "Lampignon",
            german: "Lamellux",
            korean: "마셰이드",
            japanese: "マシェード",
            types: Types::Dual(Type::Grass, Type::Fairy),
        },
    );
    m.insert(
        757,
        Pokemon {
            number: 757,
            english: "Salandit",
            mandarin: "夜盜火蜥",
            french: "Tritox",
            german: "Molunk",
            korean: "야도뇽",
            japanese: "ヤトウモリ",
            types: Types::Dual(Type::Poison, Type::Fire),
        },
    );
    m.insert(
        758,
        Pokemon {
            number: 758,
            english: "Salazzle",
            mandarin: "焰后蜥",
            french: "Malamandre",
            german: "Amfira",
            korean: "염뉴트",
            japanese: "エンニュート",
            types: Types::Dual(Type::Poison, Type::Fire),
        },
    );
    m.insert(
        759,
        Pokemon {
            number: 759,
            english: "Stufful",
            mandarin: "童偶熊",
            french: "Nounourson",
            german: "Velursi",
            korean: "포곰곰",
            japanese: "ヌイコグマ",
            types: Types::Dual(Type::Normal, Type::Fighting),
        },
    );
    m.insert(
        760,
        Pokemon {
            number: 760,
            english: "Bewear",
            mandarin: "穿著熊",
            french: "Chelours",
            german: "Kosturso",
            korean: "이븐곰",
            japanese: "キテルグマ",
            types: Types::Dual(Type::Normal, Type::Fighting),
        },
    );
    m.insert(
        761,
        Pokemon {
            number: 761,
            english: "Bounsweet",
            mandarin: "甜竹竹",
            french: "Croquine",
            german: "Frubberl",
            korean: "달콤아",
            japanese: "アマカジ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        762,
        Pokemon {
            number: 762,
            english: "Steenee",
            mandarin: "甜舞妮",
            french: "Candine",
            german: "Frubaila",
            korean: "달무리나",
            japanese: "アママイコ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        763,
        Pokemon {
            number: 763,
            english: "Tsareena",
            mandarin: "甜冷美后",
            french: "Sucreine",
            german: "Fruyal",
            korean: "달코퀸",
            japanese: "アマージョ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        764,
        Pokemon {
            number: 764,
            english: "Comfey",
            mandarin: "花療環環",
            french: "Guérilande",
            german: "Curelei",
            korean: "큐아링",
            japanese: "キュワワー",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        765,
        Pokemon {
            number: 765,
            english: "Oranguru",
            mandarin: "智揮猩",
            french: "Gouroutan",
            german: "Kommandutan",
            korean: "하랑우탄",
            japanese: "ヤレユータン",
            types: Types::Dual(Type::Normal, Type::Psychic),
        },
    );
    m.insert(
        766,
        Pokemon {
            number: 766,
            english: "Passimian",
            mandarin: "投擲猴",
            french: "Quartermac",
            german: "Quartermak",
            korean: "내던숭이",
            japanese: "ナゲツケサル",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        767,
        Pokemon {
            number: 767,
            english: "Wimpod",
            mandarin: "膽小蟲",
            french: "Sovkipou",
            german: "Reißlaus",
            korean: "꼬시레",
            japanese: "コソクムシ",
            types: Types::Dual(Type::Bug, Type::Water),
        },
    );
    m.insert(
        768,
        Pokemon {
            number: 768,
            english: "Golisopod",
            mandarin: "具甲武者",
            french: "Sarmuraï",
            german: "Tectass",
            korean: "갑주무사",
            japanese: "グソクムシャ",
            types: Types::Dual(Type::Bug, Type::Water),
        },
    );
    m.insert(
        769,
        Pokemon {
            number: 769,
            english: "Sandygast",
            mandarin: "沙丘娃",
            french: "Bacabouh",
            german: "Sankabuh",
            korean: "모래꿍",
            japanese: "スナバァ",
            types: Types::Dual(Type::Ghost, Type::Ground),
        },
    );
    m.insert(
        770,
        Pokemon {
            number: 770,
            english: "Palossand",
            mandarin: "噬沙堡爺",
            french: "Trépassable",
            german: "Colossand",
            korean: "모래성이당",
            japanese: "シロデスナ",
            types: Types::Dual(Type::Ghost, Type::Ground),
        },
    );
    m.insert(
        771,
        Pokemon {
            number: 771,
            english: "Pyukumuku",
            mandarin: "拳海參",
            french: "Concombaffe",
            german: "Gufa",
            korean: "해무기",
            japanese: "ナマコブシ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        772,
        Pokemon {
            number: 772,
            english: "Type: Null",
            mandarin: "屬性：空",
            french: "Type:0",
            german: "Typ:Null",
            korean: "타입:널",
            japanese: "タイプ：ヌル",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        773,
        Pokemon {
            number: 773,
            english: "Silvally",
            mandarin: "銀伴戰獸",
            french: "Silvallié",
            german: "Amigento",
            korean: "실버디",
            japanese: "シルヴァディ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        774,
        Pokemon {
            number: 774,
            english: "Minior",
            mandarin: "小隕星",
            french: "Météno",
            german: "Meteno",
            korean: "메테노",
            japanese: "メテノ",
            types: Types::Dual(Type::Rock, Type::Flying),
        },
    );
    m.insert(
        775,
        Pokemon {
            number: 775,
            english: "Komala",
            mandarin: "樹枕尾熊",
            french: "Dodoala",
            german: "Koalelu",
            korean: "자말라",
            japanese: "ネッコアラ",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        776,
        Pokemon {
            number: 776,
            english: "Turtonator",
            mandarin: "爆焰龜獸",
            french: "Boumata",
            german: "Tortunator",
            korean: "폭거북스",
            japanese: "バクガメス",
            types: Types::Dual(Type::Fire, Type::Dragon),
        },
    );
    m.insert(
        777,
        Pokemon {
            number: 777,
            english: "Togedemaru",
            mandarin: "托戈德瑪爾",
            french: "Togedemaru",
            german: "Togedemaru",
            korean: "토게데마루",
            japanese: "トゲデマル",
            types: Types::Dual(Type::Electric, Type::Steel),
        },
    );
    m.insert(
        778,
        Pokemon {
            number: 778,
            english: "Mimikyu",
            mandarin: "謎擬Ｑ",
            french: "Mimiqui",
            german: "Mimigma",
            korean: "따라큐",
            japanese: "ミミッキュ",
            types: Types::Dual(Type::Ghost, Type::Fairy),
        },
    );
    m.insert(
        779,
        Pokemon {
            number: 779,
            english: "Bruxish",
            mandarin: "磨牙彩皮魚",
            french: "Denticrisse",
            german: "Knirfish",
            korean: "치갈기",
            japanese: "ハギギシリ",
            types: Types::Dual(Type::Water, Type::Psychic),
        },
    );
    m.insert(
        780,
        Pokemon {
            number: 780,
            english: "Drampa",
            mandarin: "老翁龍",
            french: "Draïeul",
            german: "Sen-Long",
            korean: "할비롱",
            japanese: "ジジーロン",
            types: Types::Dual(Type::Normal, Type::Dragon),
        },
    );
    m.insert(
        781,
        Pokemon {
            number: 781,
            english: "Dhelmise",
            mandarin: "破破舵輪",
            french: "Sinistrail",
            german: "Moruda",
            korean: "타타륜",
            japanese: "ダダリン",
            types: Types::Dual(Type::Ghost, Type::Grass),
        },
    );
    m.insert(
        782,
        Pokemon {
            number: 782,
            english: "Jangmo-o",
            mandarin: "心鱗寶",
            french: "Bébécaille",
            german: "Miniras",
            korean: "짜랑꼬",
            japanese: "ジャラコ",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        783,
        Pokemon {
            number: 783,
            english: "Hakamo-o",
            mandarin: "鱗甲龍",
            french: "Écaïd",
            german: "Mediras",
            korean: "짜랑고우",
            japanese: "ジャランゴ",
            types: Types::Dual(Type::Dragon, Type::Fighting),
        },
    );
    m.insert(
        784,
        Pokemon {
            number: 784,
            english: "Kommo-o",
            mandarin: "杖尾鱗甲龍",
            french: "Ékaïser",
            german: "Grandiras",
            korean: "짜랑고우거",
            japanese: "ジャラランガ",
            types: Types::Dual(Type::Dragon, Type::Fighting),
        },
    );
    m.insert(
        785,
        Pokemon {
            number: 785,
            english: "Tapu Koko",
            mandarin: "卡璞‧鳴鳴",
            french: "Tokorico",
            german: "Kapu-Riki",
            korean: "카푸꼬꼬꼭",
            japanese: "カプ・コケコ",
            types: Types::Dual(Type::Electric, Type::Fairy),
        },
    );
    m.insert(
        786,
        Pokemon {
            number: 786,
            english: "Tapu Lele",
            mandarin: "卡璞‧蝶蝶",
            french: "Tokopiyon",
            german: "Kapu-Fala",
            korean: "카푸나비나",
            japanese: "カプ・テテフ",
            types: Types::Dual(Type::Psychic, Type::Fairy),
        },
    );
    m.insert(
        787,
        Pokemon {
            number: 787,
            english: "Tapu Bulu",
            mandarin: "卡璞‧哞哞",
            french: "Tokotoro",
            german: "Kapu-Toro",
            korean: "카푸브루루",
            japanese: "カプ・ブルル",
            types: Types::Dual(Type::Grass, Type::Fairy),
        },
    );
    m.insert(
        788,
        Pokemon {
            number: 788,
            english: "Tapu Fini",
            mandarin: "卡璞‧鰭鰭",
            french: "Tokopisco",
            german: "Kapu-Kime",
            korean: "카푸느지느",
            japanese: "カプ・レヒレ",
            types: Types::Dual(Type::Water, Type::Fairy),
        },
    );
    m.insert(
        789,
        Pokemon {
            number: 789,
            english: "Cosmog",
            mandarin: "科斯莫古",
            french: "Cosmog",
            german: "Cosmog",
            korean: "코스모그",
            japanese: "コスモッグ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        790,
        Pokemon {
            number: 790,
            english: "Cosmoem",
            mandarin: "科斯莫姆",
            french: "Cosmovum",
            german: "Cosmovum",
            korean: "코스모움",
            japanese: "コスモウム",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        791,
        Pokemon {
            number: 791,
            english: "Solgaleo",
            mandarin: "索爾迦雷歐",
            french: "Solgaleo",
            german: "Solgaleo",
            korean: "솔가레오",
            japanese: "ソルガレオ",
            types: Types::Dual(Type::Psychic, Type::Steel),
        },
    );
    m.insert(
        792,
        Pokemon {
            number: 792,
            english: "Lunala",
            mandarin: "露奈雅拉",
            french: "Lunala",
            german: "Lunala",
            korean: "루나아라",
            japanese: "ルナアーラ",
            types: Types::Dual(Type::Psychic, Type::Ghost),
        },
    );
    m.insert(
        793,
        Pokemon {
            number: 793,
            english: "Nihilego",
            mandarin: "虛吾伊德",
            french: "Zéroïd",
            german: "Anego",
            korean: "텅비드",
            japanese: "ウツロイド",
            types: Types::Dual(Type::Rock, Type::Poison),
        },
    );
    m.insert(
        794,
        Pokemon {
            number: 794,
            english: "Buzzwole",
            mandarin: "爆肌蚊",
            french: "Mouscoto",
            german: "Masskito",
            korean: "매시붕",
            japanese: "マッシブーン",
            types: Types::Dual(Type::Bug, Type::Fighting),
        },
    );
    m.insert(
        795,
        Pokemon {
            number: 795,
            english: "Pheromosa",
            mandarin: "費洛美螂",
            french: "Cancrelove",
            german: "Schabelle",
            korean: "페로코체",
            japanese: "フェローチェ",
            types: Types::Dual(Type::Bug, Type::Fighting),
        },
    );
    m.insert(
        796,
        Pokemon {
            number: 796,
            english: "Xurkitree",
            mandarin: "電束木",
            french: "Câblifère",
            german: "Voltriant",
            korean: "전수목",
            japanese: "デンジュモク",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        797,
        Pokemon {
            number: 797,
            english: "Celesteela",
            mandarin: "鐵火輝夜",
            french: "Bamboiselle",
            german: "Kaguron",
            korean: "철화구야",
            japanese: "テッカグヤ",
            types: Types::Dual(Type::Steel, Type::Flying),
        },
    );
    m.insert(
        798,
        Pokemon {
            number: 798,
            english: "Kartana",
            mandarin: "紙御劍",
            french: "Katagami",
            german: "Katagami",
            korean: "종이신도",
            japanese: "カミツルギ",
            types: Types::Dual(Type::Grass, Type::Steel),
        },
    );
    m.insert(
        799,
        Pokemon {
            number: 799,
            english: "Guzzlord",
            mandarin: "惡食大王",
            french: "Engloutyran",
            german: "Schlingking",
            korean: "악식킹",
            japanese: "アクジキング",
            types: Types::Dual(Type::Dark, Type::Dragon),
        },
    );
    m.insert(
        800,
        Pokemon {
            number: 800,
            english: "Necrozma",
            mandarin: "奈克洛茲瑪",
            french: "Necrozma",
            german: "Necrozma",
            korean: "네크로즈마",
            japanese: "ネクロズマ",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        801,
        Pokemon {
            number: 801,
            english: "Magearna",
            mandarin: "瑪機雅娜",
            french: "Magearna",
            german: "Magearna",
            korean: "마기아나",
            japanese: "マギアナ",
            types: Types::Dual(Type::Steel, Type::Fairy),
        },
    );
    m.insert(
        802,
        Pokemon {
            number: 802,
            english: "Marshadow",
            mandarin: "瑪夏多",
            french: "Marshadow",
            german: "Marshadow",
            korean: "마샤도",
            japanese: "マーシャドー",
            types: Types::Dual(Type::Fighting, Type::Ghost),
        },
    );
    m.insert(
        803,
        Pokemon {
            number: 803,
            english: "Poipole",
            mandarin: "毒貝比",
            french: "Vémini",
            german: "Venicro",
            korean: "베베놈",
            japanese: "ベベノム",
            types: Types::Single(Type::Poison),
        },
    );
    m.insert(
        804,
        Pokemon {
            number: 804,
            english: "Naganadel",
            mandarin: "四顎針龍",
            french: "Mandrillon",
            german: "Agoyon",
            korean: "아고용",
            japanese: "アーゴヨン",
            types: Types::Dual(Type::Poison, Type::Dragon),
        },
    );
    m.insert(
        805,
        Pokemon {
            number: 805,
            english: "Stakataka",
            mandarin: "壘磊石",
            french: "Ama-Ama",
            german: "Muramura",
            korean: "차곡차곡",
            japanese: "ツンデツンデ",
            types: Types::Dual(Type::Rock, Type::Steel),
        },
    );
    m.insert(
        806,
        Pokemon {
            number: 806,
            english: "Blacephalon",
            mandarin: "砰頭小丑",
            french: "Pierroteknik",
            german: "Kopplosio",
            korean: "두파팡",
            japanese: "ズガドーン",
            types: Types::Dual(Type::Fire, Type::Ghost),
        },
    );
    m.insert(
        807,
        Pokemon {
            number: 807,
            english: "Zeraora",
            mandarin: "捷拉奧拉",
            french: "Zeraora",
            german: "Zeraora",
            korean: "제라오라",
            japanese: "ゼラオラ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        808,
        Pokemon {
            number: 808,
            english: "Meltan",
            mandarin: "美錄坦",
            french: "Meltan",
            german: "Meltan",
            korean: "멜탄",
            japanese: "メルタン",
            types: Types::Single(Type::Steel),
        },
    );
    m.insert(
        809,
        Pokemon {
            number: 809,
            english: "Melmetal",
            mandarin: "美錄梅塔",
            french: "Melmetal",
            german: "Melmetal",
            korean: "멜메탈",
            japanese: "メルメタル",
            types: Types::Single(Type::Steel),
        },
    );
    m.insert(
        810,
        Pokemon {
            number: 810,
            english: "Grookey",
            mandarin: "敲音猴",
            french: "Ouistempo",
            german: "Chimpep",
            korean: "흥나숭",
            japanese: "サルノリ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        811,
        Pokemon {
            number: 811,
            english: "Thwackey",
            mandarin: "啪咚猴",
            french: "Badabouin",
            german: "Chimstix",
            korean: "채키몽",
            japanese: "バチンキー",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        812,
        Pokemon {
            number: 812,
            english: "Rillaboom",
            mandarin: "轟擂金剛猩",
            french: "Gorythmic",
            german: "Gortrom",
            korean: "고릴타",
            japanese: "ゴリランダー",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        813,
        Pokemon {
            number: 813,
            english: "Scorbunny",
            mandarin: "炎兔兒",
            french: "Flambino",
            german: "Hopplo",
            korean: "염버니",
            japanese: "ヒバニー",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        814,
        Pokemon {
            number: 814,
            english: "Raboot",
            mandarin: "騰蹴小將",
            french: "Lapyro",
            german: "Kickerlo",
            korean: "래비풋",
            japanese: "ラビフット",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        815,
        Pokemon {
            number: 815,
            english: "Cinderace",
            mandarin: "閃焰王牌",
            french: "Pyrobut",
            german: "Liberlo",
            korean: "에이스번",
            japanese: "エースバーン",
            types: Types::Single(Type::Fire),
        },
    );
    m.insert(
        816,
        Pokemon {
            number: 816,
            english: "Sobble",
            mandarin: "淚眼蜥",
            french: "Larméléon",
            german: "Memmeon",
            korean: "울머기",
            japanese: "メッソン",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        817,
        Pokemon {
            number: 817,
            english: "Drizzile",
            mandarin: "變澀蜥",
            french: "Arrozard",
            german: "Phlegleon",
            korean: "누겔레온",
            japanese: "ジメレオン",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        818,
        Pokemon {
            number: 818,
            english: "Inteleon",
            mandarin: "千面避役",
            french: "Lézargus",
            german: "Intelleon",
            korean: "인텔리레온",
            japanese: "インテレオン",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        819,
        Pokemon {
            number: 819,
            english: "Skwovet",
            mandarin: "貪心栗鼠",
            french: "Rongourmand",
            german: "Raffel",
            korean: "탐리스",
            japanese: "ホシガリス",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        820,
        Pokemon {
            number: 820,
            english: "Greedent",
            mandarin: "藏飽栗鼠",
            french: "Rongrigou",
            german: "Schlaraffel",
            korean: "요씽리스",
            japanese: "ヨクバリス",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        821,
        Pokemon {
            number: 821,
            english: "Rookidee",
            mandarin: "稚山雀",
            french: "Minisange",
            german: "Meikro",
            korean: "파라꼬",
            japanese: "ココガラ",
            types: Types::Single(Type::Flying),
        },
    );
    m.insert(
        822,
        Pokemon {
            number: 822,
            english: "Corvisquire",
            mandarin: "藍鴉",
            french: "Bleuseille",
            german: "Kranoviz",
            korean: "파크로우",
            japanese: "アオガラス",
            types: Types::Single(Type::Flying),
        },
    );
    m.insert(
        823,
        Pokemon {
            number: 823,
            english: "Corviknight",
            mandarin: "鋼鎧鴉",
            french: "Corvaillus",
            german: "Krarmor",
            korean: "아머까오",
            japanese: "アーマーガア",
            types: Types::Dual(Type::Flying, Type::Steel),
        },
    );
    m.insert(
        824,
        Pokemon {
            number: 824,
            english: "Blipbug",
            mandarin: "索偵蟲",
            french: "Larvadar",
            german: "Sensect",
            korean: "두루지벌레",
            japanese: "サッチムシ",
            types: Types::Single(Type::Bug),
        },
    );
    m.insert(
        825,
        Pokemon {
            number: 825,
            english: "Dottler",
            mandarin: "天罩蟲",
            french: "Coléodôme",
            german: "Keradar",
            korean: "레돔벌레",
            japanese: "レドームシ",
            types: Types::Dual(Type::Bug, Type::Psychic),
        },
    );
    m.insert(
        826,
        Pokemon {
            number: 826,
            english: "Orbeetle",
            mandarin: "以歐路普",
            french: "Astronelle",
            german: "Maritellit",
            korean: "이올브",
            japanese: "イオルブ",
            types: Types::Dual(Type::Bug, Type::Psychic),
        },
    );
    m.insert(
        827,
        Pokemon {
            number: 827,
            english: "Nickit",
            mandarin: "偷兒狐",
            french: "Goupilou",
            german: "Kleptifux",
            korean: "훔처우",
            japanese: "クスネ",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        828,
        Pokemon {
            number: 828,
            english: "Thievul",
            mandarin: "狐大盜",
            french: "Roublenard",
            german: "Gaunux",
            korean: "폭슬라이",
            japanese: "フォクスライ",
            types: Types::Single(Type::Dark),
        },
    );
    m.insert(
        829,
        Pokemon {
            number: 829,
            english: "Gossifleur",
            mandarin: "幼棉棉",
            french: "Tournicoton",
            german: "Cottini",
            korean: "꼬모카",
            japanese: "ヒメンカ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        830,
        Pokemon {
            number: 830,
            english: "Eldegoss",
            mandarin: "白蓬蓬",
            french: "Blancoton",
            german: "Cottomi",
            korean: "백솜모카",
            japanese: "ワタシラガ",
            types: Types::Single(Type::Grass),
        },
    );
    m.insert(
        831,
        Pokemon {
            number: 831,
            english: "Wooloo",
            mandarin: "毛辮羊",
            french: "Moumouton",
            german: "Wolly",
            korean: "우르",
            japanese: "ウールー",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        832,
        Pokemon {
            number: 832,
            english: "Dubwool",
            mandarin: "毛毛角羊",
            french: "Moumouflon",
            german: "Zwollock",
            korean: "배우르",
            japanese: "バイウールー",
            types: Types::Single(Type::Normal),
        },
    );
    m.insert(
        833,
        Pokemon {
            number: 833,
            english: "Chewtle",
            mandarin: "咬咬龜",
            french: "Khélocrok",
            german: "Kamehaps",
            korean: "깨물부기",
            japanese: "カムカメ",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        834,
        Pokemon {
            number: 834,
            english: "Drednaw",
            mandarin: "暴噬龜",
            french: "Torgamord",
            german: "Kamalm",
            korean: "갈가부기",
            japanese: "カジリガメ",
            types: Types::Dual(Type::Water, Type::Rock),
        },
    );
    m.insert(
        835,
        Pokemon {
            number: 835,
            english: "Yamper",
            mandarin: "來電汪",
            french: "Voltoutou",
            german: "Voldi",
            korean: "멍파치",
            japanese: "ワンパチ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        836,
        Pokemon {
            number: 836,
            english: "Boltund",
            mandarin: "逐電犬",
            french: "Fulgudog",
            german: "Bellektro",
            korean: "펄스멍",
            japanese: "パルスワン",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        837,
        Pokemon {
            number: 837,
            english: "Rolycoly",
            mandarin: "小炭仔",
            french: "Charbi",
            german: "Klonkett",
            korean: "탄동",
            japanese: "タンドン",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        838,
        Pokemon {
            number: 838,
            english: "Carkol",
            mandarin: "大炭車",
            french: "Wagomine",
            german: "Wagong",
            korean: "탄차곤",
            japanese: "トロッゴン",
            types: Types::Dual(Type::Rock, Type::Fire),
        },
    );
    m.insert(
        839,
        Pokemon {
            number: 839,
            english: "Coalossal",
            mandarin: "巨炭山",
            french: "Monthracite",
            german: "Montecarbo",
            korean: "석탄산",
            japanese: "セキタンザン",
            types: Types::Dual(Type::Rock, Type::Fire),
        },
    );
    m.insert(
        840,
        Pokemon {
            number: 840,
            english: "Applin",
            mandarin: "啃果蟲",
            french: "Verpom",
            german: "Knapfel",
            korean: "과사삭벌레",
            japanese: "カジッチュ",
            types: Types::Dual(Type::Grass, Type::Dragon),
        },
    );
    m.insert(
        841,
        Pokemon {
            number: 841,
            english: "Flapple",
            mandarin: "蘋裹龍",
            french: "Pomdrapi",
            german: "Drapfel",
            korean: "애프룡",
            japanese: "アップリュー",
            types: Types::Dual(Type::Grass, Type::Dragon),
        },
    );
    m.insert(
        842,
        Pokemon {
            number: 842,
            english: "Appletun",
            mandarin: "豐蜜龍",
            french: "Dratatin",
            german: "Schlapfel",
            korean: "단지래플",
            japanese: "タルップル",
            types: Types::Dual(Type::Grass, Type::Dragon),
        },
    );
    m.insert(
        843,
        Pokemon {
            number: 843,
            english: "Silicobra",
            mandarin: "沙包蛇",
            french: "Dunaja",
            german: "Salanga",
            korean: "모래뱀",
            japanese: "スナヘビ",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        844,
        Pokemon {
            number: 844,
            english: "Sandaconda",
            mandarin: "沙螺蟒",
            french: "Dunaconda",
            german: "Sanaconda",
            korean: "사다이사",
            japanese: "サダイジャ",
            types: Types::Single(Type::Ground),
        },
    );
    m.insert(
        845,
        Pokemon {
            number: 845,
            english: "Cramorant",
            mandarin: "古月鳥",
            french: "Nigosier",
            german: "Urgl",
            korean: "윽우지",
            japanese: "ウッウ",
            types: Types::Dual(Type::Flying, Type::Water),
        },
    );
    m.insert(
        846,
        Pokemon {
            number: 846,
            english: "Arrokuda",
            mandarin: "刺梭魚",
            french: "Embrochet",
            german: "Pikuda",
            korean: "찌로꼬치",
            japanese: "サシカマス",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        847,
        Pokemon {
            number: 847,
            english: "Barraskewda",
            mandarin: "戽斗尖梭",
            french: "Hastacuda",
            german: "Barrakiefa",
            korean: "꼬치조",
            japanese: "カマスジョー",
            types: Types::Single(Type::Water),
        },
    );
    m.insert(
        848,
        Pokemon {
            number: 848,
            english: "Toxel",
            mandarin: "毒電嬰",
            french: "Toxizap",
            german: "Toxel",
            korean: "일레즌",
            japanese: "エレズン",
            types: Types::Dual(Type::Electric, Type::Poison),
        },
    );
    m.insert(
        849,
        Pokemon {
            number: 849,
            english: "Toxtricity",
            mandarin: "顫弦蠑螈",
            french: "Salarsen",
            german: "Riffex",
            korean: "스트린더",
            japanese: "ストリンダー",
            types: Types::Dual(Type::Electric, Type::Poison),
        },
    );
    m.insert(
        850,
        Pokemon {
            number: 850,
            english: "Sizzlipede",
            mandarin: "燒火蚣",
            french: "Grillepattes",
            german: "Thermopod",
            korean: "태우지네",
            japanese: "ヤクデ",
            types: Types::Dual(Type::Fire, Type::Bug),
        },
    );
    m.insert(
        851,
        Pokemon {
            number: 851,
            english: "Centiskorch",
            mandarin: "焚焰蚣",
            french: "Scolocendre",
            german: "Infernopod",
            korean: "다태우지네",
            japanese: "マルヤクデ",
            types: Types::Dual(Type::Fire, Type::Bug),
        },
    );
    m.insert(
        852,
        Pokemon {
            number: 852,
            english: "Clobbopus",
            mandarin: "拳拳蛸",
            french: "Poulpaf",
            german: "Klopptopus",
            korean: "때때무노",
            japanese: "タタッコ",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        853,
        Pokemon {
            number: 853,
            english: "Grapploct",
            mandarin: "八爪武師",
            french: "Krakos",
            german: "Kaocto",
            korean: "케오퍼스",
            japanese: "オトスパス",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        854,
        Pokemon {
            number: 854,
            english: "Sinistea",
            mandarin: "來悲茶",
            french: "Théffroi",
            german: "Fatalitee",
            korean: "데인차",
            japanese: "ヤバチャ",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        855,
        Pokemon {
            number: 855,
            english: "Polteageist",
            mandarin: "怖思壺",
            french: "Polthégeist",
            german: "Mortipot",
            korean: "포트데스",
            japanese: "ポットデス",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        856,
        Pokemon {
            number: 856,
            english: "Hatenna",
            mandarin: "迷布莉姆",
            french: "Bibichut",
            german: "Brimova",
            korean: "몸지브림",
            japanese: "ミブリム",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        857,
        Pokemon {
            number: 857,
            english: "Hattrem",
            mandarin: "提布莉姆",
            french: "Chapotus",
            german: "Brimano",
            korean: "손지브림",
            japanese: "テブリム",
            types: Types::Single(Type::Psychic),
        },
    );
    m.insert(
        858,
        Pokemon {
            number: 858,
            english: "Hatterene",
            mandarin: "布莉姆溫",
            french: "Sorcilence",
            german: "Silembrim",
            korean: "브리무음",
            japanese: "ブリムオン",
            types: Types::Dual(Type::Psychic, Type::Fairy),
        },
    );
    m.insert(
        859,
        Pokemon {
            number: 859,
            english: "Impidimp",
            mandarin: "搗蛋小妖",
            french: "Grimalin",
            german: "Bähmon",
            korean: "메롱꿍",
            japanese: "ベロバー",
            types: Types::Dual(Type::Dark, Type::Fairy),
        },
    );
    m.insert(
        860,
        Pokemon {
            number: 860,
            english: "Morgrem",
            mandarin: "詐唬魔",
            french: "Fourbelin",
            german: "Pelzebub",
            korean: "쏘겨모",
            japanese: "ギモー",
            types: Types::Dual(Type::Dark, Type::Fairy),
        },
    );
    m.insert(
        861,
        Pokemon {
            number: 861,
            english: "Grimmsnarl",
            mandarin: "長毛巨魔",
            french: "Angoliath",
            german: "Olangaar",
            korean: "오롱털",
            japanese: "オーロンゲ",
            types: Types::Dual(Type::Dark, Type::Fairy),
        },
    );
    m.insert(
        862,
        Pokemon {
            number: 862,
            english: "Obstagoon",
            mandarin: "堵攔熊",
            french: "Ixon",
            german: "Barrikadax",
            korean: "가로막구리",
            japanese: "タチフサグマ",
            types: Types::Dual(Type::Dark, Type::Normal),
        },
    );
    m.insert(
        863,
        Pokemon {
            number: 863,
            english: "Perrserker",
            mandarin: "喵頭目",
            french: "Berserkatt",
            german: "Mauzinger",
            korean: "나이킹",
            japanese: "ニャイキング",
            types: Types::Single(Type::Steel),
        },
    );
    m.insert(
        864,
        Pokemon {
            number: 864,
            english: "Cursola",
            mandarin: "魔靈珊瑚",
            french: "Corayôme",
            german: "Gorgasonn",
            korean: "산호르곤",
            japanese: "サニゴーン",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        865,
        Pokemon {
            number: 865,
            english: "Sirfetch'd",
            mandarin: "蔥遊兵",
            french: "Palarticho",
            german: "Lauchzelot",
            korean: "창파나이트",
            japanese: "ネギガナイト",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        866,
        Pokemon {
            number: 866,
            english: "Mr. Rime",
            mandarin: "踏冰人偶",
            french: "M. Glaquette",
            german: "Pantifrost",
            korean: "마임꽁꽁",
            japanese: "バリコオル",
            types: Types::Dual(Type::Ice, Type::Psychic),
        },
    );
    m.insert(
        867,
        Pokemon {
            number: 867,
            english: "Runerigus",
            mandarin: "死神板",
            french: "Tutétékri",
            german: "Oghnatoll",
            korean: "데스판",
            japanese: "デスバーン",
            types: Types::Dual(Type::Ground, Type::Ghost),
        },
    );
    m.insert(
        868,
        Pokemon {
            number: 868,
            english: "Milcery",
            mandarin: "小仙奶",
            french: "Crèmy",
            german: "Hokumil",
            korean: "마빌크",
            japanese: "マホミル",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        869,
        Pokemon {
            number: 869,
            english: "Alcremie",
            mandarin: "霜奶仙",
            french: "Charmilly",
            german: "Pokusan",
            korean: "마휘핑",
            japanese: "マホイップ",
            types: Types::Single(Type::Fairy),
        },
    );
    m.insert(
        870,
        Pokemon {
            number: 870,
            english: "Falinks",
            mandarin: "列陣兵",
            french: "Hexadron",
            german: "Legios",
            korean: "대여르",
            japanese: "タイレーツ",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        871,
        Pokemon {
            number: 871,
            english: "Pincurchin",
            mandarin: "啪嚓海膽",
            french: "Wattapik",
            german: "Britzigel",
            korean: "찌르성게",
            japanese: "バチンウニ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        872,
        Pokemon {
            number: 872,
            english: "Snom",
            mandarin: "雪吞蟲",
            french: "Frissonille",
            german: "Snomnom",
            korean: "누니머기",
            japanese: "ユキハミ",
            types: Types::Dual(Type::Ice, Type::Bug),
        },
    );
    m.insert(
        873,
        Pokemon {
            number: 873,
            english: "Frosmoth",
            mandarin: "雪絨蛾",
            french: "Beldeneige",
            german: "Mottineva",
            korean: "모스노우",
            japanese: "モスノウ",
            types: Types::Dual(Type::Ice, Type::Bug),
        },
    );
    m.insert(
        874,
        Pokemon {
            number: 874,
            english: "Stonjourner",
            mandarin: "巨石丁",
            french: "Dolman",
            german: "Humanolith",
            korean: "돌헨진",
            japanese: "イシヘンジン",
            types: Types::Single(Type::Rock),
        },
    );
    m.insert(
        875,
        Pokemon {
            number: 875,
            english: "Eiscue",
            mandarin: "冰砌鵝",
            french: "Bekaglaçon",
            german: "Kubuin",
            korean: "빙큐보",
            japanese: "コオリッポ",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        876,
        Pokemon {
            number: 876,
            english: "Indeedee",
            mandarin: "愛管侍",
            french: "Wimessir",
            german: "Servol",
            korean: "에써르",
            japanese: "イエッサン",
            types: Types::Dual(Type::Psychic, Type::Normal),
        },
    );
    m.insert(
        877,
        Pokemon {
            number: 877,
            english: "Morpeko",
            mandarin: "莫魯貝可",
            french: "Morpeko",
            german: "Morpeko",
            korean: "모르페코",
            japanese: "モルペコ",
            types: Types::Dual(Type::Electric, Type::Dark),
        },
    );
    m.insert(
        878,
        Pokemon {
            number: 878,
            english: "Cufant",
            mandarin: "銅象",
            french: "Charibari",
            german: "Kupfanti",
            korean: "끼리동",
            japanese: "ゾウドウ",
            types: Types::Single(Type::Steel),
        },
    );
    m.insert(
        879,
        Pokemon {
            number: 879,
            english: "Copperajah",
            mandarin: "大王銅象",
            french: "Pachyradjah",
            german: "Patinaraja",
            korean: "대왕끼리동",
            japanese: "ダイオウドウ",
            types: Types::Single(Type::Steel),
        },
    );
    m.insert(
        880,
        Pokemon {
            number: 880,
            english: "Dracozolt",
            mandarin: "雷鳥龍",
            french: "Galvagon",
            german: "Lectragon",
            korean: "파치래곤",
            japanese: "パッチラゴン",
            types: Types::Dual(Type::Electric, Type::Dragon),
        },
    );
    m.insert(
        881,
        Pokemon {
            number: 881,
            english: "Arctozolt",
            mandarin: "雷鳥海獸",
            french: "Galvagla",
            german: "Lecryodon",
            korean: "파치르돈",
            japanese: "パッチルドン",
            types: Types::Dual(Type::Electric, Type::Ice),
        },
    );
    m.insert(
        882,
        Pokemon {
            number: 882,
            english: "Dracovish",
            mandarin: "鰓魚龍",
            french: "Hydragon",
            german: "Pescragon",
            korean: "어래곤",
            japanese: "ウオノラゴン",
            types: Types::Dual(Type::Water, Type::Dragon),
        },
    );
    m.insert(
        883,
        Pokemon {
            number: 883,
            english: "Arctovish",
            mandarin: "鰓魚海獸",
            french: "Hydragla",
            german: "Pescryodon",
            korean: "어치르돈",
            japanese: "ウオチルドン",
            types: Types::Dual(Type::Water, Type::Ice),
        },
    );
    m.insert(
        884,
        Pokemon {
            number: 884,
            english: "Duraludon",
            mandarin: "鋁鋼龍",
            french: "Duralugon",
            german: "Duraludon",
            korean: "두랄루돈",
            japanese: "ジュラルドン",
            types: Types::Dual(Type::Steel, Type::Dragon),
        },
    );
    m.insert(
        885,
        Pokemon {
            number: 885,
            english: "Dreepy",
            mandarin: "多龍梅西亞",
            french: "Fantyrm",
            german: "Grolldra",
            korean: "드라꼰",
            japanese: "ドラメシヤ",
            types: Types::Dual(Type::Dragon, Type::Ghost),
        },
    );
    m.insert(
        886,
        Pokemon {
            number: 886,
            english: "Drakloak",
            mandarin: "多龍奇",
            french: "Dispareptil",
            german: "Phandra",
            korean: "드래런치",
            japanese: "ドロンチ",
            types: Types::Dual(Type::Dragon, Type::Ghost),
        },
    );
    m.insert(
        887,
        Pokemon {
            number: 887,
            english: "Dragapult",
            mandarin: "多龍巴魯托",
            french: "Lanssorien",
            german: "Katapuldra",
            korean: "드래펄트",
            japanese: "ドラパルト",
            types: Types::Dual(Type::Dragon, Type::Ghost),
        },
    );
    m.insert(
        888,
        Pokemon {
            number: 888,
            english: "Zacian",
            mandarin: "蒼響",
            french: "Zacian",
            german: "Zacian",
            korean: "자시안",
            japanese: "ザシアン",
            types: Types::Variants(vec![
                Types::Single(Type::Fairy),
                Types::Dual(Type::Fairy, Type::Steel),
            ]),
        },
    );
    m.insert(
        889,
        Pokemon {
            number: 889,
            english: "Zamazenta",
            mandarin: "藏瑪然特",
            french: "Zamazenta",
            german: "Zamazenta",
            korean: "자마젠타",
            japanese: "ザマゼンタ",
            types: Types::Variants(vec![
                Types::Single(Type::Fighting),
                Types::Dual(Type::Fighting, Type::Steel),
            ]),
        },
    );
    m.insert(
        890,
        Pokemon {
            number: 890,
            english: "Eternatus",
            mandarin: "無極汰那",
            french: "Éthernatos",
            german: "Endynalos",
            korean: "무한다이노",
            japanese: "ムゲンダイナ",
            types: Types::Dual(Type::Poison, Type::Dragon),
        },
    );
    m.insert(
        891,
        Pokemon {
            number: 891,
            english: "Kubfu",
            mandarin: "熊徒弟",
            french: "Wushours",
            german: "Dakuma",
            korean: "치고마",
            japanese: "ダクマ",
            types: Types::Single(Type::Fighting),
        },
    );
    m.insert(
        892,
        Pokemon {
            number: 892,
            english: "Urshifu",
            mandarin: "武道熊師",
            french: "Shifours",
            german: "Wulaosu",
            korean: "우라오스",
            japanese: "ウーラオス",
            types: Types::Variants(vec![
                Types::Dual(Type::Fighting, Type::Dark),
                Types::Dual(Type::Fighting, Type::Water),
            ]),
        },
    );
    m.insert(
        893,
        Pokemon {
            number: 893,
            english: "Zarude",
            mandarin: "薩戮德",
            french: "Zarude",
            german: "Zarude",
            korean: "자루도",
            japanese: "ザルード",
            types: Types::Dual(Type::Dark, Type::Grass),
        },
    );
    m.insert(
        894,
        Pokemon {
            number: 894,
            english: "Regieleki",
            mandarin: "雷吉艾勒奇",
            french: "Regieleki",
            german: "Regieleki",
            korean: "레지에레키",
            japanese: "レジエレキ",
            types: Types::Single(Type::Electric),
        },
    );
    m.insert(
        895,
        Pokemon {
            number: 895,
            english: "Regidrago",
            mandarin: "雷吉鐸拉戈",
            french: "Regidrago",
            german: "Regidrago",
            korean: "레지드래고",
            japanese: "レジドラゴ",
            types: Types::Single(Type::Dragon),
        },
    );
    m.insert(
        896,
        Pokemon {
            number: 896,
            english: "Glastrier",
            mandarin: "雪暴馬",
            french: "Blizzeval",
            german: "Polaross",
            korean: "블리자포스",
            japanese: "ブリザポス",
            types: Types::Single(Type::Ice),
        },
    );
    m.insert(
        897,
        Pokemon {
            number: 897,
            english: "Spectrier",
            mandarin: "靈幽馬",
            french: "Spectreval",
            german: "Phantoross",
            korean: "레이스포스",
            japanese: "レイスポス",
            types: Types::Single(Type::Ghost),
        },
    );
    m.insert(
        898,
        Pokemon {
            number: 898,
            english: "Calyrex",
            mandarin: "蕾冠王",
            french: "Sylveroy",
            german: "Coronospa",
            korean: "버드렉스",
            japanese: "バドレックス",
            types: Types::Variants(vec![
                Types::Dual(Type::Psychic, Type::Grass),
                Types::Dual(Type::Psychic, Type::Ice),
                Types::Dual(Type::Psychic, Type::Ghost),
            ]),
        },
    );

    m
});
