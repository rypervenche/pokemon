use clap::Parser;
use std::cell::RefCell;
use std::fmt;
use std::rc::Rc;

#[derive(Debug, Hash, Copy, Clone)]
pub enum Type {
    Bug,
    Dark,
    Dragon,
    Electric,
    Fairy,
    Fighting,
    Fire,
    Flying,
    Ghost,
    Grass,
    Ground,
    Ice,
    Normal,
    Poison,
    Psychic,
    Rock,
    Steel,
    Water,
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, Hash, Clone)]
pub enum Types {
    Single(Type),
    Dual(Type, Type),
    Variants(Vec<Types>),
}

impl fmt::Display for Types {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Types::Single(s) => write!(f, "{}", s),
            Types::Dual(s1, s2) => write!(f, "{}/{}", s1, s2),
            Types::Variants(v) => {
                for t in v {
                    writeln!(f, "{}", t)?;
                }
                Ok(())
            }
        }
    }
}

#[derive(Debug, Hash, Clone)]
pub struct Pokemon<'a> {
    pub number: i16,
    pub english: &'a str,
    pub mandarin: &'a str,
    pub french: &'a str,
    pub german: &'a str,
    pub korean: &'a str,
    pub japanese: &'a str,
    pub types: Types,
}

#[derive(Debug, Clone)]
pub struct Counter {
    count: Rc<RefCell<usize>>,
}

impl Counter {
    pub fn new(val: usize) -> Self {
        Counter {
            count: Rc::from(RefCell::from(val)),
        }
    }

    pub fn increment(&mut self, num: usize) {
        *self.count.borrow_mut() += num;
    }

    pub fn decrement(&mut self, num: usize) {
        *self.count.borrow_mut() -= num;
    }

    pub fn value(&self) -> usize {
        *self.count.borrow()
    }

    pub fn update(&mut self, num: usize) {
        *self.count.borrow_mut() = num;
    }
}

#[derive(Parser)]
#[clap(version, help = "Multilingual Pokemon name learner")]
pub struct Opts {
    #[clap(
        short,
        long,
        use_delimiter = true,
        require_delimiter = true,
        value_name = "digit",
        help = "Generations to choose"
    )]
    pub generation: Vec<i8>,
    #[clap(short, long, help = "Use random order")]
    pub random: bool,
}
