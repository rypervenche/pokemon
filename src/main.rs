// TODO: Add generation number to struct
// TODO: Add field for each attribute in GUI
// TODO: Add language choice via CLI options (-a, -c, -k, -j, etc.), have a default as well, maybe
// all or only Mandarin
mod constants;
mod fltk;

use anyhow::Result;
use clap::Parser;
use constants::*;
use pokemon::*;
use std::collections::BTreeSet;

fn main() -> Result<()> {
    let opts: Opts = Opts::parse();

    let mut chosen_nums: BTreeSet<i16> = BTreeSet::new();

    let generation = &opts.generation;

    if !generation.is_empty() {
        for gen in generation {
            let gen = gen - 1;
            chosen_nums.extend(&GENERATIONS[gen as usize]);
        }
    } else {
        for gen in GENERATIONS.iter() {
            chosen_nums.extend(gen);
        }
    }

    let chosen_pokemon: Vec<Pokemon> = chosen_nums
        .iter()
        .map(|num| POKEMON_LIST[num].to_owned())
        .collect();

    fltk::popup(chosen_pokemon)?;
    println!("{:#?}", opts.generation);

    Ok(())
}
