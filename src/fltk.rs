use anyhow::Result;
use fltk::{enums::*, image::SharedImage, prelude::*, *};
//use fontconfig::Fontconfig;
use pokemon::{Counter, Pokemon};
use std::path::PathBuf;

const POKEMON_IMAGE_DIR: &str = "/home/rypervenche/git/Random-Pokemon-Generator/sprites";

pub fn popup(pokemon_list: Vec<Pokemon>) -> Result<()> {
    let app = app::App::default().with_scheme(app::Scheme::Gleam);
    // TODO: Specify font in the future, for now use default
    //let fc = Fontconfig::new().unwrap();
    //let font = fc.find("全字庫正楷體", None).unwrap();
    //let _font = app.load_font(font.path).unwrap();
    let mut wind = window::Window::default().with_size(800, 800);
    let mut frame = frame::Frame::default().size_of(&wind);

    let quit = Counter::new(0);
    let index = Counter::new(0);
    let len = pokemon_list.len();
    while quit.value() == 0 {
        let mut index = index.clone();
        let mut quit = quit.clone();
        let current_pokemon: Pokemon = pokemon_list[index.value()].clone();
        let mut pokemon_image_path = PathBuf::from(POKEMON_IMAGE_DIR);
        pokemon_image_path.push(format!("{:03}", &current_pokemon.number));
        pokemon_image_path.set_extension("png");

        let mut image = SharedImage::load(pokemon_image_path)?;
        image.scale(400, 400, true, true);
        frame.set_label_size(30);
        frame.set_image(Some(image));
        wind.set_color(enums::Color::White);
        wind.end();
        wind.make_resizable(true);
        wind.show();

        let info = format!(
            "{}\n{}\n{}\n{}",
            &current_pokemon.number,
            &current_pokemon.mandarin,
            &current_pokemon.english,
            &current_pokemon.types
        );
        frame.set_label(&info);
        // TODO: Part of commented out font thing above
        //frame.set_label_font(enums::Font::by_name(&font));

        frame.handle(move |_f, ev| {
            if ev == Event::Shortcut {
                if app::event_text() == " "
                    || app::event_text() == "n"
                    || app::event_key() == Key::Enter
                {
                    if index.value() < len - 1 {
                        index.increment(1);
                    }

                    app::quit();
                } else if app::event_text() == "N" {
                    if index.value() < len - 50 {
                        index.increment(50);
                    } else {
                        index.update(len - 1);
                    }
                    app::quit();
                } else if app::event_text() == "p" || app::event_key() == Key::BackSpace {
                    if index.value() > 0 {
                        index.decrement(1);
                    }
                    app::quit();
                } else if app::event_text() == "P" {
                    if index.value() > 50 {
                        index.decrement(50);
                    } else {
                        index.update(0);
                    }
                    app::quit();
                } else if app::event_text() == "q" {
                    quit.increment(1);
                    app::quit();
                }
                true
            } else {
                false
            }
        });
        app.run()?;
    }
    Ok(())
}
